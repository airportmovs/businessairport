﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class FormPlayerFlights : Form
    {
        Game game;
        MainForm mainForm;
        public bool isBackFlight;
        public DateTime BackFlightTime;
        public FormPlayerFlights(MainForm mainForm, Game game)
        {
            InitializeComponent();
            this.game = game;
            this.mainForm = mainForm;
            dgvSchedule.AutoGenerateColumns = false;
            dgvSchedule.DataSource = game.ListOfPlayerFlights;
        }

        private void dgvSchedule_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && dgvSchedule.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                    (string)dgvSchedule.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == "Отказаться")
            {
                ColumnCancelClick(e);
            }
            // нажали кнопку в столбце Самолет
            else
            {
                ColumnPlaneClick(e);
            }
        }

        private void ColumnCancelClick(DataGridViewCellEventArgs e)
        {
            bool isTicking = game.tMachine.Status;
            if (isTicking)
                game.tMachine.Stop();
            string message = "Вы уверены, что хотите отказаться от этого рейса?";
            if (MessageBox.Show(message, "Отказ от рейса", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Flight flight = game.ListOfPlayerFlights[e.RowIndex];
                flight.AppointCount = 0;
                game.ReturnToBoard(flight);
                game.ListOfPlayerFlights.Remove(flight);
                game.DeleteFromSchedule(flight);
            }
            if (isTicking)
                game.tMachine.Start();
        }

        private void ColumnPlaneClick(DataGridViewCellEventArgs e)
        {
            bool isTicking = game.tMachine.Status;
            if (isTicking)
                game.tMachine.Stop();
            if (e.RowIndex != -1 && dgvSchedule.Columns[e.ColumnIndex] is DataGridViewButtonColumn
                && (string)dgvSchedule[e.ColumnIndex, e.RowIndex].Value != "")        // при нажатии на пустую кнопку не выводим форму
            {
                Flight flight = game.ListOfPlayerFlights[e.RowIndex];
                if ((string)dgvSchedule[e.ColumnIndex, e.RowIndex].Value == "Изменить")
                    ChangeClick(flight);
                else
                    AppointClick(flight, e);
            }
            if (isTicking)
                game.tMachine.Start();
        }

        private void ChangeClick(Flight flight)
        {
            int index = 0;
            while (index < game.ListOfFlightSheduleElem.Count && flight != game.ListOfFlightSheduleElem[index].Flight)
                index++;
            if (index != game.ListOfFlightSheduleElem.Count)
            {
                FlightScheduleElem fse = game.ListOfFlightSheduleElem[index];
                FormAppoint formAppoint = new FormAppoint(mainForm, game, fse);
                if (formAppoint.ShowDialog() == DialogResult.OK)
                {
                    game.ChangeFlight(index, true);
                }
            }
        }

        private void AppointClick(Flight flight, DataGridViewCellEventArgs e)
        {
            FlightScheduleElem fse = new FlightScheduleElem(game.ScheduleNumber, flight, 1);
            FormAppoint formAppoint = new FormAppoint(mainForm, game, fse);
            if (formAppoint.ShowDialog() == DialogResult.OK)
            {
                if (flight.HowOften == HowOften.Нет)
                    flight.BtPlane = "Изменить";
                flight.FseList.Add(fse);
                game.AppointFlight(fse);
                if (flight.HowOften != HowOften.Нет)
                    if (MessageBox.Show("Желаете назначить обратный рейс?", "Обратный рейс", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        isBackFlight = true;
                        BackFlightTime = formAppoint.TimeFinish.AddMinutes(1);
                        FlightScheduleElem fseBro = new FlightScheduleElem(game.ScheduleNumber, new Flight(0, flight), 1);
                        formAppoint = new FormAppoint(mainForm, game, fseBro);
                        if (formAppoint.ShowDialog() == DialogResult.OK)
                        {
                            flight.FseList.Add(fseBro);
                            game.AppointFlight(fseBro);
                            fse.AddBrother(fseBro);
                            fseBro.AddBrother(fse);
                        }
                        isBackFlight = false;
                    }
            }
        }

        public void DisableRow(Flight flight)
        {
            if (flight.HowOften == HowOften.Нет && !flight.IsEmptyFlight)
            {
                flight.BtCancel = flight.BtPlane = "";
                flight.Status = Status.Выполняется;
            }
        }

        public void EnableRow(Flight flight)
        {
            if (flight.HowOften == HowOften.Нет && !flight.IsEmptyFlight)
            {
                flight.BtPlane = "Назначить";
            }
        }

        public void RefreshCount(Flight flight, bool add = true)
        {
            if (add)
            {
                flight.Status = Status.Выполнен;
                flight.CompleteCount++;
            }
            else
            {
                flight.Status = Status.НеВыполнен;
                flight.CompleteCount = 0;
            }
        }

        private int FindInDGV(Flight flight)
        {
            int index = 0;
            while (index < dgvSchedule.RowCount && (Flight)dgvSchedule.Rows[index].Tag != flight)
                index++;
            if (index == dgvSchedule.RowCount)
                index = -1;
            return index;
        }

        private void FormPlayerFlights_Shown(object sender, EventArgs e)
        {
            dgvSchedule.Refresh();
        }
    }
}
