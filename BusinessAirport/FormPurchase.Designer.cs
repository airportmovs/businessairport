﻿namespace BusinessAirport
{
    partial class FormPurchase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnYes = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.btnNo = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lbType = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numDaysOrMonth = new System.Windows.Forms.NumericUpDown();
            this.gbDaysOrMonth = new System.Windows.Forms.GroupBox();
            this.lbDaysOrMonth = new System.Windows.Forms.Label();
            this.lbPrice = new System.Windows.Forms.Label();
            this.lbMonthPay = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numDaysOrMonth)).BeginInit();
            this.gbDaysOrMonth.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnYes
            // 
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnYes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnYes.Location = new System.Drawing.Point(16, 274);
            this.btnYes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(168, 28);
            this.btnYes.TabIndex = 1;
            this.btnYes.Text = "Да";
            this.btnYes.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Выберите тип:";
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "Покупка",
            "Аренда",
            "Лизинг"});
            this.cbType.Location = new System.Drawing.Point(211, 15);
            this.cbType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(160, 24);
            this.cbType.TabIndex = 3;
            this.cbType.SelectedIndexChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 106);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Самолет:";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(92, 106);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(74, 16);
            this.labelName.TabIndex = 5;
            this.labelName.Text = "Название";
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNo.Location = new System.Drawing.Point(192, 274);
            this.btnNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(180, 28);
            this.btnNo.TabIndex = 6;
            this.btnNo.Text = "Нет";
            this.btnNo.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 190);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Вы уверены, что хотите";
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Location = new System.Drawing.Point(188, 190);
            this.lbType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(53, 16);
            this.lbType.TabIndex = 8;
            this.lbType.Text = "купить";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(207, 224);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "этот самолет?";
            // 
            // numDaysOrMonth
            // 
            this.numDaysOrMonth.Location = new System.Drawing.Point(293, 15);
            this.numDaysOrMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numDaysOrMonth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numDaysOrMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDaysOrMonth.Name = "numDaysOrMonth";
            this.numDaysOrMonth.Size = new System.Drawing.Size(75, 22);
            this.numDaysOrMonth.TabIndex = 10;
            this.numDaysOrMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDaysOrMonth.ValueChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
            this.numDaysOrMonth.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numDaysOrMonth_KeyUp);
            // 
            // gbDaysOrMonth
            // 
            this.gbDaysOrMonth.Controls.Add(this.lbDaysOrMonth);
            this.gbDaysOrMonth.Controls.Add(this.numDaysOrMonth);
            this.gbDaysOrMonth.Location = new System.Drawing.Point(4, 48);
            this.gbDaysOrMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbDaysOrMonth.Name = "gbDaysOrMonth";
            this.gbDaysOrMonth.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbDaysOrMonth.Size = new System.Drawing.Size(376, 48);
            this.gbDaysOrMonth.TabIndex = 11;
            this.gbDaysOrMonth.TabStop = false;
            // 
            // lbDaysOrMonth
            // 
            this.lbDaysOrMonth.AutoSize = true;
            this.lbDaysOrMonth.Location = new System.Drawing.Point(12, 20);
            this.lbDaysOrMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbDaysOrMonth.Name = "lbDaysOrMonth";
            this.lbDaysOrMonth.Size = new System.Drawing.Size(144, 16);
            this.lbDaysOrMonth.TabIndex = 11;
            this.lbDaysOrMonth.Text = "Введите количество";
            // 
            // lbPrice
            // 
            this.lbPrice.AutoSize = true;
            this.lbPrice.Location = new System.Drawing.Point(12, 133);
            this.lbPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPrice.Name = "lbPrice";
            this.lbPrice.Size = new System.Drawing.Size(41, 16);
            this.lbPrice.TabIndex = 12;
            this.lbPrice.Text = "Цена";
            // 
            // lbMonthPay
            // 
            this.lbMonthPay.AutoSize = true;
            this.lbMonthPay.Location = new System.Drawing.Point(12, 161);
            this.lbMonthPay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbMonthPay.Name = "lbMonthPay";
            this.lbMonthPay.Size = new System.Drawing.Size(151, 16);
            this.lbMonthPay.TabIndex = 13;
            this.lbMonthPay.Text = "Ежемесячный платёж";
            // 
            // FormPurchase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.ClientSize = new System.Drawing.Size(387, 320);
            this.Controls.Add(this.lbMonthPay);
            this.Controls.Add(this.lbPrice);
            this.Controls.Add(this.gbDaysOrMonth);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnYes);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPurchase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Покупка/аренда/лизинг";
            ((System.ComponentModel.ISupportInitialize)(this.numDaysOrMonth)).EndInit();
            this.gbDaysOrMonth.ResumeLayout(false);
            this.gbDaysOrMonth.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnYes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button btnNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numDaysOrMonth;
        private System.Windows.Forms.GroupBox gbDaysOrMonth;
        private System.Windows.Forms.Label lbDaysOrMonth;
        private System.Windows.Forms.Label lbPrice;
        private System.Windows.Forms.Label lbMonthPay;
    }
}