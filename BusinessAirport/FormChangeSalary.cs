﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class FormChangeSalary : Form
    {
        int salary;

        /// <summary>
        /// Зарплата, которую ввели на форме
        /// </summary>
        public int Salary
        {
            get { return salary; }
        }

        /// <summary>
        /// Конструктор формы изменения зарплаты
        /// </summary>
        /// <param name="p">Человек, у которого изменяем зарплату</param>
        public FormChangeSalary(Person p)
        {
            InitializeComponent();
            nudSalary.Value = salary = p.TempSalary;
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            salary = (int)nudSalary.Value;
        }
    }
}
