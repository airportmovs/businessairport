﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAirport
{
    [Serializable]
    public class Plane
    {
        public string name; // Наименование
        public OwnType typeOfOwnership; // Тип владения
        public PlaneType pType; // Тип самолета
        public int carryWeight; // Число перевозимых едениц
        public Cities curCity; // Текущий город
        public int shopPrice; // Цена в магазине
        public int monthPayment; // месячный платеж при Лизинге
        public int speed; // скорость
        public int rentCost; // стоимость аренды за день
        public int lease; // срок аренды
        public int consumption; // расход денежных едениц на 100 км
        public int maxDistance; // Допустимая дистанция
        public bool IsFlight; // назначен на рейс?
        public bool Dolzhoook; //Должоок :D
        public List<Person> persons = new List<Person>(); // список экипажа
        public byte maxPersons; //число работников, необходимое для взлета самолета
        public uint allDistance; // Дистанция налёта
        public uint popul; //популярность самолёта (количество раз, которое его взяли)

        /// <summary>
        /// Полный конструктор для экземпляра класса самолет
        /// </summary>
        /// <param name="name">Наименование самолета</param>
        /// <param name="tOwn">Тип владения</param>
        /// <param name="pType">Тип самолета</param>
        /// <param name="weight">Число перевозимых единиц</param>
        /// <param name="curCity">Текущий город</param>
        /// <param name="sPrice">Цена в магазине</param>
        /// <param name="mPay">Месячный платеж при Лизинге</param>
        /// <param name="spd">Скорость</param>
        /// <param name="rCost">Стоимость аренды</param>
        /// <param name="lease">Длительность лизинга</param>
        /// <param name="cons">Расход денежных единиц на 100км</param>
        /// <param name="dist">Допустимая дистанция перелета</param>
        ///  <param name="mPersons">Число работников экипажа</param>
        ///   /// <param name="allDist">Сколько пролетел самолет за свою тяжкую жизнь</param>
        public Plane(string name, OwnType tOwn, PlaneType pType, int weight, int spd, int cons, int dist, Cities curCity, int sPrice, int mPay, int rCost, int lease, byte mPersons, uint allDist)
        {
            this.name = name;
            typeOfOwnership = tOwn;
            this.pType = pType;
            carryWeight = weight;
            popul = 0;
            shopPrice = Convert.ToInt32(sPrice + sPrice * popul * 0.0003);
            monthPayment = mPay;
            this.curCity = curCity;
            speed = spd;
            rentCost = rCost;
            this.lease = lease;
            consumption = cons;
            maxDistance = dist;
            maxPersons = mPersons;
            allDistance = allDist;
        }

        /// <summary>
        /// Конструктор для случая покупки в магазине самолёта
        /// </summary>
        /// <param name="p">Самолет</param>
        /// <param name="tOwn">Тип владения</param>
        /// <param name="mPay">Месячный платеж по лизингу</param>
        /// <param name="rCost">Стоимость Аренды</param>
        /// <param name="lease">Длительность лизинга</param>
        /// <param name="mPersons">Число работников экипажа</param>
        /// <param name="allDist">Сколько пролетел самолет за свою тяжкую жизнь</param>
        public Plane(Plane p, OwnType tOwn, int mPay, int rCost, int lease, byte mPersons, uint allDist)
        {
            typeOfOwnership = tOwn;
            name = p.name;
            pType = p.pType;
            carryWeight = p.carryWeight;
            shopPrice = Convert.ToInt32(p.shopPrice + p.shopPrice * p.popul * 0.0003);
            speed = p.speed;
            maxDistance = p.maxDistance;
            consumption = p.consumption;
            monthPayment = mPay;
            rentCost = rCost;
            this.lease = lease;
            maxPersons = mPersons;
            allDistance = allDist;
        }

        // конструктор для самолетов из списка мазазина
        /// <summary>
        /// Конструктор для случая размещения информации на "витрине"
        /// </summary>
        /// <param name="name">Наименование</param>
        /// <param name="pType">Тип самолета</param>
        /// <param name="weight">Число перевозимых едениц</param>
        /// <param name="spd">Скорость</param>
        /// <param name="sPrice">Цена в магазине</param>
        /// <param name="rCost">Стоимость аренды за день</param>
        /// <param name="cons">Расход денежных единиц на 100 км</param>
        /// <param name="dist">Допустимая дистанцая перелета</param>
        /// <param name="mPersons">Число работников экипажа</param>
        /// <param name="allDist">Сколько пролетел самолет за свою тяжкую жизнь</param>
        public Plane(string name, PlaneType pType, int weight, int spd, int cons, int dist, int sPrice, int rCost, byte mPersons, uint allDist)
        {
            this.name = name;
            this.pType = pType;
            carryWeight = weight;
            popul = 0;
            shopPrice =  Convert.ToInt32(sPrice + sPrice * popul * 0.0003);
            speed = spd;
            rentCost = rCost;
            consumption = cons;
            maxDistance = dist;
            maxPersons = mPersons;
            allDistance = allDist;
        }

        /// <summary>
        /// генерация количества налёта км
        /// </summary>
        /// <returns></returns>
        private int RandDist()
        {
            int randD;
            return randD = Game.Random.Next(10000, 8000000);
        }

        // конструктор для старых самолетов из списка мазазина
        /// <summary>
        /// Конструктор для случая размещения информации на "витрине"
        /// </summary>
        /// <param name="name">Наименование</param>
        /// <param name="pType">Тип самолета</param>
        /// <param name="weight">Число перевозимых едениц</param>
        /// <param name="spd">Скорость</param>
        /// <param name="sPrice">Цена в магазине</param>
        /// <param name="rCost">Стоимость аренды за день</param>
        /// <param name="cons">Расход денежных единиц на 100 км</param>
        /// <param name="dist">Допустимая дистанцая перелета</param>
        /// <param name="allDist"> Дистанцая налета</param>
        public Plane(Plane p)
        {
            name = p.name;
            pType = p.pType;
            carryWeight = p.carryWeight;
            allDistance = Convert.ToUInt32(RandDist());
            shopPrice = Convert.ToInt32(p.shopPrice-p.shopPrice*((double)allDistance/p.speed)*0.00007);
            speed = p.speed;
            rentCost = p.rentCost;
            consumption = p.consumption;
            maxDistance = p.maxDistance;
            maxPersons = p.maxPersons;            
        }

        public override string ToString()
        {
            return name;
        }
    }
}
