﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Drawing;

namespace BusinessAirport
{
    [Serializable]
    public class Hangar
    {
        [NonSerialized]
        TimeMachine tMachine;
        [NonSerialized]
        MainForm mainForm;
        [NonSerialized]
        Game game;
        List<Person> freePersons;
        List<Plane> planeList;
        List<Plane> listOfOldPlane;
        List<Plane> listOfPlaneEnemy;
        List<Plane> planeShop = new List<Plane>()
            {
                new Plane("Боинг-747-400",  PlaneType.Пассажирский, 600,    913,    10400, 14000, 13134000,   43780, 13, 0),
                new Plane("Боинг-777-300",  PlaneType.Пассажирский, 450,    945,    7900, 10000,  11526350,   38421, 11, 0),
                new Plane("Airbus A318",    PlaneType.Пассажирский, 117,    890,    2250,  6000, 2844600,    9482, 5, 0),
                new Plane("Airbus A321",    PlaneType.Пассажирский, 220,    890,    2740,  5500, 3300000,    11000, 7, 0),
                new Plane("Airbus A340-500",    PlaneType.Пассажирский, 313,    890,    8300, 16400,  10450000,   34833, 9, 0),
                new Plane("Airbus А380",    PlaneType.Пассажирский, 853,    1020,   12000, 15400,  16500000,   55000, 20, 0),
                new Plane("Ту-204-300", PlaneType.Пассажирский, 164,    820,    3250, 9200,  1760000,   5867, 6, 0),
                new Plane("Ту-154М",    PlaneType.Пассажирский, 180,    975,    5300, 3900,  2475000,   8250, 8, 0),
                new Plane("Sukhoi Superjet 100",    PlaneType.Пассажирский, 98, 860,    1700, 3048,  2035000,  6783, 4, 0),
                new Plane("Як-42",  PlaneType.Пассажирский, 120,    810,    3100, 2900,  1540000,  5133, 6, 0),
                new Plane("Ил-96М/Т",   PlaneType.Пассажирский, 435,    900,    7500, 12800,  5720000,   19067, 12, 0),
                new Plane("Ан-148-100Е",    PlaneType.Пассажирский, 85, 870,    1650, 4400,  1288100,    4294, 4, 0),
                new Plane("Airbus Beluga",  PlaneType.Грузовой, 47, 750,    5700, 2779,  748000,   2493, 2, 0),
                new Plane("Ан-124 «Руслан»",    PlaneType.Грузовой, 120,    865,    12600, 4880, 11000000,   36667, 8, 0),
                new Plane("ТУ-204С",    PlaneType.Грузовой, 27, 810,    3300, 3200,  2681580,    8939, 3, 0),
                new Plane("Ан-225 «Мрия»",  PlaneType.Грузовой, 150,    850,    15900, 10000, 17490000,   58300, 6, 0),
                new Plane("ИЛ-76ТД",    PlaneType.Грузовой, 50, 830,    8170,  8200, 1788655,    5962, 4, 0),
                new Plane("АН 124-100", PlaneType.Грузовой, 120,    780,    12600, 4040,  12925000,   43083, 8, 0),
                new Plane("Ан-227 «Мрия»",  PlaneType.Грузовой, 250,    850,    15900, 4000, 17490000,   58300, 6, 0)
            };

        public List<Plane> PlaneList
        {
            get { return planeList; }
            set { planeList = value; }
        }

        public List<Plane> PlaneShop
        {
            get { return planeShop; }
            set { planeShop = value; }
        }

        public List<Plane> ListOfOldPlane
        {
            set { listOfOldPlane = value; }
            get { return listOfOldPlane; }
        }

        public List<Plane> ListOfPlaneEnemy
        {
            set { listOfPlaneEnemy = value; }
            get { return listOfPlaneEnemy; }
        }
        /// <summary>
        /// Конструктор для класса Hangar
        /// </summary>
        /// <param name="mainForm">Экземпляр главной формы</param>
        /// <param name="tMachine">Экземпляр машины времени</param>
        public Hangar(MainForm mainForm, Game game)
        {
            this.mainForm = mainForm;
            this.tMachine = game.tMachine;
            this.game = game;
            this.freePersons = game.ListOfFreePersons;
            ListOfOldPlane = new List<Plane>();
            ListOfPlaneEnemy = new List<Plane>();

        }


        #region Работа со временем
        /// <summary>
        /// Отсчёт дней до конца аренды/появления самолёта
        /// </summary>
        /// <param name="e">Самолёт (Plane), для которого будет отсчёт</param>
        public void AddDays(object e)
        {
            Plane p = (Plane)e;
            p.Dolzhoook = false;
            //Если дни ещё не закончились
            if (p.lease > 1)
            {
                p.lease--;
                //Событие на следующий день
                tMachine.AddToList(tMachine.MainTime.AddDays(1), AddDays, p);
            }
            //Если последний день закончился
            else
            {
                //Купленный самолёт просто будет доступен на форме
                if (p.lease != 0) p.lease--;
                //Арендованный удаляется
                if (p.typeOfOwnership == OwnType.Rent)
                {
                    if (p.allDistance != 0)
                    {
                        p.rentCost = p.shopPrice / 300;
                        ListOfOldPlane.Add(p);
                        mainForm.formHangar.FillBoard();
                    }
                    //Если аренда закончилась, но самолёт в рейсе - ждём окончания
                    if (!p.IsFlight)
                    {
                        //перемещаем людей в резервный список
                        InListOfFreePersons(p);
                        //удаляем самолет
                        planeList.Remove(p);
                        bool isTicking = tMachine.Status;
                        if (isTicking)
                            tMachine.Stop();
                        mainForm.Invoke((MethodInvoker)delegate
                        {
                            if (MessageBox.Show("Закончился срок аренды " + p.name, "", MessageBoxButtons.OK) == DialogResult.OK) { };
                        });
                        if (isTicking)
                            tMachine.Start();
                        return;
                    }
                    else
                    {
                        p.Dolzhoook = true;
                        return;
                    }
                }
            }
        }
        /// <summary>
        /// Выплаты лизинга каждый месяц
        /// </summary>
        /// <param name="e">Самолёт (Plane), для которого будут выплаты</param>
        public void MonthPay(object e)
        {
            Plane p = (Plane)e;
            p.Dolzhoook = false;
            //Если не последний месяц
            if (p.lease > 1)
            {
                //Уменьшение количества месяцев до полной выплаты
                p.lease--;
                //Выплата лизинга
                if (mainForm.Balance >= p.monthPayment)
                {
                    mainForm.RefreshBalance("Выплата лизинга", -p.monthPayment);
                }
                else
                {
                    //Если денег не хватает, но самолёт поставлен на рейс, ждём завершения рейса
                    if (!p.IsFlight)
                    {
                        //перемещаем людей в резервный список
                        InListOfFreePersons(p);
                        //удаляем самолет
                        planeList.Remove(p);
                        bool isTicking = tMachine.Status;
                        if (isTicking)
                            tMachine.Stop();
                        string s = "Ваш баланс близок к нулю! Вы не можете оплатить очередной взнос по лизингу! Самолёт " + p.name + " переходит в собственность кредитора.";
                        mainForm.Invoke((MethodInvoker)delegate
                        {
                            if (MessageBox.Show(s, "", MessageBoxButtons.OK) == DialogResult.OK) { };
                        });
                        if (isTicking)
                            tMachine.Start();
                    }
                    else
                    {
                        p.Dolzhoook = true;
                    }
                    return;
                }
                //Событие на следующий месяц
                tMachine.AddToList(tMachine.MainTime.AddMonths(1), MonthPay, p);
            }
            //Если это был последний месяц - самолёт переходит в собственность
            else
            {
                p.lease--;
                p.typeOfOwnership = OwnType.Buy;
            }
        }
        #endregion

        /// <summary>
        /// Аренда/Покупка/Лизинг
        /// </summary>
        /// <param name="p">Приобретаемый самолёт</param>
        public int BuyPlane(Plane p)
        {
            if (planeList == null) planeList = new List<Plane>();
            switch (p.typeOfOwnership)
            {
                case OwnType.Buy:
                    #region Покупка
                    if (mainForm.Balance < p.shopPrice)
                    {

                        MessageBox.Show("Недостаточно средств для приобретения!");
                        return -1;
                    }
                    mainForm.RefreshBalance("Покупка самолета " + p.name, -p.shopPrice);
                    if (p.allDistance == 0)
                    {
                        game.PlayerRaiting += 10;
                        mainForm.RefreshRaiting();
                    }
                    planeList.Add(p);
                    //Дни до появления самолёта
                    tMachine.AddToList(tMachine.MainTime.AddDays(3), AddDays, p);
                    break;
                #endregion
                case OwnType.Rent:
                    #region Взятие в аренду
                    if (mainForm.Balance < p.rentCost * p.lease)
                    {
                        MessageBox.Show("Недостаточно средств для приобретения!");
                        return -1;
                    }
                    mainForm.RefreshBalance("Оплата аренды " + p.name, -p.rentCost * p.lease);
                    planeList.Add(p);
                    //Дни до окончания аренды самолёта
                    tMachine.AddToList(tMachine.MainTime.AddDays(1), AddDays, p);
                    break;
                #endregion
                case OwnType.Leasing:
                    #region Взятие в лизинг
                    if (mainForm.Balance < Convert.ToInt32((p.shopPrice + p.shopPrice * 0.2) * 0.2))
                    {
                        MessageBox.Show("Недостаточно средств для приобретения!");
                        return -1;
                    }
                    mainForm.RefreshBalance("Оплата лизинга", -Convert.ToInt32((p.shopPrice + p.shopPrice * 0.2) * 0.2));
                    if (p.allDistance == 0)
                    {
                        game.PlayerRaiting += 7;
                        mainForm.RefreshRaiting();
                    }
                    planeList.Add(p);
                    //Выплаты каждый месяц
                    tMachine.AddToList(tMachine.MainTime.AddMonths(1), MonthPay, p);
                    break;
                    #endregion
            }
            //При удачном завершении приобретения возврат 0, при неудачном -1
            return 0;
        }


        /// <summary>
        /// Перемещение людей в резервный список
        /// </summary>
        /// <param name="p">Самолет, людей которого перемещаем</param>
        public void InListOfFreePersons(Plane p)
        {
            if (p.persons != null)
            {
                foreach (Person person in p.persons)
                {
                    freePersons.Add(person);
                }
            }
        }
    }
}