﻿namespace BusinessAirport
{
    partial class FormRecruitPersons
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.nudSalary = new System.Windows.Forms.NumericUpDown();
            this.nudCountPerson = new System.Windows.Forms.NumericUpDown();
            this.btnRecruit = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountPerson)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Зарплата";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 73);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Число человек";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.ForeColor = System.Drawing.Color.White;
            this.lblResult.Location = new System.Drawing.Point(13, 181);
            this.lblResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(94, 16);
            this.lblResult.TabIndex = 2;
            this.lblResult.Text = "Я жду, пиши...";
            // 
            // nudSalary
            // 
            this.nudSalary.Location = new System.Drawing.Point(16, 37);
            this.nudSalary.Margin = new System.Windows.Forms.Padding(4);
            this.nudSalary.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nudSalary.Name = "nudSalary";
            this.nudSalary.Size = new System.Drawing.Size(396, 22);
            this.nudSalary.TabIndex = 3;
            this.nudSalary.ValueChanged += new System.EventHandler(this.nudSalary_ValueChanged);
            this.nudSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nudSalary_KeyPress);
            // 
            // nudCountPerson
            // 
            this.nudCountPerson.Location = new System.Drawing.Point(15, 93);
            this.nudCountPerson.Margin = new System.Windows.Forms.Padding(4);
            this.nudCountPerson.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nudCountPerson.Name = "nudCountPerson";
            this.nudCountPerson.Size = new System.Drawing.Size(397, 22);
            this.nudCountPerson.TabIndex = 4;
            // 
            // btnRecruit
            // 
            this.btnRecruit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnRecruit.ForeColor = System.Drawing.Color.White;
            this.btnRecruit.Location = new System.Drawing.Point(15, 138);
            this.btnRecruit.Margin = new System.Windows.Forms.Padding(4);
            this.btnRecruit.Name = "btnRecruit";
            this.btnRecruit.Size = new System.Drawing.Size(191, 28);
            this.btnRecruit.TabIndex = 5;
            this.btnRecruit.Text = "Нанять";
            this.btnRecruit.UseVisualStyleBackColor = false;
            this.btnRecruit.Click += new System.EventHandler(this.btnRecruit_Click);
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.ForeColor = System.Drawing.Color.White;
            this.btnOK.Location = new System.Drawing.Point(214, 138);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(198, 28);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "ОК, хватит";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FormRecruitPersons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.ClientSize = new System.Drawing.Size(425, 221);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnRecruit);
            this.Controls.Add(this.nudCountPerson);
            this.Controls.Add(this.nudSalary);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormRecruitPersons";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Поиск работников";
            this.Shown += new System.EventHandler(this.FormRecruitPersons_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.nudSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountPerson)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.NumericUpDown nudSalary;
        private System.Windows.Forms.NumericUpDown nudCountPerson;
        private System.Windows.Forms.Button btnRecruit;
        private System.Windows.Forms.Button btnOK;
    }
}