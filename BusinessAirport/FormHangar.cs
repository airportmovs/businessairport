﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class FormHangar : Form
    {
        Hangar hangar;
        List<Person> freePersons; //Список резервных людей
        TimeMachine tMachine;
        MainForm mainForm;
        Game game;
        /// <summary>
        /// Конструктор для формы ангара
        /// </summary>
        /// <param name="hangar">Экземпляр класса ангар</param>
        public FormHangar(Game game, MainForm m)
        {
            InitializeComponent();
            this.hangar = game.Hangar;
            this.game = game;
            this.freePersons = game.ListOfFreePersons;
            mainForm = m;
            tMachine = game.tMachine;
            #region Инициализация списков самолётов
            //Вывод новых самолётов в магазине
            foreach (Plane p in hangar.PlaneShop)
            {
                dgvShopPlanes.Rows.Add(
                    p.name,
                    p.pType,
                    p.carryWeight,
                    p.shopPrice,
                    p.speed,
                    p.rentCost,
                    p.consumption,
                    p.maxDistance,
                    p.maxPersons);
            }
            if ( hangar.ListOfOldPlane.Count == 0)
                GenerateOldPlane();
            //////Вывод поддержанных самолётов в магазине
            foreach (Plane p in hangar.ListOfOldPlane)
            {
                dgvShopOldPlanes.Rows.Add(
                    p.name,
                    p.pType,
                    p.carryWeight,
                    p.shopPrice,
                    p.speed,
                    p.shopPrice / 300,
                    p.consumption,
                    p.maxDistance,
                    p.allDistance / p.speed,
                    p.maxPersons);
            }
            EnemyBuyPlane(null);
            #endregion
        }

        private void btnPurchase_Click(object sender, EventArgs e)
        {
            Plane PlaneForBuy;
            //В дгв вроде всегда выбрано, но на всякий
            try
            {
                if (rbNewPlane.Checked)
                    PlaneForBuy = hangar.PlaneShop[dgvShopPlanes.CurrentRow.Index];
                else PlaneForBuy = hangar.ListOfOldPlane[dgvShopOldPlanes.CurrentRow.Index];
            }
            catch
            {
                MessageBox.Show("Выберите самолёт, который хотите приобрести!");
                return;
            }
            FormPurchase frmPurchase = new FormPurchase(PlaneForBuy);
            //Выбрали - добавляем в ангар и на форму
            if (frmPurchase.ShowDialog() == DialogResult.OK)
            {

                //Создаём объект с нужными параметрами
                //если новый, то.. иначе для поддержанного самолёта
                switch (frmPurchase.Type)
                {
                    case "Покупка":
                        //Например 3 дня делается
                        if (rbNewPlane.Checked)
                            PlaneForBuy = new Plane(PlaneForBuy, OwnType.Buy, 0, 0, 3, PlaneForBuy.maxPersons, 0);
                        else PlaneForBuy = new Plane(PlaneForBuy, OwnType.Buy, 0, 0, 0, PlaneForBuy.maxPersons, Convert.ToUInt32(PlaneForBuy.allDistance));
                            break;
                    case "Аренда":
                        if (rbNewPlane.Checked)
                            PlaneForBuy = new Plane(PlaneForBuy, OwnType.Rent, 0, PlaneForBuy.shopPrice / 300, frmPurchase.Days, PlaneForBuy.maxPersons, 0);
                        else PlaneForBuy = new Plane(PlaneForBuy, OwnType.Rent, 0, PlaneForBuy.shopPrice / 300, frmPurchase.Days, PlaneForBuy.maxPersons, Convert.ToUInt32(PlaneForBuy.allDistance));
                        break;
                    case "Лизинг":
                        if (rbNewPlane.Checked)
                            PlaneForBuy = new Plane(PlaneForBuy, OwnType.Leasing, frmPurchase.MonthPay, 0, frmPurchase.Days, PlaneForBuy.maxPersons, 0);
                        else PlaneForBuy = new Plane(PlaneForBuy, OwnType.Leasing, frmPurchase.MonthPay, 0, frmPurchase.Days, PlaneForBuy.maxPersons, Convert.ToUInt32(PlaneForBuy.allDistance));
                        break;
                }
                //Вызываем метод для приобретения
                PlaneForBuy.curCity = Cities.Москва;
                int answer = hangar.BuyPlane(PlaneForBuy);
                //Если приобретение прошло удачно (денег хватило)
                if (answer == 0)
                {
                    //если поддержанный - удаляем из списка
                    if (rbOldPlane.Checked)
                    {
                        int i;
                        i = hangar.PlaneShop.FindIndex(x => x.name == dgvShopOldPlanes.CurrentRow.Cells["oldName"].Value.ToString());
                        hangar.PlaneShop[i].popul++;
                        hangar.PlaneShop[i].shopPrice+= Convert.ToInt32(hangar.PlaneShop[i].shopPrice * hangar.PlaneShop[i].popul * 0.0003);
                        UpgradeBoard();
                        hangar.ListOfOldPlane.RemoveAt(dgvShopOldPlanes.CurrentRow.Index);
                        dgvShopOldPlanes.Rows.RemoveAt(dgvShopOldPlanes.CurrentRow.Index);
                    }
                    else
                    {
                        hangar.PlaneShop[dgvShopPlanes.CurrentRow.Index].popul++;
                        hangar.PlaneShop[dgvShopPlanes.CurrentRow.Index].shopPrice += Convert.ToInt32(hangar.PlaneShop[dgvShopPlanes.CurrentRow.Index].shopPrice * hangar.PlaneShop[dgvShopPlanes.CurrentRow.Index].popul * 0.0003);
                        UpgradeBoard();
                    }

                    #region Добавление на форму нового самолёта
                    //Добавляем на форму (текущий город - домашний аэропорт)
                    dgvMyPlanes.Rows.Add(
                        PlaneForBuy.name,
                        PlaneForBuy.typeOfOwnership,
                        PlaneForBuy.pType,
                        PlaneForBuy.carryWeight,
                        PlaneForBuy.curCity,
                        PlaneForBuy.speed,
                        PlaneForBuy.lease,
                        PlaneForBuy.consumption,
                        PlaneForBuy.maxDistance,
                        PlaneForBuy.allDistance / PlaneForBuy.speed,
                        PlaneForBuy.persons.Count.ToString() + "/" + PlaneForBuy.maxPersons.ToString(),
                        "Изменить");
                    //Обозначим тип владения, если только купленный - "блокируем"
                    switch (PlaneForBuy.typeOfOwnership)
                    {
                        case OwnType.Buy:
                            if (rbNewPlane.Checked)
                                dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.BackColor = Color.Silver;
                            else dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.BackColor = Color.CornflowerBlue;
                            break;
                        case OwnType.Leasing:
                            dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.BackColor = Color.LightCoral;
                            break;
                        case OwnType.Rent:
                            dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.BackColor = Color.White;
                            break;
                    }
                    #endregion
                    //вызов формы экипажа самолета
                    FormPlaneCrew formPlaneCrew = new FormPlaneCrew(PlaneForBuy, game, mainForm);
                    formPlaneCrew.ShowDialog();
                    //изменение в дгв числа экипажа
                    dgvMyPlanes[dgvMyPlanes.ColumnCount - 2, dgvMyPlanes.RowCount - 1].Value = PlaneForBuy.persons.Count.ToString() + "/" + PlaneForBuy.maxPersons.ToString();
                }

            }
        }

        private void btnSale_Click(object sender, EventArgs e)
        {
            Plane PlaneForSale;
            //Если вдруг продавать нечего (опять же, если ничего не выбрано)
            try
            {
                //Самолёт может быть недоступен для выбора(ещё не пришёл), 
                //поэтому проверка выбранной строки
                if (dgvMyPlanes.CurrentRow.Selected)
                {
                    PlaneForSale = hangar.PlaneList[dgvMyPlanes.CurrentRow.Index];
                }
                else
                {
                    MessageBox.Show("Выберите самолёт, который хотите продать/отказаться!");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Выберите самолёт, который хотите продать/отказаться!");
                return;
            }
            //Выбрали - убираем из ангара и с формы
            FormSale frmSale = new FormSale(PlaneForSale);
            if (frmSale.ShowDialog() == DialogResult.OK)
            {
                SellPlane(PlaneForSale);
                FillBoard();
            }
        }

        /// <summary>
        /// Продажа/отказ от самолёта
        /// </summary>
        /// <param name="p">Продаваемый самолёт / самолёт, от которого отказываются</param>
        public void SellPlane(Plane p)
        {
            hangar.InListOfFreePersons(p);
            game.DeleteFlightsForPlane(p);
            if (p.allDistance != 0)
            {
                p.rentCost = p.shopPrice / 300;
                hangar.ListOfOldPlane.Add(p);

                dgvMyPlanes.Rows.RemoveAt(dgvMyPlanes.CurrentRow.Index);
            }
            else
            {
                dgvMyPlanes.Rows.RemoveAt(dgvMyPlanes.CurrentRow.Index);
            }
            if (p.typeOfOwnership == OwnType.Buy)
            {
                //Продажа зависит от налёта часов
                mainForm.RefreshBalance("Продажа самолета", +(Convert.ToInt32(p.shopPrice - p.shopPrice * ((double)p.allDistance / p.speed) * 0.00007) / 2));
            }
            hangar.PlaneList.Remove(p);
            if (p.typeOfOwnership == OwnType.Leasing)
            {
                tMachine.RemoveFromList(hangar.MonthPay, p);
                if (p.allDistance == 0)
                {
                    mainForm.RefreshRaiting(-7);
                }
                if(p.allDistance/p.speed>0&& p.allDistance/p.speed<3000)
                {
                    mainForm.RefreshRaiting(-2);
                }
            }
            else
            {
                if (p.typeOfOwnership == OwnType.Rent)
                {
                    tMachine.RemoveFromList(hangar.AddDays, p);
                }
            }
        }

        private void dgvMyPlanes_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvMyPlanes.CurrentRow != null && dgvMyPlanes.CurrentRow.Index < hangar.PlaneList.Count)
                //Для запрета выбора самолёта, который ещё не пришёл
                if (hangar.PlaneList[dgvMyPlanes.CurrentRow.Index].typeOfOwnership == OwnType.Buy
                    && hangar.PlaneList[dgvMyPlanes.CurrentRow.Index].lease > 0
                    || hangar.PlaneList[dgvMyPlanes.CurrentRow.Index].IsFlight)
                {
                    dgvMyPlanes.CurrentRow.Selected = false;
                }
        }

        private void FormHangar_Shown(object sender, EventArgs e)
        {
            if (hangar.PlaneList != null)
            {
                if (dgvMyPlanes.Rows.Count != 0) dgvMyPlanes.Rows.Clear();
                foreach (Plane p in hangar.PlaneList)
                {
                    string curCity;
                    if (!p.IsFlight)
                    {
                        curCity = Convert.ToString(p.curCity);
                    }
                    else
                    {
                        curCity = "В пути";
                    }
                    dgvMyPlanes.Rows.Add(
                        p.name,
                        p.typeOfOwnership,
                        p.pType,
                        p.carryWeight,
                        curCity,
                        p.speed,
                        p.lease,
                        p.consumption,
                        p.maxDistance,
                        p.allDistance / p.speed,
                        p.persons.Count.ToString() + "/" + p.maxPersons.ToString(),
                        curCity == "В пути" ? "В полете" : "Изменить");
                    //Выделим разные типы владения
                    switch (p.typeOfOwnership)
                    {
                        case OwnType.Buy:
                            dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.BackColor = Color.CornflowerBlue;
                            break;
                        case OwnType.Leasing:
                            dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.BackColor = Color.LightCoral;
                            break;
                        case OwnType.Rent:
                            dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.BackColor = Color.White;
                            break;
                    }
                    //"Блокируем" самолёт, который ещё не поступил, т.е строится
                    if (p.typeOfOwnership == OwnType.Buy && p.lease > 0)
                    {
                        dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.BackColor = Color.Silver;
                        // блокируем кнопку изменения экипажа
                        dgvMyPlanes[dgvMyPlanes.ColumnCount - 1, dgvMyPlanes.RowCount - 1].Value = "";
                    }

                }
            }
        }

        private void rbNewPlane_CheckedChanged(object sender, EventArgs e)
        {
            gbOldPlane.Visible = false;
            gbNewPlane.Visible = true;
        }

        private void rbOldPlane_CheckedChanged(object sender, EventArgs e)
        {
            gbNewPlane.Visible = false;
            gbOldPlane.Visible = true;
        }

        /// <summary>
        /// Ежедневная проверка наших поддержанных самолётов
        /// </summary>
        public void CheckOld(object e)
        {
            int old = 0;
            if(hangar.PlaneList.Count==0)
            {
                game.PlayerRaiting -= 7;
            }
            foreach (Plane p in hangar.PlaneList)
            {
                if (p.allDistance / p.speed > 7000) old++;
            }
            game.PlayerRaiting -= game.PlayerRaiting * 0.001 * old;
            game.PlayerRaiting = Math.Round(game.PlayerRaiting, 3);
            mainForm.RefreshRaiting();
            game.tMachine.AddToList(game.tMachine.MainTime.AddDays(1), CheckOld, null);
        }
        /// <summary>
        /// Обновление списка поддержанных самолетов
        /// </summary>
        public void RefreshListsPlane()
        {
            dgvShopOldPlanes.Rows.Clear();
            FillBoard();
        }
        /// <summary>
        /// Вывод самолётов на форму
        /// </summary>
        /// <param name="f">Рейс</param>
        public void FillBoard()
        {
            dgvShopOldPlanes.Rows.Clear();
            //////Вывод поддержанных самолётов в магазине
            foreach (Plane p in hangar.ListOfOldPlane)
            {
                dgvShopOldPlanes.Rows.Add(
                    p.name,
                    p.pType,
                    p.carryWeight,
                    p.shopPrice,
                    p.speed,
                    p.shopPrice / 300,
                    p.consumption,
                    p.maxDistance,
                    p.allDistance / p.speed,
                    p.maxPersons);
            }
        }

        /// <summary>
        /// обновление списка новых самолётов
        /// </summary>
        public void UpgradeBoard()
        {
            dgvShopPlanes.Rows.Clear();
            foreach (Plane p in hangar.PlaneShop)
            {
                dgvShopPlanes.Rows.Add(
                    p.name,
                    p.pType,
                    p.carryWeight,
                    p.shopPrice,
                    p.speed,
                    p.rentCost,
                    p.consumption,
                    p.maxDistance,
                    p.maxPersons);
            }
        }


        /// <summary>
        /// Отсчёт дней до появления поддержанных самолётов на рынке
        /// </summary>
        /// <param name="e">Самолёт (Plane), для которого будет отсчёт</param>
        public void AddOldPlane(object e)
        {
            List<Plane> p = (List<Plane>)e;
            p.Clear();
            int n = Game.Random.Next(3, 7);
            for (int i = 0; i < n; i++)
            {
                Plane pl = new Plane(hangar.PlaneShop[Game.Random.Next(0, hangar.PlaneShop.Count)]);
                pl.rentCost = pl.shopPrice / 300;
                hangar.ListOfOldPlane.Add(pl);
            }
            tMachine.AddToList(tMachine.MainTime.AddDays(3), AddOldPlane, p);
            RefreshListsPlane();
        }

        /// <summary>
        /// Генерация старых самолётов
        /// </summary>
        public void GenerateOldPlane()
        {
            int n = Game.Random.Next(5, 10);
            for (int i = 0; i < n; i++)
            {
                Plane p = new Plane(hangar.PlaneShop[Game.Random.Next(0, hangar.PlaneShop.Count)]);
                hangar.ListOfOldPlane.Add(p);
            }
            tMachine.AddToList(tMachine.MainTime.AddDays(3), AddOldPlane, hangar.ListOfOldPlane);
        }

        private void dgvMyPlanes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (IsHeaderButtonCell(e))
            {
                Plane p = hangar.PlaneList[dgvMyPlanes.CurrentRow.Index];
                //вызов формы экипажа
                FormPlaneCrew formPlaneCrew = new FormPlaneCrew(p, game, mainForm);
                formPlaneCrew.ShowDialog();
                //изменение числа людей экипажа в dgv
                dgvMyPlanes[e.ColumnIndex - 1, e.RowIndex].Value = p.persons.Count.ToString() + "/" + p.maxPersons.ToString();
            }
        }
        /// <summary>
        /// Проверка является ли выбранная ячейка DGV кнопкой
        /// </summary>
        /// <param name="cellEvent">Событие нажатия на DGV (DataGridViewCellEventArgs)</param>
        /// <returns>true - если в ячейке кнопка </returns>
        private bool IsHeaderButtonCell(DataGridViewCellEventArgs cellEvent)
        {
            if (cellEvent.RowIndex != -1 &&
              dgvMyPlanes.Columns[cellEvent.ColumnIndex] is DataGridViewButtonColumn &&
                (string)dgvMyPlanes[cellEvent.ColumnIndex, cellEvent.RowIndex].Value == "Изменить")
            { return true; }
            else { return (false); }
        }



        /// <summary>
        /// Ежедневная проверка поддержанных самолётов конкурента
        /// </summary>
        public void CheckOldEnemy(object e)
        {

            int old = 0;
            foreach (Plane p in hangar.ListOfPlaneEnemy)
            {
                if (p.allDistance / p.speed > 7000) old++;
            }
            game.EnemyRaiting -= game.EnemyRaiting * 0.001 * old;
            game.EnemyRaiting = Math.Round(game.EnemyRaiting, 3);
            mainForm.RefreshRaiting();
            game.tMachine.AddToList(game.tMachine.MainTime.AddDays(1), CheckOldEnemy, null);
        }
        /// <summary>
        /// Конкурент покупает самолёты
        /// </summary>
        public void EnemyBuyPlane(object e)
        {
            int n =  Game.Random.Next(1, 3);
            //если поддержанный
            if (hangar.ListOfOldPlane.Count>0 && n==1)
            {
                int i = Game.Random.Next(0, hangar.ListOfOldPlane.Count);
                int name = hangar.PlaneShop.FindIndex(x => x.name == dgvShopOldPlanes.Rows[i].Cells["oldName"].Value.ToString());
                if (game.EnemyBalance - hangar.ListOfOldPlane[i].shopPrice > 0)
                {
                    hangar.PlaneShop[name].popul++;
                    hangar.ListOfPlaneEnemy.Add(hangar.ListOfOldPlane[i]);
                    game.EnemyBalance -= hangar.ListOfOldPlane[i].shopPrice;
                    UpgradeBoard();
                    hangar.ListOfOldPlane.RemoveAt(i);
                    dgvShopOldPlanes.Rows.RemoveAt(i);
                }
            }
            //если новый
            else
            {
                int i = Game.Random.Next(0, 18);
                if (game.EnemyBalance - hangar.PlaneShop[i].shopPrice > 0)
                {
                    hangar.PlaneShop[i].popul++;
                    hangar.ListOfPlaneEnemy.Add(hangar.PlaneShop[i]);
                    game.EnemyRaiting += 10;
                    UpgradeBoard();
                    game.EnemyBalance -= hangar.PlaneShop[i].shopPrice;
                }
            }
            tMachine.AddToList(tMachine.MainTime.AddDays(2 + Game.Random.Next(15)), EnemyBuyPlane, null);
        }

        /// <summary>
        /// Конкурент продаёт самолёты
        /// </summary>
        public void EnemySalePlane(object e)
        {

            int i = -1;
            foreach (Plane p in hangar.ListOfPlaneEnemy)
            {
                if (p.allDistance / p.speed > 1000 && p.allDistance / p.speed < 4000)
                {
                    i = hangar.ListOfPlaneEnemy.IndexOf(p);
                }
            }
            if (i != -1)
            {
                Plane p = hangar.ListOfPlaneEnemy[i];
                game.EnemyBalance += (Convert.ToInt32(p.shopPrice - p.shopPrice * ((double)p.allDistance / p.speed) * 0.00007) / 2);                
                hangar.ListOfOldPlane.Add(p);
                hangar.ListOfPlaneEnemy.Remove(p);
                RefreshListsPlane();
            }
            tMachine.AddToList(tMachine.MainTime.AddDays(4 + Game.Random.Next(15)), EnemySalePlane, null);
        }
    }
}
