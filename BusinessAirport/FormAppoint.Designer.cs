﻿namespace BusinessAirport
{
    partial class FormAppoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvMyPlanes = new System.Windows.Forms.DataGridView();
            this.NameOfPlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.curCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consumption = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hourPlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbTimeOfFlight = new System.Windows.Forms.GroupBox();
            this.lbCompleteBefore = new System.Windows.Forms.Label();
            this.rbEmptyFlight = new System.Windows.Forms.RadioButton();
            this.rbFlight = new System.Windows.Forms.RadioButton();
            this.dtpDateDep = new System.Windows.Forms.DateTimePicker();
            this.lbProfitTime = new System.Windows.Forms.Label();
            this.lbProfitText = new System.Windows.Forms.Label();
            this.lbTimeOfFinish = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCansel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyPlanes)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gbTimeOfFlight.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvMyPlanes
            // 
            this.dgvMyPlanes.AllowUserToAddRows = false;
            this.dgvMyPlanes.AllowUserToDeleteRows = false;
            this.dgvMyPlanes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMyPlanes.BackgroundColor = System.Drawing.Color.White;
            this.dgvMyPlanes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMyPlanes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameOfPlane,
            this.curCity,
            this.distance,
            this.consumption,
            this.hourPlane});
            this.dgvMyPlanes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMyPlanes.Location = new System.Drawing.Point(3, 19);
            this.dgvMyPlanes.MultiSelect = false;
            this.dgvMyPlanes.Name = "dgvMyPlanes";
            this.dgvMyPlanes.ReadOnly = true;
            this.dgvMyPlanes.RowHeadersVisible = false;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dgvMyPlanes.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMyPlanes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMyPlanes.Size = new System.Drawing.Size(485, 357);
            this.dgvMyPlanes.TabIndex = 1;
            this.dgvMyPlanes.SelectionChanged += new System.EventHandler(this.dgvMyPlanes_SelectionChanged);
            // 
            // NameOfPlane
            // 
            this.NameOfPlane.HeaderText = "Назавние";
            this.NameOfPlane.Name = "NameOfPlane";
            this.NameOfPlane.ReadOnly = true;
            // 
            // curCity
            // 
            this.curCity.HeaderText = "Местоположение";
            this.curCity.Name = "curCity";
            this.curCity.ReadOnly = true;
            // 
            // distance
            // 
            this.distance.HeaderText = "Расстояние до пункта отправления";
            this.distance.Name = "distance";
            this.distance.ReadOnly = true;
            // 
            // consumption
            // 
            this.consumption.HeaderText = "Расход на 100 км";
            this.consumption.Name = "consumption";
            this.consumption.ReadOnly = true;
            this.consumption.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // hourPlane
            // 
            this.hourPlane.HeaderText = "Часов в полете";
            this.hourPlane.Name = "hourPlane";
            this.hourPlane.ReadOnly = true;
            this.hourPlane.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvMyPlanes);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(491, 379);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Назначить самолёт";
            // 
            // gbTimeOfFlight
            // 
            this.gbTimeOfFlight.Controls.Add(this.lbCompleteBefore);
            this.gbTimeOfFlight.Controls.Add(this.rbEmptyFlight);
            this.gbTimeOfFlight.Controls.Add(this.rbFlight);
            this.gbTimeOfFlight.Controls.Add(this.dtpDateDep);
            this.gbTimeOfFlight.Controls.Add(this.lbProfitTime);
            this.gbTimeOfFlight.Controls.Add(this.lbProfitText);
            this.gbTimeOfFlight.Controls.Add(this.lbTimeOfFinish);
            this.gbTimeOfFlight.Controls.Add(this.label3);
            this.gbTimeOfFlight.Controls.Add(this.label2);
            this.gbTimeOfFlight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbTimeOfFlight.ForeColor = System.Drawing.Color.White;
            this.gbTimeOfFlight.Location = new System.Drawing.Point(509, 13);
            this.gbTimeOfFlight.Name = "gbTimeOfFlight";
            this.gbTimeOfFlight.Size = new System.Drawing.Size(220, 230);
            this.gbTimeOfFlight.TabIndex = 3;
            this.gbTimeOfFlight.TabStop = false;
            this.gbTimeOfFlight.Text = "Назначить время рейса";
            // 
            // lbCompleteBefore
            // 
            this.lbCompleteBefore.AutoSize = true;
            this.lbCompleteBefore.Location = new System.Drawing.Point(9, 151);
            this.lbCompleteBefore.Name = "lbCompleteBefore";
            this.lbCompleteBefore.Size = new System.Drawing.Size(45, 16);
            this.lbCompleteBefore.TabIndex = 11;
            this.lbCompleteBefore.Text = "label1";
            // 
            // rbEmptyFlight
            // 
            this.rbEmptyFlight.AutoSize = true;
            this.rbEmptyFlight.Location = new System.Drawing.Point(12, 48);
            this.rbEmptyFlight.Name = "rbEmptyFlight";
            this.rbEmptyFlight.Size = new System.Drawing.Size(156, 20);
            this.rbEmptyFlight.TabIndex = 10;
            this.rbEmptyFlight.Text = "Назначить перелет";
            this.rbEmptyFlight.UseVisualStyleBackColor = true;
            // 
            // rbFlight
            // 
            this.rbFlight.AutoSize = true;
            this.rbFlight.Checked = true;
            this.rbFlight.Location = new System.Drawing.Point(12, 22);
            this.rbFlight.Name = "rbFlight";
            this.rbFlight.Size = new System.Drawing.Size(132, 20);
            this.rbFlight.TabIndex = 9;
            this.rbFlight.TabStop = true;
            this.rbFlight.Text = "Назначить рейс";
            this.rbFlight.UseVisualStyleBackColor = true;
            this.rbFlight.CheckedChanged += new System.EventHandler(this.rbFlight_CheckedChanged);
            // 
            // dtpDateDep
            // 
            this.dtpDateDep.CustomFormat = "dd.MM.yy # HH:mm";
            this.dtpDateDep.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.dtpDateDep.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateDep.Location = new System.Drawing.Point(11, 124);
            this.dtpDateDep.MinDate = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dtpDateDep.Name = "dtpDateDep";
            this.dtpDateDep.Size = new System.Drawing.Size(199, 24);
            this.dtpDateDep.TabIndex = 8;
            this.dtpDateDep.ValueChanged += new System.EventHandler(this.dtpDateDep_ValueChanged);
            // 
            // lbProfitTime
            // 
            this.lbProfitTime.AutoSize = true;
            this.lbProfitTime.Location = new System.Drawing.Point(129, 76);
            this.lbProfitTime.Name = "lbProfitTime";
            this.lbProfitTime.Size = new System.Drawing.Size(39, 16);
            this.lbProfitTime.TabIndex = 7;
            this.lbProfitTime.Text = "утро";
            // 
            // lbProfitText
            // 
            this.lbProfitText.AutoSize = true;
            this.lbProfitText.Location = new System.Drawing.Point(9, 76);
            this.lbProfitText.Name = "lbProfitText";
            this.lbProfitText.Size = new System.Drawing.Size(118, 16);
            this.lbProfitText.TabIndex = 6;
            this.lbProfitText.Text = "Выгодное время:";
            // 
            // lbTimeOfFinish
            // 
            this.lbTimeOfFinish.BackColor = System.Drawing.Color.White;
            this.lbTimeOfFinish.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbTimeOfFinish.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbTimeOfFinish.ForeColor = System.Drawing.Color.Black;
            this.lbTimeOfFinish.Location = new System.Drawing.Point(11, 193);
            this.lbTimeOfFinish.Name = "lbTimeOfFinish";
            this.lbTimeOfFinish.Size = new System.Drawing.Size(199, 23);
            this.lbTimeOfFinish.TabIndex = 5;
            this.lbTimeOfFinish.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Дата и время прибытия:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Дата и время вылета:";
            // 
            // btnCansel
            // 
            this.btnCansel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnCansel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCansel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCansel.ForeColor = System.Drawing.Color.White;
            this.btnCansel.Location = new System.Drawing.Point(509, 356);
            this.btnCansel.Name = "btnCansel";
            this.btnCansel.Size = new System.Drawing.Size(220, 32);
            this.btnCansel.TabIndex = 8;
            this.btnCansel.Text = "Отмена";
            this.btnCansel.UseVisualStyleBackColor = false;
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOK.ForeColor = System.Drawing.Color.White;
            this.btnOK.Location = new System.Drawing.Point(509, 316);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(220, 31);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "Назначить";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(509, 249);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(220, 61);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Обозначения";
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(11, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(199, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Самолёт не в нужном городе";
            // 
            // FormAppoint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.ClientSize = new System.Drawing.Size(740, 400);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnCansel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gbTimeOfFlight);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAppoint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Назначение самолёта на рейс";
            this.Load += new System.EventHandler(this.FormAppoint_Load);
            this.Shown += new System.EventHandler(this.FormAppoint_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyPlanes)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.gbTimeOfFlight.ResumeLayout(false);
            this.gbTimeOfFlight.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }
        
        #endregion
        private System.Windows.Forms.DataGridView dgvMyPlanes;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbTimeOfFlight;
        private System.Windows.Forms.Label lbTimeOfFinish;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCansel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbProfitTime;
        private System.Windows.Forms.Label lbProfitText;
        private System.Windows.Forms.DateTimePicker dtpDateDep;
        private System.Windows.Forms.RadioButton rbEmptyFlight;
        private System.Windows.Forms.RadioButton rbFlight;
        private System.Windows.Forms.Label lbCompleteBefore;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameOfPlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn curCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn distance;
        private System.Windows.Forms.DataGridViewTextBoxColumn consumption;
        private System.Windows.Forms.DataGridViewTextBoxColumn hourPlane;
    }
}