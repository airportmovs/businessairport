﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAirport
{
    [Serializable]
    public class EventElem
    {
        public delegate void MethodContainer(object e); // прообраз исполнимого метода
        public MethodContainer SomeMethod; // сслыка на метод
        public object param;

        /// <summary>
        /// Элемент списка ивентов
        /// </summary>
        /// <param name="t">Время срабатывания</param>
        /// <param name="mc">Вызываемый метод</param>
        public EventElem(MethodContainer mc, object param)
        {
            SomeMethod = mc;
            this.param = param;
        }
    }
}
