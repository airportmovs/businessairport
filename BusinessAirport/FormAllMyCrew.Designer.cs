﻿namespace BusinessAirport
{
    partial class FormAllMyCrew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnRecruit = new System.Windows.Forms.Button();
            this.dgvAllPersons = new System.Windows.Forms.DataGridView();
            this.Salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Plane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mood = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dismiss = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllPersons)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRecruit
            // 
            this.btnRecruit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRecruit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnRecruit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecruit.ForeColor = System.Drawing.Color.White;
            this.btnRecruit.Location = new System.Drawing.Point(14, 294);
            this.btnRecruit.Name = "btnRecruit";
            this.btnRecruit.Size = new System.Drawing.Size(640, 32);
            this.btnRecruit.TabIndex = 1;
            this.btnRecruit.Text = "Нанять";
            this.btnRecruit.UseVisualStyleBackColor = false;
            this.btnRecruit.Click += new System.EventHandler(this.btnRecruit_Click);
            // 
            // dgvAllPersons
            // 
            this.dgvAllPersons.AllowUserToAddRows = false;
            this.dgvAllPersons.AllowUserToDeleteRows = false;
            this.dgvAllPersons.AllowUserToResizeRows = false;
            this.dgvAllPersons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAllPersons.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAllPersons.BackgroundColor = System.Drawing.Color.White;
            this.dgvAllPersons.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvAllPersons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllPersons.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Salary,
            this.Plane,
            this.inFlight,
            this.mood,
            this.dismiss});
            this.dgvAllPersons.Location = new System.Drawing.Point(14, 14);
            this.dgvAllPersons.MultiSelect = false;
            this.dgvAllPersons.Name = "dgvAllPersons";
            this.dgvAllPersons.RowHeadersVisible = false;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(92)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgvAllPersons.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAllPersons.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAllPersons.Size = new System.Drawing.Size(640, 273);
            this.dgvAllPersons.TabIndex = 4;
            this.dgvAllPersons.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAllPersons_CellContentClick);
            this.dgvAllPersons.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAllPersons_CellValueChanged);
            this.dgvAllPersons.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvAllPersons_EditingControlShowing);
            this.dgvAllPersons.SelectionChanged += new System.EventHandler(this.dgvAllPersons_SelectionChanged);
            this.dgvAllPersons.Click += new System.EventHandler(this.dgvAllPersons_Click);
            // 
            // Salary
            // 
            this.Salary.HeaderText = "Зарплата";
            this.Salary.Name = "Salary";
            // 
            // Plane
            // 
            this.Plane.HeaderText = "Самолет";
            this.Plane.Name = "Plane";
            this.Plane.ReadOnly = true;
            // 
            // inFlight
            // 
            this.inFlight.HeaderText = "В полете/нет";
            this.inFlight.Name = "inFlight";
            this.inFlight.ReadOnly = true;
            // 
            // mood
            // 
            this.mood.HeaderText = "Настроение";
            this.mood.Name = "mood";
            this.mood.ReadOnly = true;
            // 
            // dismiss
            // 
            this.dismiss.HeaderText = "Увольнение";
            this.dismiss.Name = "dismiss";
            this.dismiss.ReadOnly = true;
            // 
            // FormAllMyCrew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.ClientSize = new System.Drawing.Size(671, 340);
            this.Controls.Add(this.dgvAllPersons);
            this.Controls.Add(this.btnRecruit);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimizeBox = false;
            this.Name = "FormAllMyCrew";
            this.Text = "Все работники";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAllMyCrew_FormClosing);
            this.Shown += new System.EventHandler(this.FormAllMyCrew_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllPersons)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnRecruit;
        private System.Windows.Forms.DataGridView dgvAllPersons;
        private System.Windows.Forms.DataGridViewTextBoxColumn Salary;
        private System.Windows.Forms.DataGridViewTextBoxColumn Plane;
        private System.Windows.Forms.DataGridViewTextBoxColumn inFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn mood;
        private System.Windows.Forms.DataGridViewButtonColumn dismiss;
    }
}