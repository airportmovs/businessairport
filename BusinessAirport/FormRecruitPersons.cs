﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class FormRecruitPersons : Form
    {
        Game game; // экземпляр класса гэйм
        Random random = new Random(); 
        bool isFromAllMyCrew; //флажок - если вызов из формы AllMyCrew - true
        Plane plane; // самолет
        FormAllMyCrew formAllMyCrew; //эксзепляр класса AllMyCrew

        /// <summary>
        /// Конструктор для вызова из формы AllMyCrew 
        /// </summary>
        /// <param name="game"> Экземпляр класса game</param>
        /// <param name="formAllMyCrew"> форма AllMyCrew</param>
        public FormRecruitPersons(Game game, FormAllMyCrew formAllMyCrew) 
        {
            InitializeComponent();
            this.game = game;
            isFromAllMyCrew = true;
            this.formAllMyCrew = formAllMyCrew;
        }

        /// <summary>
        /// конструктор для вызова из Ангара
        /// </summary>
        /// <param name="game">Экземпляр класса game </param>
        /// <param name="plane">Самолет, экипаж которого надо изменить</param>
        public FormRecruitPersons(Game game, Plane plane)
        {
            InitializeComponent();
            this.game = game;
            this.plane = plane;
            isFromAllMyCrew = false;
        }

        /// <summary>
        /// Начальная инициализация элементов формы
        /// </summary>
        private void Open()
        {
            nudSalary.Minimum = 0;
            nudSalary.Value = 0;
            if (isFromAllMyCrew)
            {
                nudCountPerson.Value = 0;
                nudCountPerson.Maximum = 1000;
            }
            else
            {
                nudCountPerson.Value = nudCountPerson.Maximum = plane.maxPersons - plane.persons.Count;
            }
            lblResult.Text = "Я жду, пиши...";
        }

        private void btnRecruit_Click(object sender, EventArgs e)
        {
                //если введеная зарплата меньше чем у конкурента, никто не приходит
                if (nudSalary.Value < game.EnemyAverSalary)
                {
                    lblResult.Text = "К вам никто не пришел...";
                }
                else
                {
                    GeneratePersons();
                    if (isFromAllMyCrew) formAllMyCrew.FillDGV();
                }
            //}
            
        }

        /// <summary>
        /// Генерация людей при нанятии
        /// </summary>
        private void GeneratePersons()
        {
            int count = 0;
            if (nudCountPerson.Value != 0)
            {
                
                //если введенная зарплата больше средней в 2 раза, то добавятся все люди
                if (nudSalary.Value > game.EnemyAverSalary * 2)
                {
                    count = (int)nudCountPerson.Value;
                    lblResult.Text = "К вам пришло столько людей, сколько Вы хотели, а именно " + count.ToString();
                    CreateNewPerson(count, isFromAllMyCrew ? game.ListOfFreePersons : plane.persons);
                }
                else
                {
                    count = (int)random.Next(1, (int)nudCountPerson.Value);
                    lblResult.Text = "Оп-па, несколько людишек согласилось, а именно " + count.ToString();
                    CreateNewPerson(count, isFromAllMyCrew ? game.ListOfFreePersons : plane.persons);
                    nudSalary.Minimum = nudSalary.Value + 1;
                    btnRecruit.Enabled = false;
                }
                
            }
        }

        /// <summary>
        /// Создание и добавление людей в список
        /// </summary>
        /// <param name="count">Число сгенерированных людей</param>
        /// <param name="persons">Список куда люди будут добавляться</param>
        private void CreateNewPerson(int count, List<Person> persons)
        {
            for (int i = 0; i < count; i++)
            {
                persons.Add(new Person((int)nudSalary.Value, game.tMachine.MainTime));
                if (!isFromAllMyCrew) nudCountPerson.Maximum = plane.maxPersons - plane.persons.Count;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormRecruitPersons_Shown(object sender, EventArgs e)
        {
            Open();
        }

        private void nudSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            btnRecruit.Enabled = true;
        }

        private void nudSalary_ValueChanged(object sender, EventArgs e)
        {
            btnRecruit.Enabled = true;
        }
    }
}
