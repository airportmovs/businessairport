﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class FormPurchase : Form
    {
        Plane plane;
        //Тип покупки
        public string Type
        {
            get { return cbType.Text; }
        }
        //На сколько дней аренда
        public int Days
        {
            get { return Convert.ToInt32(numDaysOrMonth.Value); }
        }
        //Месячный платёж лизинга
        public int MonthPay
        {
            get { return Month(); }
        }
        //Первоначальный взнос
        public int StartPay
        {
            get { return StartLease(); }
        }

        public FormPurchase(Plane PlaneForBuy)
        {
            InitializeComponent();
            plane = PlaneForBuy;
            //GroupBox для ввода количества дней аренды или месяцев выплаты лизинга
            gbDaysOrMonth.Visible = false;
            cbType.SelectedIndex = 0;
        }

        //Подсчёт месячного платежа
        public int Month()
        {
            //Ну вроде как лизинг (например) на 20 проц дороже обычной цены
            //Считаем цену в месяц
            return Convert.ToInt32(((plane.shopPrice + plane.shopPrice * 0.2) 
                                    - (plane.shopPrice + plane.shopPrice * 0.2) * 0.2) 
                                    / Convert.ToDouble(numDaysOrMonth.Value));
        }
        //Подсчёт первоначального взноса
        public int StartLease()
        {
            //Потому что все цены сделали инт
            return Convert.ToInt32((plane.shopPrice + plane.shopPrice * 0.2) * 0.2);
        }
        //Для изменения формы при выборе другого вида приобретения
        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelName.Text = (plane.name + " " + plane.pType);
            switch (cbType.SelectedIndex)
            {
                case 0:
                    gbDaysOrMonth.Visible = false;
                    lbPrice.Text = ("Цена: "+ plane.shopPrice);
                    lbType.Text = "купить";
                    lbMonthPay.Visible = false;
                    break;
                case 1:
                    gbDaysOrMonth.Visible = true;
                    lbDaysOrMonth.Text = "Введите количество дней аренды";
                    lbType.Text = "взять в аренду";
                    lbMonthPay.Visible = false;
                    lbPrice.Text = ("Цена: " + plane.shopPrice / 300 * Convert.ToInt32(numDaysOrMonth.Value));
                    break;
                case 2:
                    gbDaysOrMonth.Visible = true;
                    lbDaysOrMonth.Text = "Введите количество месяцев лизинга";
                    lbType.Text = "взять в лизинг";
                    lbMonthPay.Visible = true;
                    lbMonthPay.Text = "Ежемесячный платёж: " + MonthPay;
                    lbPrice.Text = "Первоначальный взнос: " + StartPay;
                    break;
            }
        }
        //Чтобы цена на форме менялась и при вводе дней/месяцев с клавиатуры
        private void numDaysOrMonth_KeyUp(object sender, KeyEventArgs e)
        {
            cbType_SelectedIndexChanged(sender, e);
        }
    }
}
