﻿namespace BusinessAirport
{
    partial class FormAdvert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbAdv = new System.Windows.Forms.GroupBox();
            this.cbBanner = new System.Windows.Forms.CheckBox();
            this.cbTV = new System.Windows.Forms.CheckBox();
            this.cbPaper = new System.Windows.Forms.CheckBox();
            this.cbRadio = new System.Windows.Forms.CheckBox();
            this.btGo = new System.Windows.Forms.Button();
            this.lbCost = new System.Windows.Forms.Label();
            this.lbPrice = new System.Windows.Forms.Label();
            this.gbSalt = new System.Windows.Forms.GroupBox();
            this.cbAntiAdv = new System.Windows.Forms.CheckBox();
            this.cbStrike = new System.Windows.Forms.CheckBox();
            this.btGoC = new System.Windows.Forms.Button();
            this.lbCostC = new System.Windows.Forms.Label();
            this.lbPric = new System.Windows.Forms.Label();
            this.gbAdv.SuspendLayout();
            this.gbSalt.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbAdv
            // 
            this.gbAdv.Controls.Add(this.cbBanner);
            this.gbAdv.Controls.Add(this.cbTV);
            this.gbAdv.Controls.Add(this.cbPaper);
            this.gbAdv.Controls.Add(this.cbRadio);
            this.gbAdv.Controls.Add(this.btGo);
            this.gbAdv.Controls.Add(this.lbCost);
            this.gbAdv.Controls.Add(this.lbPrice);
            this.gbAdv.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbAdv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAdv.ForeColor = System.Drawing.Color.White;
            this.gbAdv.Location = new System.Drawing.Point(0, 0);
            this.gbAdv.Name = "gbAdv";
            this.gbAdv.Size = new System.Drawing.Size(366, 195);
            this.gbAdv.TabIndex = 0;
            this.gbAdv.TabStop = false;
            this.gbAdv.Text = "Реклама для имиджа";
            // 
            // cbBanner
            // 
            this.cbBanner.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbBanner.AutoSize = true;
            this.cbBanner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbBanner.Image = global::BusinessAirport.Properties.Resources.banner;
            this.cbBanner.Location = new System.Drawing.Point(273, 25);
            this.cbBanner.Name = "cbBanner";
            this.cbBanner.Size = new System.Drawing.Size(81, 105);
            this.cbBanner.TabIndex = 14;
            this.cbBanner.Text = "Баннер";
            this.cbBanner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbBanner.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbBanner.UseVisualStyleBackColor = true;
            this.cbBanner.CheckedChanged += new System.EventHandler(this.cbBanner_CheckedChanged);
            // 
            // cbTV
            // 
            this.cbTV.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbTV.AutoSize = true;
            this.cbTV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbTV.Image = global::BusinessAirport.Properties.Resources.tv;
            this.cbTV.Location = new System.Drawing.Point(186, 25);
            this.cbTV.Name = "cbTV";
            this.cbTV.Size = new System.Drawing.Size(81, 105);
            this.cbTV.TabIndex = 13;
            this.cbTV.Text = "ТВ";
            this.cbTV.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbTV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbTV.UseVisualStyleBackColor = true;
            this.cbTV.CheckedChanged += new System.EventHandler(this.cbTV_CheckedChanged);
            // 
            // cbPaper
            // 
            this.cbPaper.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbPaper.AutoSize = true;
            this.cbPaper.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbPaper.Image = global::BusinessAirport.Properties.Resources.newspaper;
            this.cbPaper.Location = new System.Drawing.Point(99, 25);
            this.cbPaper.Name = "cbPaper";
            this.cbPaper.Size = new System.Drawing.Size(81, 105);
            this.cbPaper.TabIndex = 12;
            this.cbPaper.Text = "Газета";
            this.cbPaper.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbPaper.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbPaper.UseVisualStyleBackColor = true;
            this.cbPaper.CheckedChanged += new System.EventHandler(this.cbPaper_CheckedChanged);
            // 
            // cbRadio
            // 
            this.cbRadio.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbRadio.AutoSize = true;
            this.cbRadio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cbRadio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbRadio.Image = global::BusinessAirport.Properties.Resources.radio;
            this.cbRadio.Location = new System.Drawing.Point(12, 25);
            this.cbRadio.Name = "cbRadio";
            this.cbRadio.Size = new System.Drawing.Size(81, 105);
            this.cbRadio.TabIndex = 11;
            this.cbRadio.Text = "Радио";
            this.cbRadio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbRadio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbRadio.UseVisualStyleBackColor = true;
            this.cbRadio.CheckedChanged += new System.EventHandler(this.cbRadio_CheckedChanged);
            // 
            // btGo
            // 
            this.btGo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btGo.Location = new System.Drawing.Point(230, 154);
            this.btGo.Name = "btGo";
            this.btGo.Size = new System.Drawing.Size(124, 32);
            this.btGo.TabIndex = 6;
            this.btGo.Text = "Заказать";
            this.btGo.UseVisualStyleBackColor = false;
            this.btGo.Click += new System.EventHandler(this.btGo_Click);
            // 
            // lbCost
            // 
            this.lbCost.AutoSize = true;
            this.lbCost.Location = new System.Drawing.Point(109, 160);
            this.lbCost.Name = "lbCost";
            this.lbCost.Size = new System.Drawing.Size(18, 20);
            this.lbCost.TabIndex = 5;
            this.lbCost.Text = "0";
            // 
            // lbPrice
            // 
            this.lbPrice.AutoSize = true;
            this.lbPrice.Location = new System.Drawing.Point(8, 160);
            this.lbPrice.Name = "lbPrice";
            this.lbPrice.Size = new System.Drawing.Size(97, 20);
            this.lbPrice.TabIndex = 4;
            this.lbPrice.Text = "Стоимость:";
            // 
            // gbSalt
            // 
            this.gbSalt.Controls.Add(this.cbAntiAdv);
            this.gbSalt.Controls.Add(this.cbStrike);
            this.gbSalt.Controls.Add(this.btGoC);
            this.gbSalt.Controls.Add(this.lbCostC);
            this.gbSalt.Controls.Add(this.lbPric);
            this.gbSalt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbSalt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSalt.ForeColor = System.Drawing.Color.White;
            this.gbSalt.Location = new System.Drawing.Point(0, 195);
            this.gbSalt.Name = "gbSalt";
            this.gbSalt.Size = new System.Drawing.Size(366, 187);
            this.gbSalt.TabIndex = 1;
            this.gbSalt.TabStop = false;
            this.gbSalt.Text = "Подсолить конкуренту";
            // 
            // cbAntiAdv
            // 
            this.cbAntiAdv.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbAntiAdv.AutoSize = true;
            this.cbAntiAdv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAntiAdv.Image = global::BusinessAirport.Properties.Resources.antiAd;
            this.cbAntiAdv.Location = new System.Drawing.Point(186, 25);
            this.cbAntiAdv.Name = "cbAntiAdv";
            this.cbAntiAdv.Size = new System.Drawing.Size(122, 105);
            this.cbAntiAdv.TabIndex = 14;
            this.cbAntiAdv.Text = "Антиреклама";
            this.cbAntiAdv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbAntiAdv.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAntiAdv.UseVisualStyleBackColor = true;
            this.cbAntiAdv.CheckedChanged += new System.EventHandler(this.cbAntiAdv_CheckedChanged);
            // 
            // cbStrike
            // 
            this.cbStrike.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbStrike.AutoSize = true;
            this.cbStrike.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbStrike.Image = global::BusinessAirport.Properties.Resources.strike;
            this.cbStrike.Location = new System.Drawing.Point(71, 25);
            this.cbStrike.Name = "cbStrike";
            this.cbStrike.Size = new System.Drawing.Size(109, 105);
            this.cbStrike.TabIndex = 13;
            this.cbStrike.Text = "Забастовка";
            this.cbStrike.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbStrike.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbStrike.UseVisualStyleBackColor = true;
            this.cbStrike.CheckedChanged += new System.EventHandler(this.cbStrike_CheckedChanged);
            // 
            // btGoC
            // 
            this.btGoC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btGoC.Location = new System.Drawing.Point(230, 148);
            this.btGoC.Name = "btGoC";
            this.btGoC.Size = new System.Drawing.Size(124, 32);
            this.btGoC.TabIndex = 12;
            this.btGoC.Text = "Заказать";
            this.btGoC.UseVisualStyleBackColor = false;
            this.btGoC.Click += new System.EventHandler(this.btGoC_Click);
            // 
            // lbCostC
            // 
            this.lbCostC.AutoSize = true;
            this.lbCostC.Location = new System.Drawing.Point(105, 154);
            this.lbCostC.Name = "lbCostC";
            this.lbCostC.Size = new System.Drawing.Size(18, 20);
            this.lbCostC.TabIndex = 11;
            this.lbCostC.Text = "0";
            // 
            // lbPric
            // 
            this.lbPric.AutoSize = true;
            this.lbPric.Location = new System.Drawing.Point(8, 154);
            this.lbPric.Name = "lbPric";
            this.lbPric.Size = new System.Drawing.Size(97, 20);
            this.lbPric.TabIndex = 10;
            this.lbPric.Text = "Стоимость:";
            // 
            // FormAdvert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.ClientSize = new System.Drawing.Size(366, 382);
            this.Controls.Add(this.gbSalt);
            this.Controls.Add(this.gbAdv);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FormAdvert";
            this.Text = "Имидж";
            this.gbAdv.ResumeLayout(false);
            this.gbAdv.PerformLayout();
            this.gbSalt.ResumeLayout(false);
            this.gbSalt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbAdv;
        private System.Windows.Forms.GroupBox gbSalt;
        private System.Windows.Forms.Button btGo;
        private System.Windows.Forms.Label lbCost;
        private System.Windows.Forms.Label lbPrice;
        private System.Windows.Forms.Button btGoC;
        private System.Windows.Forms.Label lbCostC;
        private System.Windows.Forms.Label lbPric;
        private System.Windows.Forms.CheckBox cbBanner;
        private System.Windows.Forms.CheckBox cbTV;
        private System.Windows.Forms.CheckBox cbPaper;
        private System.Windows.Forms.CheckBox cbRadio;
        private System.Windows.Forms.CheckBox cbStrike;
        private System.Windows.Forms.CheckBox cbAntiAdv;
    }
}