﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    // TODO Заполнить перечиcлимый городов
    public enum OwnType { Buy, Rent, Leasing };
    public enum Cities
    {
        Амстердам, Анталья, Берлин, Вена, Владивосток, Каир, Киев, Лондон,
        ЛосАнджелес, Мадрид, Мехико, Москва, НьюЙорк, Одесса, Оттава, Париж,
        Пермь, Прага, Рим, СанктПетербург, Сочи, Токио, Торонто, Тунис, Чикаго
    };
    public enum PlaneType { Пассажирский, Грузовой };
    public enum HowOften { Нет, РазВМесяц, РазВНеделю, РазВДень };
    public enum Status { НеВыполнен, Выполняется, Выполнен };

    [Serializable]
    public class Game : IDisposable
    {
        bool disposed = false;
        [NonSerialized] DataGridView dgvFlight;
        [NonSerialized] DataGridView dgvSchedule;
        [NonSerialized] MainForm mainForm;
        [NonSerialized] public TimeMachine tMachine;
        [NonSerialized] Hangar hangar;
        [NonSerialized] BinaryFormatter formatter;
        [NonSerialized] public static Random Random;
        //топливо
        public static double FuelCost = 0.039;
        //рейтинг
        public static double playerRating;
        public static double enemyRating;
        private long balance;
        private int scheduleNumber;
        private int flightNumber;
        BindingList<Flight> listOfFlights;                         // список рейсов с доски объявлений
        BindingList<FlightScheduleElem> listOfFlightSheduleElem;   // список рейсов из расписания
        BindingList<Flight> listOfPlayerFlights;
        List<Person> listOfFreePersons = new List<Person>(); //резервные люди 
        int enemyAverSalary = Properties.Settings.Default.StartEnemyAveSalary;
        long enemyBalance = Properties.Settings.Default.StartEnemyBalance;


        public long Balance
        {
            get { return balance; }
            set { balance = value; }
        }

        public string SBalance
        {
            get { return balance.ToString("N", MainForm.nfi); }
        }
        public int ScheduleNumber
        {
            get { return scheduleNumber; }
            set { scheduleNumber = value; }
        }

        public int FlightNumber
        {
            get { return flightNumber; }
            set { flightNumber = value; }
        }
        public BindingList<Flight> ListOfFlights
        {
            set { listOfFlights = value; }
            get { return listOfFlights; }
        }
        public BindingList<FlightScheduleElem> ListOfFlightSheduleElem
        {
            set { listOfFlightSheduleElem = value; }
            get { return listOfFlightSheduleElem; }
        }
        public BindingList<Flight> ListOfPlayerFlights
        {
            set { listOfPlayerFlights = value; }
            get { return listOfPlayerFlights; }
        }
        public Hangar Hangar
        {
            get { return hangar; }
        }
        public double PlayerRaiting
        {
            get { return playerRating; }
            set { playerRating = value; }
        }
        public double EnemyRaiting
        {
            get { return enemyRating; }
            set { enemyRating = value; }
        }

        public long EnemyBalance
        {
            get { return enemyBalance; }
            set { enemyBalance = value; mainForm.RefreshEnemyBalance(); }
        }

        public int FilghtNumber { set; get; }
        public List<Person> ListOfFreePersons
        {
            set { listOfFreePersons = value; }
            get { return listOfFreePersons; }
        }

        public int MyAverageSalary
        {
            get { return CalcMyAverageSalary(); }
        }

        public int EnemyAverSalary
        {
            set { enemyAverSalary = value; }
            get { return enemyAverSalary; }
        }

        public Game(DataGridView dgvFlight, DataGridView dgvSchedule, MainForm mainForm)
        {
            formatter = new BinaryFormatter();
            Random = new Random();
            this.mainForm = mainForm;
            this.dgvFlight = dgvFlight;
            this.dgvSchedule = dgvSchedule;
            ListOfFlights = new BindingList<Flight>();
            ListOfFlightSheduleElem = new BindingList<FlightScheduleElem>();
            listOfPlayerFlights = new BindingList<Flight>();
            tMachine = new TimeMachine();
            hangar = new Hangar(mainForm, this);
            balance = Properties.Settings.Default.StartMoney;
            playerRating = Properties.Settings.Default.StartPlayerRating;
            enemyRating = Properties.Settings.Default.StartEnemyRaiting;
            scheduleNumber = 1;
            flightNumber = 1;
            DateTime t = new DateTime(tMachine.MainTime.Year, tMachine.MainTime.Month, tMachine.MainTime.Day, Random.Next(1, 23), Random.Next(1, 59), 0);
            tMachine.AddToList(tMachine.MainTime.AddMonths(1), PaySalary, null);
            tMachine.AddToList(tMachine.MainTime.AddDays(5), CalcEnemyAverSalary, null); 
            tMachine.AddToList(t.AddDays(6), OrderStrikes, null);
        }

        /// <summary>
        /// Сохранение игры в файл
        /// </summary>
        public void Save()
        {
            List<object> values = new List<object>();
            List<EventElemSer> SerList = Serialize();
            values.Add(tMachine.MainTime);
            values.Add(SerList);
            values.Add(listOfFlights);
            values.Add(listOfFlightSheduleElem);
            values.Add(hangar.PlaneList);
            values.Add(balance);
            values.Add(listOfPlayerFlights);
            values.Add(listOfFreePersons);
            values.Add(hangar.ListOfOldPlane);
            values.Add(hangar.ListOfPlaneEnemy);
            values.Add(mainForm.formAdvert.GetButtonsState());
            values.Add(enemyBalance);
            values.Add(enemyRating);
            using (Stream stream = File.Open("save.dat", FileMode.Create))
            {
                formatter.Serialize(stream, values); // сериализуем
            }
        }

        /// <summary>
        /// Загрузка игры из файла
        /// </summary>
        public void Load()
        {
            List<object> values = null;
            List<EventElemSer> SerList = new List<EventElemSer>();
            if (File.Exists("save.dat"))
            {
                using (Stream stream = File.Open("save.dat", FileMode.Open))
                {
                    values = (List<object>)formatter.Deserialize(stream); // десериализуем
                }
                tMachine.MainTime = (DateTime)values[0];
                SerList = (List<EventElemSer>)values[1];
                Deserialize(SerList);
                ListOfFlights = (BindingList<Flight>)values[2];
                ListOfFlightSheduleElem = (BindingList<FlightScheduleElem>)values[3];
                hangar.PlaneList = (List<Plane>)values[4];
                Balance = (long)values[5];
                listOfPlayerFlights = (BindingList<Flight>)values[6];
                listOfFreePersons = (List<Person>)values[7];
                hangar.ListOfOldPlane = (List<Plane>)values[8];
                hangar.ListOfPlaneEnemy = (List<Plane>)values[9];
                mainForm.formAdvert.SetButtonsState((List<bool>)values[10]);
                enemyBalance = (long)values[11];
                enemyRating = (double)values[12];
                mainForm.RefreshRaiting();
            }
        }

        /// <summary>
        /// Удаление рейса с доски объявлений
        /// </summary>
        /// <param name="f">Рейс</param>
        public void DelFromBoard(object f)
        {
            dgvFlight.Invoke((MethodInvoker)delegate
            {
                ListOfFlights.Remove((Flight)f);
            });
        }

        /// <summary>
        /// Генерация рейсов
        /// </summary>
        public void GenerateFlights()
        {
            DateTime time;
            int n = Random.Next(5, 15);
            for (int i = 0; i < n; i++)
            {
                time = tMachine.MainTime.AddDays(Random.Next(4, 30));
                time = time.AddHours(Random.Next(24));
                Flight f = new Flight(flightNumber++, time, tMachine.MainTime);
                ListOfFlights.Add(f);
                //4 часа         44 часа     
                DateTime TimeDisp = tMachine.MainTime.AddMinutes(240 + Random.Next(2640));
                TimeDisp = TimeDisp > f.CompleteBefore ? f.CompleteBefore : TimeDisp;
                tMachine.AddToList(TimeDisp, DelFromBoard, f);
            }
        }

        /// <summary>
        /// Удаление рейса из расписания
        /// </summary>
        /// <param name="index">Строка</param>
        public void DelFlight(int index)
        {
            if (ListOfFlightSheduleElem[index].Brother != null)
                DelRegularFlight(index);
            else
                DelIrregularFlight(index);
        }

        /// <summary>
        /// Удаление регулярного рейса
        /// </summary>
        /// <param name="index">удаляемы индекс</param>
        private void DelRegularFlight(int index)
        {
            FlightScheduleElem fse = ListOfFlightSheduleElem[index];
            if (fse.Brother.Flight.BackFlight && fse.Brother.Status != Status.Выполняется)
            {
                ReleaseEventForPlane(fse.Brother);
                listOfFlightSheduleElem.Remove(fse.Brother);
                fse.Brother.Flight.OriginalFlight.FseList.Remove(fse.Brother);
            }
            ReleaseEventForPlane(fse);
            listOfFlightSheduleElem.Remove(fse);

            if (fse.Flight.BackFlight)
            {
                fse.Brother.Brother = null;
                fse.Flight.OriginalFlight.FseList.Remove(fse);
                RefreshRegularFlights(fse.Flight.OriginalFlight);
            }
            else
            {
                fse.Flight.FseList.Remove(fse);
                RefreshRegularFlights(fse.Flight);
            }
        }

        /// <summary>
        /// Удаление нерегулярного рейса
        /// </summary>
        /// <param name="index">удаляемый индекс</param>
        private void DelIrregularFlight(int index)
        {
            listOfFlightSheduleElem[index].Flight.BtPlane = "Назначить";
            // если рейс не выполнен, а самолет назначен - удаляем события отправления и прибытия 
            ReleaseEventForPlane(listOfFlightSheduleElem[index]);
            //удаляем из расписания
            ListOfFlightSheduleElem.RemoveAt(index);
        }

        private void DelFlightNewDay(int index)
        {
            if (listOfFlightSheduleElem[index].Flight.HowOften != HowOften.Нет)
                if (listOfFlightSheduleElem[index].Flight.BackFlight)
                    listOfFlightSheduleElem[index].Flight.OriginalFlight.FseList.Remove(listOfFlightSheduleElem[index]);
                else
                    listOfFlightSheduleElem[index].Flight.FseList.Remove(listOfFlightSheduleElem[index]);
            mainForm.pFlightsForm.EnableRow(listOfFlightSheduleElem[index].Flight);
            //удаляем из расписания
            ListOfFlightSheduleElem.RemoveAt(index);
        }


        /// <summary>
        /// Блокировка рейса при выполнении
        /// </summary>
        /// <param name="e">Строка рейса</param>
        public void DisableFlight(object e)
        {
            tMachine.Stop();
            dgvSchedule.ClearSelection();
            FlightScheduleElem fse = (FlightScheduleElem)e;
            if (!isFullCrew(fse.Plane) || !IsNormalMood(fse.Plane) || fse.Plane.IsFlight || fse.Plane.curCity != fse.Flight.From)
                CantStart(fse);
            else
            {
                fse.StartFlight();
                mainForm.pFlightsForm.DisableRow(fse.Flight);
            }

            dgvSchedule.Invoke((MethodInvoker)delegate
            {
                dgvSchedule.Refresh();
            });
            tMachine.Start();
        }

        private void CantStart(FlightScheduleElem fse)
        {
            if (fse.Brother != null && fse.Brother.Flight.BackFlight)
            {
                fse.Brother.Plane = null;
                fse.Brother.CantStart(true);
                dgvSchedule.Rows[ListOfFlightSheduleElem.IndexOf(fse.Brother)].DefaultCellStyle.BackColor = Color.Yellow;
                tMachine.RemoveFromList(DisableFlight, fse.Brother);
                tMachine.RemoveFromList(EnableFlight, fse.Brother);
            }
            if (FindAllPlanes(fse.Plane) <= 1)
            {
                fse.Flight.AppointCount -= 1;
            }
            fse.Plane = null;
            fse.CantStart();
            dgvSchedule.Rows[ListOfFlightSheduleElem.IndexOf(fse)].DefaultCellStyle.BackColor = Color.Yellow;
            tMachine.RemoveFromList(EnableFlight, fse);
        }

        private int FindAllPlanes(Plane p)
        {
            int count = 0;
            int index = 0;
            while (index < listOfFlightSheduleElem.Count)
                if (listOfFlightSheduleElem[index++].Plane == p)
                    count++;
            return count;
        }

        /// <summary>
        /// Изменения при прибытии
        /// </summary>
        /// <param name="e">Строка рейса</param>
        public void EnableFlight(object e)
        {
            tMachine.Stop();
            FlightScheduleElem fse = (FlightScheduleElem)e;
            fse.EndFlight();
            if (!fse.Flight.IsEmptyFlight)
                if (fse.Flight.HowOften == HowOften.Нет)
                    dgvSchedule.Invoke((MethodInvoker)delegate
                    {
                        ListOfPlayerFlights.Remove(fse.Flight);
                    });        
                else
                    mainForm.pFlightsForm.RefreshCount(fse.Flight);
            dgvSchedule.Rows[ListOfFlightSheduleElem.IndexOf(fse)].DefaultCellStyle.BackColor = Color.Gray;   // полоска рейса серая
            if (fse.Flight.IsEmptyFlight)
                mainForm.RefreshBalance("Оплата расходов пустого рейса " + fse.From + "-" +fse.To, -fse.Expense);
            else
                mainForm.RefreshBalance("Прибыль рейса " + fse.From + "-" + fse.To, fse.Profit - fse.Expense);
            if (fse.Plane.Dolzhoook && fse.Plane.typeOfOwnership == OwnType.Rent)
                hangar.AddDays(fse.Plane);
            else
                if (fse.Plane.Dolzhoook && fse.Plane.typeOfOwnership == OwnType.Leasing)
                hangar.MonthPay(fse.Plane);
            tMachine.Start();
        }

        /// <summary>
        /// Назначение рейса
        /// </summary>
        /// <param name="RowIndex">Индекс строки</param>
        /// <param name="ColumnIndex">Индекс столбца</param>
        /// <param name="dTime">Время прибытия</param>
        /// <param name="aTime">Время отправления</param>
        /// <param name="p">Самолет</param>
        public void AppointFlight(FlightScheduleElem fse)
        {
            fse.Number = ScheduleNumber++;
            listOfFlightSheduleElem.Add(fse);
            AcceptChanges(fse);
        }

        /// <summary>
        /// Изменение рейса
        /// </summary>
        /// <param name="index">Номер изменяемого рейса</param>
        public void ChangeFlight(int index, bool isAlreadyInSchedule)
        {
            if (isAlreadyInSchedule)
                ReleaseEventForPlane(listOfFlightSheduleElem[index]);
            if (ListOfFlightSheduleElem[index].Brother != null && ListOfFlightSheduleElem[index].Brother.Flight.BackFlight)
                ListOfFlightSheduleElem[index].Brother.ChangeAccept();
            AcceptChanges(ListOfFlightSheduleElem[index]);
            dgvSchedule.Refresh();
        }

        /// <summary>
        /// Применение изменений
        /// </summary>
        /// <param name="index">Номер элемента в расписании</param>
        private void AcceptChanges(FlightScheduleElem fse)
        {
            fse.ChangeAccept();
            if (fse.Flight.HowOften != HowOften.Нет)
                if (fse.Flight.BackFlight)
                    RefreshRegularFlights(fse.Flight.OriginalFlight);
                else
                    RefreshRegularFlights(fse.Flight);
            dgvSchedule.Rows[ListOfFlightSheduleElem.IndexOf(fse)].DefaultCellStyle.BackColor = Color.White;
            //Добавление события начала полёта
            tMachine.AddToList(
                fse.DepartureTime,
                DisableFlight,
                fse);
            //Добавление события окончания полёта
            tMachine.AddToList(
                fse.ArrivalTime,
                EnableFlight,
                fse);
        }

        /// <summary>
        /// Удаление всех рейсов запланированных на этот самолет
        /// </summary>
        /// <param name="p">Самолет</param>
        public void DeleteFlightsForPlane(Plane p)
        {
            bool isTicking = tMachine.Status;
            if (isTicking)
                tMachine.Stop();
            HashSet<Flight> RefreshList = new HashSet<Flight>();
            Flight f;
            int index = 0;
            while (index < listOfFlightSheduleElem.Count)
            {
                if (listOfFlightSheduleElem[index].Plane == p && listOfFlightSheduleElem[index].Status == Status.НеВыполнен)
                {
                    if (!ListOfFlightSheduleElem[index].Flight.BackFlight && !ListOfFlightSheduleElem[index].Flight.IsEmptyFlight && ListOfFlightSheduleElem[index].Flight.HowOften != HowOften.Нет)
                        RefreshList.Add(listOfFlightSheduleElem[index].Flight);
                    DeleteFSE(ListOfFlightSheduleElem[index]);
                }
                else
                    index++;
            }
            while (RefreshList.Count > 0)
            {
                f = RefreshList.First();
                if (f.BackFlight)
                    RefreshRegularFlights(f.OriginalFlight);
                else
                    RefreshRegularFlights(f);
                RefreshList.Remove(f);
            }
            if (isTicking)
                tMachine.Start();
        }
        /// <summary>
        /// Удаление рейса из расписания по Flight
        /// </summary>
        /// <param name="flight">удаляемый рейс</param>
        public void DeleteFromSchedule(Flight flight)
        {
            int index = 0;
            FlightScheduleElem fse;
            while (index < flight.FseList.Count)
            {
                fse = flight.FseList[index];
                if (fse.Status != Status.Выполняется)
                {
                    DeleteFSE(fse);
                    flight.FseList.Remove(fse);
                }
                else
                    index++;
            }
            if (flight.HowOften != HowOften.Нет)
                if (flight.BackFlight)
                    RefreshRegularFlights(flight.OriginalFlight);
                else
                    RefreshRegularFlights(flight);
        }

        /// <summary>
        /// Удаление всех упоминаний о рейсе
        /// </summary>
        /// <param name="fse">Удаляемый рейс</param>
        /// <param name="index">Индекс удаляемого рейса</param>
        private void DeleteFSE(FlightScheduleElem fse)
        {
            ReleaseEventForPlane(fse);
            ListOfFlightSheduleElem.Remove(fse);
        }

        /// <summary>
        /// Выбор рейса и занесение его в расписание
        /// </summary>
        public void ChooseClick()
        {
            if (dgvFlight.SelectedRows.Count != 0)
            {
                int index = dgvFlight.CurrentRow.Index;
                Flight flight = listOfFlights[index];
                listOfPlayerFlights.Add(flight);

                tMachine.AddToList(flight.CompleteBefore, Deadline, flight);
                tMachine.RemoveFromList(DelFromBoard, ListOfFlights[index]);

                ListOfFlights.RemoveAt(index);
            }
        }

        /// <summary>
        /// Добавление пустого рейса
        /// </summary>
        /// <param name="flight">Рейс</param>
        /// <param name="plane">Самолет</param>
        /// <param name="finishTime">Время прибытия</param>
        public void AddEmptyFlight(FlightScheduleElem fse)
        {
            ScheduleNumber++;
            listOfFlightSheduleElem.Add(fse);
            tMachine.AddToList(fse.DepartureTime, DisableFlight, fse);
            tMachine.AddToList(fse.ArrivalTime, EnableFlight, fse);
        }

        /// <summary>
        /// Новый день - новые рейсы на доске объявлений и нерегулярные рейсы удаляются из расписания
        /// </summary>
        public void NewDay()
        {
            bool isTicking = tMachine.Status;
            if (isTicking)
                tMachine.Stop();
            
            if (EnemyBalance < 0)
            {
                mainForm.Invoke((MethodInvoker)delegate
                {
                    if (MessageBox.Show("Ваш конкурент потерпел поражение \nХотите продолжить игру с новым конекурентом? \nВ противном случае игра окончена.", "Поздравляем", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        EnemyBalance = Properties.Settings.Default.StartEnemyBalance;
                        EnemyRaiting = Properties.Settings.Default.StartEnemyRaiting;
                        Hangar.ListOfPlaneEnemy = new List<Plane>()
                    {
                        new Plane("Airbus А380", OwnType.Buy,    PlaneType.Пассажирский, 853,    1020,   12000, 15400, Cities.Москва, 16500000, 0,   55000, 0, 20, 0),
                        new Plane("Ан-225 «Мрия»",  OwnType.Buy,   PlaneType.Грузовой, 150,    850,    15900, 10000, Cities.Москва, 17490000, 0,   58300, 0, 6, 0)
                    };
                    }
                    else
                    {
                        mainForm.isGameOver = true;
                        mainForm.Close();
                    }
                        
                });
            }
            else
                EnemyBalance += 10000 * Random.Next(5, 41) + Convert.ToInt32(enemyRating - playerRating) * 100;
            int i = 0;
            scheduleNumber = 1;
            Fuel();
            while (i < listOfFlightSheduleElem.Count)
            {
                if (listOfFlightSheduleElem[i].Flight.IsEmptyFlight && listOfFlightSheduleElem[i].Status != Status.Выполняется)
                    DelFlightNewDay(i);
                else
                {
                    listOfFlightSheduleElem[i].Number = scheduleNumber++;
                    if (listOfFlightSheduleElem[i].Status != Status.Выполнен)
                        listOfFlightSheduleElem[i].CalcExpenses();
                    i++;
                }
            }
            GenerateFlights();
            dgvSchedule.Refresh();
            if (MyAverageSalary > enemyAverSalary)
                mainForm.RefreshRaiting(1);
            else
                mainForm.RefreshRaiting(-1);
            if (isTicking)
                tMachine.Start();
        }


        /// <summary>
        /// Ежедневное изменение цены на топливо
        /// </summary>
        public void Fuel()
        {
            FuelCost = Random.NextDouble() * (0.042 - 0.035) + 0.035;
            FuelCost = Math.Round(FuelCost, 6);
        }

        /// <summary>
        /// Время обновления регулярных
        /// </summary>
        /// <param name="fse">Строка рейса</param>
        /// <returns></returns>
        private DateTime TimeForRegular(FlightScheduleElem fse)
        {
            DateTime timeUpdate = new DateTime(fse.Flight.CompleteBefore.Year, fse.Flight.CompleteBefore.Month, fse.Flight.CompleteBefore.Day);
            if (fse.Flight.HowOften == HowOften.РазВДень)
                timeUpdate = timeUpdate.AddDays(1);
            if (fse.Flight.HowOften == HowOften.РазВНеделю)
                timeUpdate = timeUpdate.AddDays(7);
            if (fse.Flight.HowOften == HowOften.РазВМесяц)
                timeUpdate = timeUpdate.AddMonths(1);
            fse.Flight.RefreshTime(timeUpdate);
            return timeUpdate;
        }

        /// <summary>
        /// Подготовка списка к сериализации в файл
        /// </summary>
        /// <returns>Список, готовый для сериализации</returns>
        private List<EventElemSer> Serialize()
        {
            List<EventElemSer> SerList = new List<EventElemSer>();
            foreach (KeyValuePair<DateTime, List<EventElem>> eventList in tMachine.EventsList)
                foreach (EventElem ee in eventList.Value)
                    SerList.Add(new EventElemSer(eventList.Key, ee));
            return SerList;
        }

        /// <summary>
        /// Десериализация списка, полученного из файла
        /// </summary>
        /// <param name="SerList"> список событий в файле</param>
        private void Deserialize(List<EventElemSer> SerList)
        {
            tMachine.EventsList.Clear();
            foreach (EventElemSer ees in SerList)
            {
                switch (ees.Name)
                {
                    case "DelFromBoard": tMachine.AddToList(ees.Time, DelFromBoard, ees.Param); break;
                    case "DisableFlight": tMachine.AddToList(ees.Time, DisableFlight, ees.Param); break;
                    case "EnableFlight": tMachine.AddToList(ees.Time, EnableFlight, ees.Param); break;
                    case "AddDays": tMachine.AddToList(ees.Time, hangar.AddDays, ees.Param); break;
                    case "MonthPay": tMachine.AddToList(ees.Time, hangar.MonthPay, ees.Param); break;
                    case "DelBonus": tMachine.AddToList(ees.Time, mainForm.formAdvert.DelBonus, ees.Param); break;
                    case "CheckOld": tMachine.AddToList(ees.Time, mainForm.formHangar.CheckOld, ees.Param); break;
                    case "CheckOldEnemy": tMachine.AddToList(ees.Time, mainForm.formHangar.CheckOldEnemy, ees.Param); break;
                    case "EnemyBuyPlane": tMachine.AddToList(ees.Time, mainForm.formHangar.EnemyBuyPlane, ees.Param); break;
                    case "OrderStrikes": tMachine.AddToList(ees.Time, OrderStrikes, ees.Param); break;
                    case "Deadline": tMachine.AddToList(ees.Time, Deadline, ees.Param); break;
                    case "PaySalary": tMachine.AddToList(ees.Time, PaySalary, ees.Param); break;
                    case "AddOldPlane": tMachine.AddToList(ees.Time, mainForm.formHangar.AddOldPlane, ees.Param); break;
                    case "CalcEnemyAverSalary": tMachine.AddToList(ees.Time, CalcEnemyAverSalary, ees.Param);break;
                    case "AntiAdv": tMachine.AddToList(ees.Time, AntiAdv, ees.Param); break;
                    case "EnemyAdv": tMachine.AddToList(ees.Time, EnemyAdv, ees.Param); break;
                    case "EnemySalePlane": tMachine.AddToList(ees.Time, mainForm.formHangar.EnemySalePlane, ees.Param); break;

                    default: break;
                }
            }
        }

        /// <summary>
        /// Выплата неустойки, если прошло время, до которого надо было выполнить рейс
        /// </summary>
        /// <param name="f">Строка с рейсом</param>
        private void Deadline(object f)
        {
            Flight flight = (Flight)f;
            if (flight.Status == Status.НеВыполнен)
            {
                mainForm.RefreshBalance("Выплата неустойки за "+flight.From+ "-"+ flight.To, -flight.Cost);
                mainForm.RefreshRaiting(-1);
            }
                

            switch (flight.HowOften)
            {
                case HowOften.Нет:
                    dgvSchedule.Invoke((MethodInvoker)delegate
                    {                        
                        if (flight.FseList.Count > 0)
                        {
                            ListOfFlightSheduleElem.Remove(flight.FseList[0]);
                            flight.FseList.RemoveAt(0);
                        }
                        ListOfPlayerFlights.Remove(flight);
                    });                    
                    break;
                case HowOften.РазВДень:
                    flight.CompleteBefore = flight.CompleteBefore.AddDays(1);
                    tMachine.AddToList(flight.CompleteBefore, Deadline, flight);
                    break;
                case HowOften.РазВНеделю:
                    flight.CompleteBefore = flight.CompleteBefore.AddDays(7);
                    tMachine.AddToList(flight.CompleteBefore, Deadline, flight);
                    break;
                case HowOften.РазВМесяц:
                    flight.CompleteBefore = flight.CompleteBefore.AddMonths(1);
                    tMachine.AddToList(flight.CompleteBefore, Deadline, flight);
                    break;
            }
            if (flight.HowOften != HowOften.Нет)
            {
                mainForm.pFlightsForm.RefreshCount(flight, false);
                int i = 0;
                while (i < flight.FseList.Count)
                    if (flight.FseList[i].Status == Status.Выполнен)
                    {
                        dgvSchedule.Invoke((MethodInvoker)delegate
                        {
                            ListOfFlightSheduleElem.Remove(flight.FseList[i]);
                            flight.FseList.RemoveAt(i);
                        });
                    }
                    else
                        i++;
            }
        }

        /// <summary>
        ///  Освобождение самолета, назначенного на рейс, который отменили
        /// </summary>
        /// <param name="index">Индекс строки рейса из расписания</param>
        private void ReleaseEventForPlane(FlightScheduleElem fse)
        {
            tMachine.RemoveFromList(DisableFlight, fse);
            tMachine.RemoveFromList(EnableFlight, fse);
        }

        /// <summary>
        /// Возврат рейса на доску рейсов
        /// </summary>
        /// <param name="index">Индекс строки рейса из расписания</param>
        public void ReturnToBoard(Flight flight)
        {
            tMachine.RemoveFromList(Deadline, flight);
            ListOfFlights.Add(flight);
            dgvFlight.Refresh();
            DateTime dtRemove = tMachine.MainTime.AddMinutes(240 + Random.Next(200));
            dtRemove = dtRemove < flight.CompleteBefore ? dtRemove : flight.CompleteBefore;
            tMachine.AddToList(dtRemove, DelFromBoard, flight);
        }

        /// <summary>
        /// вычисление средней зарплаты игрока
        /// </summary>
        /// <returns></returns>
        private int CalcMyAverageSalary()
        {
            int s = 0, c = 0;
            if (hangar.PlaneList != null)
            {
                foreach (Plane plane in hangar.PlaneList)
                {
                    if (plane.persons != null)
                    {
                        foreach (Person person in plane.persons)
                        {
                            s += person.TempSalary;
                            ++c;
                        }
                    }
                }
            }
            if (ListOfFreePersons != null)
            {
                foreach (Person person in ListOfFreePersons)
                {
                    s += person.TempSalary;
                    ++c;
                }
            }
            return c == 0 ? c : s / c;
        }
        /// <summary>
        /// Выплата зарплаты
        /// </summary>
        /// <param name="obj"></param>
        private void PaySalary(object obj)
        {
            int s = 0;
            if (hangar.PlaneList != null)
            {
                foreach (Plane plane in hangar.PlaneList)
                {
                    if (plane.persons != null)
                    {
                        foreach (Person person in plane.persons)
                        {
                            s += person.TempSalary *
                        (tMachine.MainTime - person.DateChangeSalary).Days /
                        DateTime.DaysInMonth(tMachine.MainTime.Year, tMachine.MainTime.Month - 1);
                            person.SumSalary = 0;
                            person.DateChangeSalary = tMachine.MainTime;
                        }
                    }
                }
            }
            if (ListOfFreePersons != null)
            {
                foreach (Person person in ListOfFreePersons)
                {
                    s += person.TempSalary *
                        (tMachine.MainTime - person.DateChangeSalary).Days /
                        DateTime.DaysInMonth(tMachine.MainTime.Year, tMachine.MainTime.Month - 1);
                    person.SumSalary = 0;
                    person.DateChangeSalary = tMachine.MainTime;
                }
            }
            mainForm.RefreshBalance("Выплата зарплаты экипажу", -s);
            tMachine.AddToList(tMachine.MainTime.AddMonths(1), PaySalary, null);
        }

        /// <summary>
        ///  нам заказывают антирекламу
        /// </summary>
        public void AntiAdv(object e)
        {
            if (EnemyBalance - 1000000 > 0)
            {
                EnemyBalance -= 1000000;
                mainForm.RefreshRaiting(-3);
            }
            tMachine.AddToList(tMachine.MainTime.AddDays(Random.Next(10,20)), AntiAdv, null);
        }

        /// <summary>
        /// Удаление бонусов
        /// </summary>
        /// <param name="e"></param>
        public void DelBonusEnemy(object e)
        {
            int number = (int)e;
            switch (number)
            {
                case 0:
                    EnemyRaiting -= 2;
                    break;
                case 1:
                    EnemyRaiting -= 1;
                    break;
                case 2:
                    EnemyRaiting -= 4;
                    break;
                case 3:
                    EnemyRaiting -= 3;
                    break;
                case 4:
                    EnemyRaiting += 3;
                    break;
            }
            mainForm.RefreshRaiting();
        }
        /// <summary>
        ///  Конкурент заказывает себе рекламу
        /// </summary>
        public void EnemyAdv(object e)
        {
            int adv = Random.Next(1, 5);
            switch (adv)
            {
                case 1:
                    if (EnemyBalance - 750000 > 0)
                    {
                        EnemyRaiting += 2;
                        tMachine.AddToList(tMachine.MainTime.AddDays(10), DelBonusEnemy, 1);
                    }
                    break;
                case 2:
                    if (EnemyBalance - 100000 > 0)
                    {
                        EnemyRaiting += 1;
                        tMachine.AddToList(tMachine.MainTime.AddDays(7), DelBonusEnemy, 2);
                    }
                    break;
                case 3:
                    if (EnemyBalance - 1200000 > 0)
                    {
                        EnemyRaiting += 4;
                        tMachine.AddToList(tMachine.MainTime.AddMonths(1), DelBonusEnemy, 3);
                    }
                    break;
                case 4:
                    if (EnemyBalance - 60000 > 0)
                    {
                        EnemyRaiting += 3;
                        tMachine.AddToList(tMachine.MainTime.AddMonths(1), DelBonusEnemy, 4);
                    }
                    break;
                default: break;
            }
            mainForm.RefreshRaiting();
            tMachine.AddToList(tMachine.MainTime.AddDays(Random.Next(1, 2)), EnemyAdv, null);
        }

        /// <summary>
        /// Вычисление средней зп конкурента ♥
        /// </summary>
        /// <returns></returns>
        private void CalcEnemyAverSalary(object obj)
        {

            int s = CalcMyAverageSalary();
            enemyAverSalary = Random.Next(s - 2000, s + 10000);
            //пока средняя зп меняется каждый день. В итоговой версии можно поменять.
            tMachine.AddToList(tMachine.MainTime.AddDays(1), CalcEnemyAverSalary, null);
        }

        /// <summary>
        /// Проверка настроения экипажа
        /// </summary>
        /// <param name="plane">Самолет, экипаж которого надо проверить</param>
        /// <returns>true - никто не бунтует</returns>
        private bool IsNormalMood(Plane plane)
        {
            bool mood = true;
            foreach (Person person in plane.persons)
            {
                if (person.TempSalary < ((MyAverageSalary + enemyAverSalary) / 2)) person.Mood = false;
                mood = mood && person.Mood;
            }

            if (!mood)
            {
                bool isTicking = tMachine.Status;
                if (isTicking)
                    tMachine.Stop();
                mainForm.RefreshRaiting(-1);
                mainForm.Invoke((MethodInvoker)delegate
                {
                    if (MessageBox.Show("На самолете " + plane.name + " ЛЮДИ БАСТУЮТ!!!!", "", MessageBoxButtons.OK) == DialogResult.OK) { };
                });
                if (isTicking)
                    tMachine.Start();
            }
            return mood;
        }

        /// <summary>
        /// Проверка полноты экипажа
        /// </summary>
        /// <param name="plane">Самолет, у которого надо проверить экипаж</param>
        /// <returns></returns>
        private bool isFullCrew(Plane plane)
        {
            bool full = true;
            if (plane.maxPersons != plane.persons.Count)
            {
                bool isTicking = tMachine.Status;
                if (isTicking)
                    tMachine.Stop();
                full = false;
                mainForm.Invoke((MethodInvoker)delegate
                {
                    if (MessageBox.Show("На самолете " + plane.name + " неполный состав экипажа!", "", MessageBoxButtons.OK) == DialogResult.OK) { };
                });
                if (isTicking)
                    tMachine.Start();
            }
            return full;
        }

        /// <summary>
        /// Заказ забастовки нам
        /// </summary>
        /// <param name="obj"></param>
        private void OrderStrikes(object obj)
        {
            bool mood = true;
            if (playerRating - enemyRating >= 30)
            {
                if (hangar.PlaneList != null)
                {
                    foreach (Plane plane in hangar.PlaneList)
                    {
                        if (plane.persons != null)
                        {
                            foreach (Person person in plane.persons)
                            {
                                if (person.TempSalary < enemyAverSalary) person.Mood = false;
                                mood = mood && person.Mood;
                            }
                        }
                    }
                }
                if (ListOfFreePersons != null)
                {
                    foreach (Person person in ListOfFreePersons)
                    {
                        if (person.TempSalary < enemyAverSalary) person.Mood = false;
                        mood = mood && person.Mood;
                    }
                }
                if (!mood)
                {
                    bool isTicking = tMachine.Status;
                    if (isTicking)
                        tMachine.Stop();
                    //playerRating -= 7;
                    mainForm.RefreshRaiting(-7);
                    EnemyBalance -= 2000000; //new
                    mainForm.Invoke((MethodInvoker)delegate
                    {
                        if (MessageBox.Show("Ваши люди бастуют!", "", MessageBoxButtons.OK) == DialogResult.OK) { };
                    });
                    if (isTicking)
                        tMachine.Start();
                }
            }
            DateTime t = new DateTime(tMachine.MainTime.Year, tMachine.MainTime.Month, tMachine.MainTime.Day, Random.Next(1, 23), Random.Next(1, 59), 0);
            tMachine.AddToList(t.AddDays(Random.Next(6, 7)), OrderStrikes, null);
            
        }

        public void RefreshRegularFlights(Flight flight)
        {
            bool isTicking = tMachine.Status;
            if (isTicking)
                tMachine.Stop();
            List<FlightScheduleElem> fselist;
            SortedDictionary<DateTime, List<FlightScheduleElem>> sd = new SortedDictionary<DateTime, List<FlightScheduleElem>>();
            SortedDictionary<DateTime, List<FlightScheduleElem>> Backsd = new SortedDictionary<DateTime, List<FlightScheduleElem>>();
            int i = 0;
            while (i < flight.FseList.Count)
            {
                if (!flight.FseList[i].Flight.BackFlight)
                {
                    if (!sd.ContainsKey(flight.FseList[i].DepartureTime))
                        sd.Add(flight.FseList[i].DepartureTime, new List<FlightScheduleElem> { flight.FseList[i] });
                    else
                    {
                        sd.TryGetValue(flight.FseList[i].DepartureTime, out fselist);
                        fselist.Add(flight.FseList[i]);
                    }
                }
                else
                {
                    if (!Backsd.ContainsKey(flight.FseList[i].DepartureTime))
                        Backsd.Add(flight.FseList[i].DepartureTime, new List<FlightScheduleElem> { flight.FseList[i] });
                    else
                    {
                        Backsd.TryGetValue(flight.FseList[i].DepartureTime, out fselist);
                        fselist.Add(flight.FseList[i]);
                    }
                }
                i++;
            }

            RefFlight(flight, sd);
            RefFlight(flight, Backsd);
            if (isTicking)
                tMachine.Start();
        }

        private void RefFlight(Flight flight, SortedDictionary<DateTime, List<FlightScheduleElem>> sd)
        {
            int multiplier;
            DateTime prevDate, nextDate = flight.CompleteBefore;
            switch (flight.HowOften)
            {
                case HowOften.РазВДень:
                    prevDate = nextDate.AddDays(-1);
                    multiplier = 1;
                    foreach (KeyValuePair<DateTime, List<FlightScheduleElem>> sdElem in sd)
                    {
                        while (sdElem.Key > nextDate)
                        {
                            multiplier = 1;
                            prevDate = nextDate;
                            nextDate = nextDate.AddDays(1);
                        }
                        multiplier += sdElem.Value.Count - 1;
                        foreach (FlightScheduleElem fse in sdElem.Value)
                        {
                            if (fse.Status == Status.НеВыполнен)
                            {
                                fse.Multiplier = multiplier;
                                fse.CalcProfit();
                            }
                        }
                        multiplier++;
                    }
                    break;
                case HowOften.РазВНеделю:
                    prevDate = nextDate.AddDays(-7);
                    multiplier = 1;
                    foreach (KeyValuePair<DateTime, List<FlightScheduleElem>> sdElem in sd)
                    {
                        while (sdElem.Key > nextDate)
                        {
                            multiplier = 1;
                            prevDate = nextDate;
                            nextDate = nextDate.AddDays(7);
                        }
                        multiplier += sdElem.Value.Count - 1;
                        foreach (FlightScheduleElem fse in sdElem.Value)
                        {
                            if (fse.Status == Status.НеВыполнен)
                            {
                                fse.Multiplier = multiplier;
                                fse.CalcProfit();
                            }
                        }
                        multiplier++;
                    }
                    break;
                case HowOften.РазВМесяц:
                    prevDate = nextDate.AddMonths(-1);
                    multiplier = 1;
                    foreach (KeyValuePair<DateTime, List<FlightScheduleElem>> sdElem in sd)
                    {
                        while (sdElem.Key > nextDate)
                        {
                            multiplier = 1;
                            prevDate = nextDate;
                            nextDate = nextDate.AddMonths(1);
                        }
                        multiplier += sdElem.Value.Count - 1;
                        foreach (FlightScheduleElem fse in sdElem.Value)
                        {
                            if (fse.Status == Status.НеВыполнен)
                            {
                                fse.Multiplier = multiplier;
                                fse.CalcProfit();
                            }
                        }
                        multiplier++;
                    }

                    break;
                default:break;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                tMachine.Dispose();
            }
            disposed = true;
        }

    }
}
