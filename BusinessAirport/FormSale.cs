﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class FormSale : Form
    {
        public FormSale(Plane p)
        {
            InitializeComponent();
            labelName.Text = (p.name + " " + p.pType + " ");
            if (p.typeOfOwnership == OwnType.Buy)
            {
                lbPrice.Visible = true;
                lbPrice.Text += (Convert.ToInt32(p.shopPrice - p.shopPrice * ((double)p.allDistance / p.speed) * 0.00007) / 2);
                lbRemovePlane.Text = "продать этот самолёт?";
            }
            else
            {
                lbPrice.Visible = false;
                lbRemovePlane.Text = "отказаться от этого самолёта?";
            }
        }
    }
}
