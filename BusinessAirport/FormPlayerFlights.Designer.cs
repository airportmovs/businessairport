﻿namespace BusinessAirport
{
    partial class FormPlayerFlights
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvSchedule = new BusinessAirport.DataGridViewEx();
            this.colNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDistance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTypeOfFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCapacity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMoney = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPlane = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.regular = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deadline = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.del = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSchedule)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSchedule
            // 
            this.dgvSchedule.AllowUserToAddRows = false;
            this.dgvSchedule.AllowUserToDeleteRows = false;
            this.dgvSchedule.AllowUserToResizeRows = false;
            this.dgvSchedule.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSchedule.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.dgvSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSchedule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNumber,
            this.colFrom,
            this.colTo,
            this.colDistance,
            this.colTypeOfFlight,
            this.colCapacity,
            this.colMoney,
            this.colPlane,
            this.colName,
            this.regular,
            this.status,
            this.deadline,
            this.del});
            this.dgvSchedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSchedule.GridColor = System.Drawing.SystemColors.Window;
            this.dgvSchedule.Location = new System.Drawing.Point(0, 0);
            this.dgvSchedule.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.dgvSchedule.MultiSelect = false;
            this.dgvSchedule.Name = "dgvSchedule";
            this.dgvSchedule.ReadOnly = true;
            this.dgvSchedule.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(92)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgvSchedule.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSchedule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSchedule.Size = new System.Drawing.Size(1370, 418);
            this.dgvSchedule.TabIndex = 2;
            this.dgvSchedule.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSchedule_CellContentClick);
            // 
            // colNumber
            // 
            this.colNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.colNumber.DataPropertyName = "Number";
            this.colNumber.FillWeight = 49.36234F;
            this.colNumber.HeaderText = "№";
            this.colNumber.Name = "colNumber";
            this.colNumber.ReadOnly = true;
            this.colNumber.Width = 47;
            // 
            // colFrom
            // 
            this.colFrom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colFrom.DataPropertyName = "From";
            this.colFrom.FillWeight = 88.36713F;
            this.colFrom.HeaderText = "Откуда";
            this.colFrom.Name = "colFrom";
            this.colFrom.ReadOnly = true;
            // 
            // colTo
            // 
            this.colTo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colTo.DataPropertyName = "To";
            this.colTo.FillWeight = 71.65477F;
            this.colTo.HeaderText = "Куда";
            this.colTo.Name = "colTo";
            this.colTo.ReadOnly = true;
            // 
            // colDistance
            // 
            this.colDistance.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDistance.DataPropertyName = "Distance";
            this.colDistance.FillWeight = 125.0246F;
            this.colDistance.HeaderText = "Расстояние";
            this.colDistance.MinimumWidth = 50;
            this.colDistance.Name = "colDistance";
            this.colDistance.ReadOnly = true;
            // 
            // colTypeOfFlight
            // 
            this.colTypeOfFlight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colTypeOfFlight.DataPropertyName = "Type";
            this.colTypeOfFlight.FillWeight = 103.2557F;
            this.colTypeOfFlight.HeaderText = "Тип рейса";
            this.colTypeOfFlight.MinimumWidth = 60;
            this.colTypeOfFlight.Name = "colTypeOfFlight";
            this.colTypeOfFlight.ReadOnly = true;
            // 
            // colCapacity
            // 
            this.colCapacity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.colCapacity.DataPropertyName = "Weight";
            this.colCapacity.FillWeight = 175.4647F;
            this.colCapacity.HeaderText = "Грузпод-ть";
            this.colCapacity.Name = "colCapacity";
            this.colCapacity.ReadOnly = true;
            this.colCapacity.Width = 106;
            // 
            // colMoney
            // 
            this.colMoney.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colMoney.DataPropertyName = "Cost";
            this.colMoney.FillWeight = 168.0034F;
            this.colMoney.HeaderText = "Прибыль / Неустойка";
            this.colMoney.Name = "colMoney";
            this.colMoney.ReadOnly = true;
            // 
            // colPlane
            // 
            this.colPlane.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colPlane.DataPropertyName = "BtPlane";
            this.colPlane.FillWeight = 71.59013F;
            this.colPlane.HeaderText = "Назначить";
            this.colPlane.MinimumWidth = 90;
            this.colPlane.Name = "colPlane";
            this.colPlane.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.DataPropertyName = "CompleteCount";
            this.colName.FillWeight = 115.6206F;
            this.colName.HeaderText = "Количество выполнений";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // regular
            // 
            this.regular.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.regular.DataPropertyName = "SHowOften";
            this.regular.FillWeight = 129.8397F;
            this.regular.HeaderText = "Регулярность";
            this.regular.MinimumWidth = 50;
            this.regular.Name = "regular";
            this.regular.ReadOnly = true;
            // 
            // status
            // 
            this.status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.status.DataPropertyName = "SStatus";
            this.status.FillWeight = 80.80207F;
            this.status.HeaderText = "Статус";
            this.status.MinimumWidth = 100;
            this.status.Name = "status";
            this.status.ReadOnly = true;
            // 
            // deadline
            // 
            this.deadline.DataPropertyName = "SCompleteBefore";
            this.deadline.HeaderText = "Выполнить до";
            this.deadline.Name = "deadline";
            this.deadline.ReadOnly = true;
            // 
            // del
            // 
            this.del.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.del.DataPropertyName = "btCancel";
            this.del.FillWeight = 56.86351F;
            this.del.HeaderText = "Отказ";
            this.del.MinimumWidth = 90;
            this.del.Name = "del";
            this.del.ReadOnly = true;
            // 
            // FormPlayerFlights
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 418);
            this.Controls.Add(this.dgvSchedule);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormPlayerFlights";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Взятые рейсы";
            this.Shown += new System.EventHandler(this.FormPlayerFlights_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSchedule)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridViewEx dgvSchedule;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDistance;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTypeOfFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCapacity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMoney;
        private System.Windows.Forms.DataGridViewButtonColumn colPlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn regular;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn deadline;
        private System.Windows.Forms.DataGridViewButtonColumn del;
    }
}