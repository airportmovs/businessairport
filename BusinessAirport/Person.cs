﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAirport
{
    [Serializable]
    /// <summary>
    /// Класс человек хранит в себе сведения о зарплате и настроении
    /// </summary>
    public class Person
    {
        int tempSalary; //текущая зарплата
        bool mood = true;//настроение, если true - не бунтует, false - бунтует
        DateTime dateRecruting; //Дата нанятия
        DateTime dateChangeSalary; // Дата последнего изменения зп
        int sumSalary; //зп за весь месяц

        /// <summary>
        /// Зарплата
        /// </summary>
        public int TempSalary
        {
            get { return tempSalary; }
            set { tempSalary = value; }
        }

        /// <summary>
        /// Настроение, true - не бунтует
        /// </summary>
        public bool Mood
        {
            get { return mood; }
            set { mood = value; }
        }

        /// <summary>
        /// Дата найма
        /// </summary>
        public DateTime DateRecruting
        {
            get { return dateRecruting; }
            set { dateRecruting = value; }
        }

        /// <summary>
        /// Дата последнего изменения зп
        /// </summary>
        public DateTime DateChangeSalary
        {
            get { return dateChangeSalary; }
            set { dateChangeSalary = value; }
        }

        /// <summary>
        /// Суммарная зарпалата за месяц
        /// </summary>
        public int SumSalary
        {
            get { return sumSalary; }
            set { sumSalary = value; }
        }

        /// <summary>
        /// Конструктор для человека
        /// </summary>
        /// <param name="tempSalary">Зарплата</param>
        /// <param name="dateRecruting">Дата найма</param>
        public Person(int tempSalary, DateTime dateRecruting)
        {
            this.tempSalary = tempSalary;
            this.mood = true;
            this.dateRecruting = dateRecruting;
            dateChangeSalary = dateRecruting;
            sumSalary = 0;
        }
        /// <summary>
        /// Выплата процента от зарплаты при увольнении
        /// </summary>
        /// <param name="dateNow">Текущая дата</param>
        /// <returns>Отрицательное целое число</returns>
        public int DismissalPayment(DateTime dateNow)
        {
            //sumSalary += TempSalary *
            //            (dateNow - DateChangeSalary).Days /
            //            DateTime.DaysInMonth(dateNow.Year, dateNow.Month);
            //return Convert.ToInt32(-sumSalary);
            return Convert.ToInt32(-(sumSalary + TempSalary *
                       (dateNow - DateChangeSalary).Days /
                       DateTime.DaysInMonth(dateNow.Year, dateNow.Month)));
        }
    }
}
