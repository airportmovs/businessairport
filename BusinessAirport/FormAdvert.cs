﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class FormAdvert : Form
    {
        Game game;
        MainForm mainForm;
        int cost;
        int costEnemy;
        public FormAdvert(MainForm mainForm, Game game)
        {
            InitializeComponent();
            this.game = game;
            this.mainForm = mainForm;
            btGo.Enabled = false;
            btGoC.Enabled = false;
        }

        /// <summary>
        /// Удаление бонусов
        /// </summary>
        /// <param name="e"></param>
        public void DelBonus(object e)
        {
            int number = (int) e;
            switch (number)
            {
                case 0:
                    game.PlayerRaiting -= 2;
                    cbRadio.Enabled = true; break;
                case 1:
                    game.PlayerRaiting -= 1;
                    cbPaper.Enabled = true; break;
                case 2:
                    game.PlayerRaiting -= 4;
                    cbTV.Enabled = true; break;
                case 3:
                    game.PlayerRaiting -= 3;
                    cbBanner.Enabled = true; break;
                case 4:
                    game.EnemyRaiting += 3;
                    cbAntiAdv.Enabled = true; break;
            }
            mainForm.RefreshRaiting();
        }

        /// <summary>
        /// Добавление бонусов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btGo_Click(object sender, EventArgs e)
        {
            if (mainForm.Balance > cost)
            {
                //game.Balance -= cost;
                mainForm.RefreshBalance("Покупка рекламы", -cost);
                if (cbRadio.Checked)
                {
                    game.PlayerRaiting += 2;
                    game.tMachine.AddToList(game.tMachine.MainTime.AddDays(10), DelBonus, 0);
                    cbRadio.Enabled = false;
                }
                if (cbPaper.Checked)
                {
                    game.PlayerRaiting += 1;
                    game.tMachine.AddToList(game.tMachine.MainTime.AddDays(7), DelBonus, 1);
                    cbPaper.Enabled = false;
                }
                if (cbTV.Checked)
                {
                    game.PlayerRaiting += 4;
                    game.tMachine.AddToList(game.tMachine.MainTime.AddMonths(1), DelBonus, 2);
                    cbTV.Enabled = false;
                }
                if (cbBanner.Checked)
                {
                    game.PlayerRaiting += 3;
                    game.tMachine.AddToList(game.tMachine.MainTime.AddMonths(1), DelBonus, 3);
                    cbBanner.Enabled = false;
                }
                mainForm.RefreshRaiting();
            }
            else
            {
                MessageBox.Show("Недостаточно средств!");
            }
            cbRadio.Checked = false;
            cbPaper.Checked = false;
            cbTV.Checked = false;
            cbBanner.Checked = false;
            cost = 0;
            lbCost.Text = cost.ToString();
            btGo.Enabled = false;
        }

        private void cbRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (cbRadio.Checked == true)
            { // на 10 дней
                cost += 750000;
            }
            else cost -= 750000;
            lbCost.Text = Convert.ToString(cost);
            btGo.Enabled = true;
        }

        private void cbPaper_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPaper.Checked == true)
            {  //на 7 дней
                cost += 100000;      
            }
            else cost -= 100000;
            lbCost.Text = Convert.ToString(cost);
            btGo.Enabled = true;
        }

        private void cbTV_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTV.Checked == true)
            {    // на 1 мес 
                cost += 1200000;
            }
            else cost -= 1200000;
            lbCost.Text = Convert.ToString(cost);
            btGo.Enabled = true;
        }

        private void cbBanner_CheckedChanged(object sender, EventArgs e)
        {
            if (cbBanner.Checked == true)
            {  // на 1 мес
                cost += 60000;
            }
            else cost -= 60000;
            lbCost.Text = Convert.ToString(cost);
            btGo.Enabled = true;
        }

        private void cbStrike_CheckedChanged(object sender, EventArgs e)
        {
            if (cbStrike.Checked == true)
            {  // на 1 раз
                costEnemy += 2000000;
            }
            else costEnemy -= 2000000;
            lbCostC.Text = Convert.ToString(costEnemy);
            btGoC.Enabled = true;
        }

        private void cbAntiAdv_CheckedChanged(object sender, EventArgs e)
        {
            if (cbAntiAdv.Checked == true)
            {  // на 10 дней
                costEnemy += 1000000;
            }
            else costEnemy -= 1000000;
            lbCostC.Text = Convert.ToString(costEnemy);
            btGoC.Enabled = true;
        }

       

        private void btGoC_Click(object sender, EventArgs e)
        {
            if (mainForm.Balance > costEnemy)
            {
                //game.Balance -= costEnemy;
                mainForm.RefreshBalance("Покупка антирекламы", -costEnemy);
                if (cbStrike.Checked)
                {
                    game.EnemyRaiting -= 7;
                    int yesORnot = Game.Random.Next(0, 10);
                    if (yesORnot % 3 == 0)
                        game.EnemyBalance -= Game.Random.Next(20000, 80000);
                    game.EnemyAverSalary += 1000;
                }
                if (cbAntiAdv.Checked)
                {
                    game.EnemyRaiting -= 3;
                    game.tMachine.AddToList(game.tMachine.MainTime.AddDays(10), DelBonus, 4);
                    cbAntiAdv.Enabled = false;
                }
                mainForm.RefreshRaiting();
            }
            else
            {
                MessageBox.Show("Недостаточно средств!");
            }
            cbAntiAdv.Checked = false;
            cbStrike.Checked = false;
            costEnemy = 0;
            lbCostC.Text = costEnemy.ToString();
            btGoC.Enabled = false;
        }

        public List<bool> GetButtonsState()
        {
            return new List<bool> { cbRadio.Enabled, cbPaper.Enabled, cbTV.Enabled, cbBanner.Enabled, cbAntiAdv.Enabled, cbStrike.Enabled };
        }
        public void SetButtonsState(List<bool> btState)
        {
            cbRadio.Enabled = btState[0];
            cbPaper.Enabled = btState[1];
            cbTV.Enabled = btState[2];
            cbBanner.Enabled = btState[3];
            cbAntiAdv.Enabled = btState[4];
            cbStrike.Enabled = btState[5];
        }
    }
}

