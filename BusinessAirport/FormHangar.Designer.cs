﻿namespace BusinessAirport
{
    partial class FormHangar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHangar));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvMyPlanes = new BusinessAirport.DataGridViewEx();
            this.NameOfPlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeOfOwnership = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeOfPlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carryWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.curCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lease = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consumption = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxDistance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hourPlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.curPerson = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planeCrew = new System.Windows.Forms.DataGridViewButtonColumn();
            this.gbNewPlane = new System.Windows.Forms.GroupBox();
            this.dgvShopPlanes = new BusinessAirport.DataGridViewEx();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typePlane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rentCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consumption100 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mPerson = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPurchase = new System.Windows.Forms.Button();
            this.btnSale = new System.Windows.Forms.Button();
            this.rbNewPlane = new System.Windows.Forms.RadioButton();
            this.rbOldPlane = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.gbOldPlane = new System.Windows.Forms.GroupBox();
            this.dgvShopOldPlanes = new BusinessAirport.DataGridViewEx();
            this.oldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hourDist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mPerson2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyPlanes)).BeginInit();
            this.gbNewPlane.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShopPlanes)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gbOldPlane.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShopOldPlanes)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dgvMyPlanes);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(5, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1267, 197);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Мои самолеты";
            // 
            // dgvMyPlanes
            // 
            this.dgvMyPlanes.AllowUserToAddRows = false;
            this.dgvMyPlanes.AllowUserToDeleteRows = false;
            this.dgvMyPlanes.AllowUserToResizeRows = false;
            this.dgvMyPlanes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMyPlanes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMyPlanes.BackgroundColor = System.Drawing.Color.White;
            this.dgvMyPlanes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMyPlanes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameOfPlane,
            this.typeOfOwnership,
            this.TypeOfPlane,
            this.carryWeight,
            this.curCity,
            this.speed,
            this.lease,
            this.consumption,
            this.maxDistance,
            this.hourPlane,
            this.curPerson,
            this.planeCrew});
            this.dgvMyPlanes.Location = new System.Drawing.Point(3, 18);
            this.dgvMyPlanes.MultiSelect = false;
            this.dgvMyPlanes.Name = "dgvMyPlanes";
            this.dgvMyPlanes.RowHeadersVisible = false;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(92)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgvMyPlanes.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMyPlanes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMyPlanes.Size = new System.Drawing.Size(1261, 176);
            this.dgvMyPlanes.TabIndex = 0;
            this.dgvMyPlanes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMyPlanes_CellContentClick);
            this.dgvMyPlanes.SelectionChanged += new System.EventHandler(this.dgvMyPlanes_SelectionChanged);
            // 
            // NameOfPlane
            // 
            this.NameOfPlane.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NameOfPlane.FillWeight = 63.07008F;
            this.NameOfPlane.HeaderText = "Название";
            this.NameOfPlane.Name = "NameOfPlane";
            this.NameOfPlane.ReadOnly = true;
            this.NameOfPlane.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // typeOfOwnership
            // 
            this.typeOfOwnership.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.typeOfOwnership.FillWeight = 79.31604F;
            this.typeOfOwnership.HeaderText = "Тип владения";
            this.typeOfOwnership.Name = "typeOfOwnership";
            this.typeOfOwnership.ReadOnly = true;
            this.typeOfOwnership.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TypeOfPlane
            // 
            this.TypeOfPlane.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TypeOfPlane.FillWeight = 83.28922F;
            this.TypeOfPlane.HeaderText = "Тип самолёта";
            this.TypeOfPlane.Name = "TypeOfPlane";
            this.TypeOfPlane.ReadOnly = true;
            this.TypeOfPlane.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // carryWeight
            // 
            this.carryWeight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.carryWeight.FillWeight = 137.8423F;
            this.carryWeight.HeaderText = "Гру-сть (т) / Вм-сть (чел)";
            this.carryWeight.Name = "carryWeight";
            this.carryWeight.ReadOnly = true;
            this.carryWeight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // curCity
            // 
            this.curCity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.curCity.FillWeight = 111.4148F;
            this.curCity.HeaderText = "Местоположение";
            this.curCity.Name = "curCity";
            this.curCity.ReadOnly = true;
            this.curCity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // speed
            // 
            this.speed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.speed.FillWeight = 67.45232F;
            this.speed.HeaderText = "Скорость";
            this.speed.Name = "speed";
            this.speed.ReadOnly = true;
            this.speed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // lease
            // 
            this.lease.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.lease.FillWeight = 184.0164F;
            this.lease.HeaderText = "Дней: аренды / до поступления / месяцев выплаты лизинга";
            this.lease.Name = "lease";
            this.lease.ReadOnly = true;
            this.lease.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // consumption
            // 
            this.consumption.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.consumption.FillWeight = 64.75419F;
            this.consumption.HeaderText = "Расход на 100 км";
            this.consumption.Name = "consumption";
            this.consumption.ReadOnly = true;
            this.consumption.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.consumption.Width = 84;
            // 
            // maxDistance
            // 
            this.maxDistance.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.maxDistance.FillWeight = 108.8446F;
            this.maxDistance.HeaderText = "Дальность полёта";
            this.maxDistance.Name = "maxDistance";
            this.maxDistance.ReadOnly = true;
            this.maxDistance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.maxDistance.Width = 80;
            // 
            // hourPlane
            // 
            this.hourPlane.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.hourPlane.HeaderText = "Часов в полёте";
            this.hourPlane.Name = "hourPlane";
            this.hourPlane.ReadOnly = true;
            // 
            // curPerson
            // 
            this.curPerson.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.curPerson.HeaderText = "Число человек экипажа";
            this.curPerson.Name = "curPerson";
            this.curPerson.ReadOnly = true;
            // 
            // planeCrew
            // 
            this.planeCrew.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.planeCrew.HeaderText = "Экипаж";
            this.planeCrew.MinimumWidth = 90;
            this.planeCrew.Name = "planeCrew";
            this.planeCrew.ReadOnly = true;
            // 
            // gbNewPlane
            // 
            this.gbNewPlane.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbNewPlane.Controls.Add(this.dgvShopPlanes);
            this.gbNewPlane.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.gbNewPlane.ForeColor = System.Drawing.SystemColors.Window;
            this.gbNewPlane.Location = new System.Drawing.Point(5, 250);
            this.gbNewPlane.Name = "gbNewPlane";
            this.gbNewPlane.Size = new System.Drawing.Size(1002, 222);
            this.gbNewPlane.TabIndex = 0;
            this.gbNewPlane.TabStop = false;
            this.gbNewPlane.Text = "Рынок новых самолётов";
            // 
            // dgvShopPlanes
            // 
            this.dgvShopPlanes.AllowUserToAddRows = false;
            this.dgvShopPlanes.AllowUserToDeleteRows = false;
            this.dgvShopPlanes.AllowUserToResizeRows = false;
            this.dgvShopPlanes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvShopPlanes.BackgroundColor = System.Drawing.Color.White;
            this.dgvShopPlanes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShopPlanes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.typePlane,
            this.Weight,
            this.Price,
            this.maxSpeed,
            this.rentCost,
            this.consumption100,
            this.Distance,
            this.mPerson});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvShopPlanes.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvShopPlanes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvShopPlanes.Location = new System.Drawing.Point(3, 19);
            this.dgvShopPlanes.MultiSelect = false;
            this.dgvShopPlanes.Name = "dgvShopPlanes";
            this.dgvShopPlanes.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShopPlanes.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvShopPlanes.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(92)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.dgvShopPlanes.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvShopPlanes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShopPlanes.Size = new System.Drawing.Size(996, 200);
            this.dgvShopPlanes.TabIndex = 1;
            // 
            // name
            // 
            this.name.HeaderText = "Название";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // typePlane
            // 
            this.typePlane.HeaderText = "Тип самолёта";
            this.typePlane.Name = "typePlane";
            this.typePlane.ReadOnly = true;
            this.typePlane.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Weight
            // 
            this.Weight.HeaderText = "Гру-сть (т) / Вм-сть (чел)";
            this.Weight.Name = "Weight";
            this.Weight.ReadOnly = true;
            this.Weight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Price
            // 
            this.Price.HeaderText = "Цена в магазине";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // maxSpeed
            // 
            this.maxSpeed.HeaderText = "Скорость";
            this.maxSpeed.Name = "maxSpeed";
            this.maxSpeed.ReadOnly = true;
            this.maxSpeed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // rentCost
            // 
            this.rentCost.HeaderText = "Стоимость аренды";
            this.rentCost.Name = "rentCost";
            this.rentCost.ReadOnly = true;
            this.rentCost.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // consumption100
            // 
            this.consumption100.HeaderText = "Расход на 100 км";
            this.consumption100.Name = "consumption100";
            this.consumption100.ReadOnly = true;
            this.consumption100.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Distance
            // 
            this.Distance.HeaderText = "Дальность полёта";
            this.Distance.Name = "Distance";
            this.Distance.ReadOnly = true;
            this.Distance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // mPerson
            // 
            this.mPerson.HeaderText = "Экипаж";
            this.mPerson.Name = "mPerson";
            this.mPerson.ReadOnly = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(1013, 324);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(259, 148);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Обозначения";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(6, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(241, 22);
            this.label4.TabIndex = 3;
            this.label4.Text = "Самолёт скоро будет получен";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.LightCoral;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(6, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Самолёт в лизинге";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.CornflowerBlue;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Самолёт в собственности";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Арендованный самолёт";
            // 
            // btnPurchase
            // 
            this.btnPurchase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPurchase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchase.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPurchase.ForeColor = System.Drawing.SystemColors.Control;
            this.btnPurchase.Location = new System.Drawing.Point(3, 3);
            this.btnPurchase.Name = "btnPurchase";
            this.btnPurchase.Size = new System.Drawing.Size(495, 34);
            this.btnPurchase.TabIndex = 3;
            this.btnPurchase.Text = "Покупка / аренда / лизинг";
            this.btnPurchase.UseVisualStyleBackColor = true;
            this.btnPurchase.Click += new System.EventHandler(this.btnPurchase_Click);
            // 
            // btnSale
            // 
            this.btnSale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSale.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSale.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSale.Location = new System.Drawing.Point(504, 3);
            this.btnSale.Name = "btnSale";
            this.btnSale.Size = new System.Drawing.Size(495, 34);
            this.btnSale.TabIndex = 4;
            this.btnSale.Text = "Продажа / отказ от аренды или лизинга";
            this.btnSale.UseVisualStyleBackColor = true;
            this.btnSale.Click += new System.EventHandler(this.btnSale_Click);
            // 
            // rbNewPlane
            // 
            this.rbNewPlane.AutoSize = true;
            this.rbNewPlane.Checked = true;
            this.rbNewPlane.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbNewPlane.FlatAppearance.BorderSize = 0;
            this.rbNewPlane.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black;
            this.rbNewPlane.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNewPlane.ForeColor = System.Drawing.Color.White;
            this.rbNewPlane.Location = new System.Drawing.Point(6, 31);
            this.rbNewPlane.Name = "rbNewPlane";
            this.rbNewPlane.Size = new System.Drawing.Size(77, 24);
            this.rbNewPlane.TabIndex = 5;
            this.rbNewPlane.TabStop = true;
            this.rbNewPlane.Text = "Новые";
            this.rbNewPlane.UseVisualStyleBackColor = true;
            this.rbNewPlane.CheckedChanged += new System.EventHandler(this.rbNewPlane_CheckedChanged);
            // 
            // rbOldPlane
            // 
            this.rbOldPlane.AutoSize = true;
            this.rbOldPlane.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbOldPlane.ForeColor = System.Drawing.Color.White;
            this.rbOldPlane.Location = new System.Drawing.Point(6, 66);
            this.rbOldPlane.Name = "rbOldPlane";
            this.rbOldPlane.Size = new System.Drawing.Size(135, 24);
            this.rbOldPlane.TabIndex = 6;
            this.rbOldPlane.Text = "Подержанные";
            this.rbOldPlane.UseVisualStyleBackColor = true;
            this.rbOldPlane.CheckedChanged += new System.EventHandler(this.rbOldPlane_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.groupBox4.Controls.Add(this.rbNewPlane);
            this.groupBox4.Controls.Add(this.rbOldPlane);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(1013, 206);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(259, 112);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Рынок самолётов";
            // 
            // gbOldPlane
            // 
            this.gbOldPlane.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbOldPlane.Controls.Add(this.dgvShopOldPlanes);
            this.gbOldPlane.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.gbOldPlane.ForeColor = System.Drawing.SystemColors.Window;
            this.gbOldPlane.Location = new System.Drawing.Point(5, 250);
            this.gbOldPlane.Name = "gbOldPlane";
            this.gbOldPlane.Size = new System.Drawing.Size(1002, 222);
            this.gbOldPlane.TabIndex = 2;
            this.gbOldPlane.TabStop = false;
            this.gbOldPlane.Text = "Рынок старых самолётов";
            this.gbOldPlane.Visible = false;
            // 
            // dgvShopOldPlanes
            // 
            this.dgvShopOldPlanes.AllowUserToAddRows = false;
            this.dgvShopOldPlanes.AllowUserToDeleteRows = false;
            this.dgvShopOldPlanes.AllowUserToResizeRows = false;
            this.dgvShopOldPlanes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvShopOldPlanes.BackgroundColor = System.Drawing.Color.White;
            this.dgvShopOldPlanes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShopOldPlanes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.oldName,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.hourDist,
            this.mPerson2});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvShopOldPlanes.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvShopOldPlanes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvShopOldPlanes.Location = new System.Drawing.Point(3, 19);
            this.dgvShopOldPlanes.MultiSelect = false;
            this.dgvShopOldPlanes.Name = "dgvShopOldPlanes";
            this.dgvShopOldPlanes.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShopOldPlanes.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvShopOldPlanes.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(92)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            this.dgvShopOldPlanes.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvShopOldPlanes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShopOldPlanes.Size = new System.Drawing.Size(996, 200);
            this.dgvShopOldPlanes.TabIndex = 1;
            // 
            // oldName
            // 
            this.oldName.HeaderText = "Название";
            this.oldName.Name = "oldName";
            this.oldName.ReadOnly = true;
            this.oldName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Тип самолёта";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Гру-сть (т) / Вм-сть (чел)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Цена в магазине";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Скорость";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Стоимость аренды";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Расход на 100 км";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Дальность полёта";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // hourDist
            // 
            this.hourDist.HeaderText = "Часы налёта";
            this.hourDist.Name = "hourDist";
            this.hourDist.ReadOnly = true;
            // 
            // mPerson2
            // 
            this.mPerson2.HeaderText = "Экипаж";
            this.mPerson2.Name = "mPerson2";
            this.mPerson2.ReadOnly = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btnSale, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPurchase, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 204);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1002, 40);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // FormHangar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.ClientSize = new System.Drawing.Size(1284, 484);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.gbOldPlane);
            this.Controls.Add(this.gbNewPlane);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 400);
            this.Name = "FormHangar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ангар";
            this.Shown += new System.EventHandler(this.FormHangar_Shown);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyPlanes)).EndInit();
            this.gbNewPlane.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShopPlanes)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gbOldPlane.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShopOldPlanes)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbNewPlane;
        private System.Windows.Forms.Button btnPurchase;
        private System.Windows.Forms.Button btnSale;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbNewPlane;
        private System.Windows.Forms.RadioButton rbOldPlane;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox gbOldPlane;
        private DataGridViewEx dgvMyPlanes;
        private DataGridViewEx dgvShopPlanes;
        public DataGridViewEx dgvShopOldPlanes;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameOfPlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeOfOwnership;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeOfPlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn carryWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn curCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn lease;
        private System.Windows.Forms.DataGridViewTextBoxColumn consumption;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxDistance;
        private System.Windows.Forms.DataGridViewTextBoxColumn hourPlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn curPerson;
        private System.Windows.Forms.DataGridViewButtonColumn planeCrew;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn typePlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn Weight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxSpeed;
        private System.Windows.Forms.DataGridViewTextBoxColumn rentCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn consumption100;
        private System.Windows.Forms.DataGridViewTextBoxColumn Distance;
        private System.Windows.Forms.DataGridViewTextBoxColumn mPerson;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn hourDist;
        private System.Windows.Forms.DataGridViewTextBoxColumn mPerson2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}