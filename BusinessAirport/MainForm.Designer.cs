﻿namespace BusinessAirport
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                game?.Dispose();
                components.Dispose();
            }                
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbEBalance = new System.Windows.Forms.Label();
            this.lbEnemy = new System.Windows.Forms.Label();
            this.lbImage = new System.Windows.Forms.Label();
            this.lbDateName = new System.Windows.Forms.Label();
            this.lbDate = new System.Windows.Forms.Label();
            this.lbTimeName = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.btPlayX1 = new System.Windows.Forms.Button();
            this.btPlayX2 = new System.Windows.Forms.Button();
            this.lbMoney = new System.Windows.Forms.Label();
            this.lbMoneyName = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvFlight = new BusinessAirport.DataGridViewEx();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnChoose = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.играToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новаяИграToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загрузитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btAdvert = new System.Windows.Forms.Button();
            this.btFlights = new System.Windows.Forms.Button();
            this.btCrew = new System.Windows.Forms.Button();
            this.btnAngar = new System.Windows.Forms.Button();
            this.dgvSchedule = new BusinessAirport.DataGridViewEx();
            this.colNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDistance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTypeOfFlight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCapacity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMoney = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expens = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPlane = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.regular = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deadline = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.del = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFlight)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSchedule)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.lbEBalance);
            this.groupBox1.Controls.Add(this.lbEnemy);
            this.groupBox1.Controls.Add(this.lbImage);
            this.groupBox1.Controls.Add(this.lbDateName);
            this.groupBox1.Controls.Add(this.lbDate);
            this.groupBox1.Controls.Add(this.lbTimeName);
            this.groupBox1.Controls.Add(this.lbTime);
            this.groupBox1.Controls.Add(this.btPlayX1);
            this.groupBox1.Controls.Add(this.btPlayX2);
            this.groupBox1.Controls.Add(this.lbMoney);
            this.groupBox1.Controls.Add(this.lbMoneyName);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 623);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1300, 51);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // lbEBalance
            // 
            this.lbEBalance.AutoSize = true;
            this.lbEBalance.Location = new System.Drawing.Point(640, 24);
            this.lbEBalance.Name = "lbEBalance";
            this.lbEBalance.Size = new System.Drawing.Size(155, 20);
            this.lbEBalance.TabIndex = 11;
            this.lbEBalance.Text = "Баланс конкурента";
            // 
            // lbEnemy
            // 
            this.lbEnemy.AutoSize = true;
            this.lbEnemy.Location = new System.Drawing.Point(469, 24);
            this.lbEnemy.Name = "lbEnemy";
            this.lbEnemy.Size = new System.Drawing.Size(27, 20);
            this.lbEnemy.TabIndex = 10;
            this.lbEnemy.Text = "50";
            // 
            // lbImage
            // 
            this.lbImage.AutoSize = true;
            this.lbImage.Location = new System.Drawing.Point(290, 24);
            this.lbImage.Name = "lbImage";
            this.lbImage.Size = new System.Drawing.Size(27, 20);
            this.lbImage.TabIndex = 9;
            this.lbImage.Text = "50";
            // 
            // lbDateName
            // 
            this.lbDateName.AutoSize = true;
            this.lbDateName.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbDateName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDateName.Location = new System.Drawing.Point(908, 24);
            this.lbDateName.Margin = new System.Windows.Forms.Padding(0);
            this.lbDateName.Name = "lbDateName";
            this.lbDateName.Size = new System.Drawing.Size(52, 20);
            this.lbDateName.TabIndex = 2;
            this.lbDateName.Text = "Дата:";
            // 
            // lbDate
            // 
            this.lbDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDate.Location = new System.Drawing.Point(960, 24);
            this.lbDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(150, 22);
            this.lbDate.TabIndex = 3;
            this.lbDate.Text = "1 января 2017 г.";
            // 
            // lbTimeName
            // 
            this.lbTimeName.AutoSize = true;
            this.lbTimeName.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTimeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbTimeName.Location = new System.Drawing.Point(1110, 24);
            this.lbTimeName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTimeName.Name = "lbTimeName";
            this.lbTimeName.Size = new System.Drawing.Size(62, 20);
            this.lbTimeName.TabIndex = 4;
            this.lbTimeName.Text = "Время:";
            // 
            // lbTime
            // 
            this.lbTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbTime.Location = new System.Drawing.Point(1172, 24);
            this.lbTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(50, 22);
            this.lbTime.TabIndex = 5;
            this.lbTime.Text = "0:00";
            // 
            // btPlayX1
            // 
            this.btPlayX1.BackgroundImage = global::BusinessAirport.Properties.Resources.playX1;
            this.btPlayX1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btPlayX1.Dock = System.Windows.Forms.DockStyle.Right;
            this.btPlayX1.FlatAppearance.BorderSize = 0;
            this.btPlayX1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPlayX1.Location = new System.Drawing.Point(1222, 24);
            this.btPlayX1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btPlayX1.Name = "btPlayX1";
            this.btPlayX1.Size = new System.Drawing.Size(37, 22);
            this.btPlayX1.TabIndex = 7;
            this.btPlayX1.UseVisualStyleBackColor = true;
            this.btPlayX1.Click += new System.EventHandler(this.btPlayX1_Click);
            // 
            // btPlayX2
            // 
            this.btPlayX2.BackgroundImage = global::BusinessAirport.Properties.Resources.playX2;
            this.btPlayX2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btPlayX2.Dock = System.Windows.Forms.DockStyle.Right;
            this.btPlayX2.FlatAppearance.BorderSize = 0;
            this.btPlayX2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPlayX2.Location = new System.Drawing.Point(1259, 24);
            this.btPlayX2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btPlayX2.Name = "btPlayX2";
            this.btPlayX2.Size = new System.Drawing.Size(37, 22);
            this.btPlayX2.TabIndex = 8;
            this.btPlayX2.UseVisualStyleBackColor = true;
            this.btPlayX2.Click += new System.EventHandler(this.btPlayX2_Click);
            // 
            // lbMoney
            // 
            this.lbMoney.AutoSize = true;
            this.lbMoney.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbMoney.Location = new System.Drawing.Point(90, 24);
            this.lbMoney.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbMoney.Name = "lbMoney";
            this.lbMoney.Size = new System.Drawing.Size(80, 20);
            this.lbMoney.TabIndex = 1;
            this.lbMoney.Text = "7 000 000";
            // 
            // lbMoneyName
            // 
            this.lbMoneyName.AutoSize = true;
            this.lbMoneyName.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbMoneyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbMoneyName.Location = new System.Drawing.Point(4, 24);
            this.lbMoneyName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbMoneyName.Name = "lbMoneyName";
            this.lbMoneyName.Size = new System.Drawing.Size(86, 20);
            this.lbMoneyName.TabIndex = 0;
            this.lbMoneyName.Text = "Баланс:  $";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.groupBox2.Controls.Add(this.dgvFlight);
            this.groupBox2.Controls.Add(this.btnChoose);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Window;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1031, 242);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Доска объявлений";
            // 
            // dgvFlight
            // 
            this.dgvFlight.AllowUserToAddRows = false;
            this.dgvFlight.AllowUserToDeleteRows = false;
            this.dgvFlight.AllowUserToResizeRows = false;
            this.dgvFlight.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFlight.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFlight.BackgroundColor = System.Drawing.Color.White;
            this.dgvFlight.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvFlight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFlight.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn1});
            this.dgvFlight.GridColor = System.Drawing.SystemColors.Window;
            this.dgvFlight.Location = new System.Drawing.Point(4, 24);
            this.dgvFlight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvFlight.MultiSelect = false;
            this.dgvFlight.Name = "dgvFlight";
            this.dgvFlight.ReadOnly = true;
            this.dgvFlight.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(92)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgvFlight.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFlight.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFlight.Size = new System.Drawing.Size(918, 214);
            this.dgvFlight.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "From";
            this.dataGridViewTextBoxColumn2.FillWeight = 88.36713F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Откуда";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "To";
            this.dataGridViewTextBoxColumn3.FillWeight = 71.65477F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Куда";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Distance";
            this.dataGridViewTextBoxColumn4.FillWeight = 125.0246F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Расстояние";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 123;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Type";
            this.dataGridViewTextBoxColumn5.FillWeight = 103.2557F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Тип рейса";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Weight";
            this.dataGridViewTextBoxColumn6.FillWeight = 175.4647F;
            this.dataGridViewTextBoxColumn6.HeaderText = "Грузподъемность";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 171;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Cost";
            this.dataGridViewTextBoxColumn7.FillWeight = 168.0034F;
            this.dataGridViewTextBoxColumn7.HeaderText = "Прибыль / Неустойка";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn12.DataPropertyName = "SHowOften";
            this.dataGridViewTextBoxColumn12.FillWeight = 80.80207F;
            this.dataGridViewTextBoxColumn12.HeaderText = "Регулярность";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SCompleteBefore";
            this.dataGridViewTextBoxColumn1.HeaderText = "Выполнить до";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // btnChoose
            // 
            this.btnChoose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnChoose.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnChoose.Location = new System.Drawing.Point(929, 22);
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Size = new System.Drawing.Size(99, 217);
            this.btnChoose.TabIndex = 1;
            this.btnChoose.Text = "Выбрать";
            this.btnChoose.UseVisualStyleBackColor = false;
            this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.играToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1300, 27);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // играToolStripMenuItem
            // 
            this.играToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаяИграToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.загрузитьToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.играToolStripMenuItem.Name = "играToolStripMenuItem";
            this.играToolStripMenuItem.Size = new System.Drawing.Size(51, 23);
            this.играToolStripMenuItem.Text = "Игра";
            // 
            // новаяИграToolStripMenuItem
            // 
            this.новаяИграToolStripMenuItem.Image = global::BusinessAirport.Properties.Resources.refresh_1;
            this.новаяИграToolStripMenuItem.Name = "новаяИграToolStripMenuItem";
            this.новаяИграToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.новаяИграToolStripMenuItem.Size = new System.Drawing.Size(200, 24);
            this.новаяИграToolStripMenuItem.Text = "Новая игра";
            this.новаяИграToolStripMenuItem.Click += new System.EventHandler(this.новаяИграToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Image = global::BusinessAirport.Properties.Resources.save;
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(200, 24);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // загрузитьToolStripMenuItem
            // 
            this.загрузитьToolStripMenuItem.Image = global::BusinessAirport.Properties.Resources.load;
            this.загрузитьToolStripMenuItem.Name = "загрузитьToolStripMenuItem";
            this.загрузитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.загрузитьToolStripMenuItem.Size = new System.Drawing.Size(200, 24);
            this.загрузитьToolStripMenuItem.Text = "Загрузить";
            this.загрузитьToolStripMenuItem.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Image = global::BusinessAirport.Properties.Resources.exit;
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(200, 24);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 27);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvSchedule);
            this.splitContainer1.Size = new System.Drawing.Size(1300, 596);
            this.splitContainer1.SplitterDistance = 242;
            this.splitContainer1.TabIndex = 9;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Size = new System.Drawing.Size(1300, 242);
            this.splitContainer2.SplitterDistance = 265;
            this.splitContainer2.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btAdvert, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btFlights, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btCrew, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAngar, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(265, 242);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btAdvert
            // 
            this.btAdvert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btAdvert.BackgroundImage = global::BusinessAirport.Properties.Resources.ad;
            this.btAdvert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btAdvert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btAdvert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btAdvert.ForeColor = System.Drawing.Color.Black;
            this.btAdvert.Location = new System.Drawing.Point(133, 122);
            this.btAdvert.Margin = new System.Windows.Forms.Padding(1);
            this.btAdvert.Name = "btAdvert";
            this.btAdvert.Size = new System.Drawing.Size(131, 119);
            this.btAdvert.TabIndex = 5;
            this.btAdvert.Text = "Имидж";
            this.btAdvert.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btAdvert.UseVisualStyleBackColor = false;
            this.btAdvert.Click += new System.EventHandler(this.btAdvert_Click);
            // 
            // btFlights
            // 
            this.btFlights.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btFlights.BackgroundImage = global::BusinessAirport.Properties.Resources.selectedFlights;
            this.btFlights.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btFlights.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btFlights.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btFlights.ForeColor = System.Drawing.Color.Black;
            this.btFlights.Location = new System.Drawing.Point(1, 122);
            this.btFlights.Margin = new System.Windows.Forms.Padding(1);
            this.btFlights.Name = "btFlights";
            this.btFlights.Size = new System.Drawing.Size(130, 119);
            this.btFlights.TabIndex = 3;
            this.btFlights.Text = "Взятые рейсы";
            this.btFlights.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btFlights.UseVisualStyleBackColor = false;
            this.btFlights.Click += new System.EventHandler(this.btFlights_Click);
            // 
            // btCrew
            // 
            this.btCrew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btCrew.BackgroundImage = global::BusinessAirport.Properties.Resources.crew;
            this.btCrew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCrew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btCrew.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btCrew.ForeColor = System.Drawing.Color.Black;
            this.btCrew.Location = new System.Drawing.Point(133, 1);
            this.btCrew.Margin = new System.Windows.Forms.Padding(1);
            this.btCrew.Name = "btCrew";
            this.btCrew.Size = new System.Drawing.Size(131, 119);
            this.btCrew.TabIndex = 4;
            this.btCrew.Text = "Экипаж";
            this.btCrew.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btCrew.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btCrew.UseVisualStyleBackColor = false;
            this.btCrew.Click += new System.EventHandler(this.btCrew_Click);
            // 
            // btnAngar
            // 
            this.btnAngar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnAngar.BackgroundImage = global::BusinessAirport.Properties.Resources.hangar;
            this.btnAngar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAngar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAngar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAngar.Location = new System.Drawing.Point(1, 1);
            this.btnAngar.Margin = new System.Windows.Forms.Padding(1);
            this.btnAngar.Name = "btnAngar";
            this.btnAngar.Size = new System.Drawing.Size(130, 119);
            this.btnAngar.TabIndex = 2;
            this.btnAngar.Text = "Ангар";
            this.btnAngar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAngar.UseVisualStyleBackColor = false;
            this.btnAngar.Click += new System.EventHandler(this.btAngar_Click);
            // 
            // dgvSchedule
            // 
            this.dgvSchedule.AllowUserToAddRows = false;
            this.dgvSchedule.AllowUserToDeleteRows = false;
            this.dgvSchedule.AllowUserToResizeRows = false;
            this.dgvSchedule.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSchedule.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.dgvSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSchedule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNumber,
            this.colFrom,
            this.colTo,
            this.colDistance,
            this.colTypeOfFlight,
            this.colCapacity,
            this.colMoney,
            this.expens,
            this.colPlane,
            this.colName,
            this.colStart,
            this.colEnd,
            this.regular,
            this.status,
            this.deadline,
            this.del});
            this.dgvSchedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSchedule.GridColor = System.Drawing.SystemColors.Window;
            this.dgvSchedule.Location = new System.Drawing.Point(0, 0);
            this.dgvSchedule.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvSchedule.MultiSelect = false;
            this.dgvSchedule.Name = "dgvSchedule";
            this.dgvSchedule.ReadOnly = true;
            this.dgvSchedule.RowHeadersVisible = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(92)))), ((int)(((byte)(122)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.dgvSchedule.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvSchedule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSchedule.Size = new System.Drawing.Size(1300, 350);
            this.dgvSchedule.TabIndex = 1;
            this.dgvSchedule.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSchedule_CellContentClick);
            this.dgvSchedule.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvSchedule_MouseDown);
            // 
            // colNumber
            // 
            this.colNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colNumber.DataPropertyName = "Number";
            this.colNumber.FillWeight = 49.36234F;
            this.colNumber.HeaderText = "№";
            this.colNumber.MinimumWidth = 10;
            this.colNumber.Name = "colNumber";
            this.colNumber.ReadOnly = true;
            // 
            // colFrom
            // 
            this.colFrom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colFrom.DataPropertyName = "From";
            this.colFrom.FillWeight = 88.36713F;
            this.colFrom.HeaderText = "Откуда";
            this.colFrom.Name = "colFrom";
            this.colFrom.ReadOnly = true;
            // 
            // colTo
            // 
            this.colTo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colTo.DataPropertyName = "To";
            this.colTo.FillWeight = 71.65477F;
            this.colTo.HeaderText = "Куда";
            this.colTo.Name = "colTo";
            this.colTo.ReadOnly = true;
            // 
            // colDistance
            // 
            this.colDistance.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDistance.DataPropertyName = "Distance";
            this.colDistance.FillWeight = 125.0246F;
            this.colDistance.HeaderText = "Расстояние";
            this.colDistance.MinimumWidth = 50;
            this.colDistance.Name = "colDistance";
            this.colDistance.ReadOnly = true;
            // 
            // colTypeOfFlight
            // 
            this.colTypeOfFlight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colTypeOfFlight.DataPropertyName = "PType";
            this.colTypeOfFlight.FillWeight = 103.2557F;
            this.colTypeOfFlight.HeaderText = "Тип рейса";
            this.colTypeOfFlight.MinimumWidth = 60;
            this.colTypeOfFlight.Name = "colTypeOfFlight";
            this.colTypeOfFlight.ReadOnly = true;
            // 
            // colCapacity
            // 
            this.colCapacity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colCapacity.DataPropertyName = "Fullness";
            this.colCapacity.FillWeight = 175.4647F;
            this.colCapacity.HeaderText = "Заполненость";
            this.colCapacity.MinimumWidth = 10;
            this.colCapacity.Name = "colCapacity";
            this.colCapacity.ReadOnly = true;
            this.colCapacity.Width = 50;
            // 
            // colMoney
            // 
            this.colMoney.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colMoney.DataPropertyName = "Profit";
            this.colMoney.FillWeight = 168.0034F;
            this.colMoney.HeaderText = "Прибыль / Неустойка";
            this.colMoney.Name = "colMoney";
            this.colMoney.ReadOnly = true;
            // 
            // expens
            // 
            this.expens.DataPropertyName = "Expense";
            this.expens.HeaderText = "Расходы";
            this.expens.Name = "expens";
            this.expens.ReadOnly = true;
            // 
            // colPlane
            // 
            this.colPlane.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colPlane.DataPropertyName = "BtPlane";
            this.colPlane.FillWeight = 71.59013F;
            this.colPlane.HeaderText = "Самолёт";
            this.colPlane.MinimumWidth = 90;
            this.colPlane.Name = "colPlane";
            this.colPlane.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.DataPropertyName = "Plane";
            this.colName.FillWeight = 115.6206F;
            this.colName.HeaderText = "Наименование";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colStart
            // 
            this.colStart.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colStart.DataPropertyName = "DTime";
            this.colStart.FillWeight = 79.8182F;
            this.colStart.HeaderText = "Отправка";
            this.colStart.Name = "colStart";
            this.colStart.ReadOnly = true;
            this.colStart.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colStart.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colStart.Width = 89;
            // 
            // colEnd
            // 
            this.colEnd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colEnd.DataPropertyName = "ATime";
            this.colEnd.FillWeight = 84.33332F;
            this.colEnd.HeaderText = "Прибытие";
            this.colEnd.Name = "colEnd";
            this.colEnd.ReadOnly = true;
            this.colEnd.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colEnd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colEnd.Width = 92;
            // 
            // regular
            // 
            this.regular.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.regular.DataPropertyName = "Regular";
            this.regular.FillWeight = 129.8397F;
            this.regular.HeaderText = "Регулярность";
            this.regular.MinimumWidth = 50;
            this.regular.Name = "regular";
            this.regular.ReadOnly = true;
            // 
            // status
            // 
            this.status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.status.DataPropertyName = "SStatus";
            this.status.FillWeight = 80.80207F;
            this.status.HeaderText = "Статус";
            this.status.MinimumWidth = 100;
            this.status.Name = "status";
            this.status.ReadOnly = true;
            // 
            // deadline
            // 
            this.deadline.DataPropertyName = "CBefore";
            this.deadline.HeaderText = "Выполнить до";
            this.deadline.Name = "deadline";
            this.deadline.ReadOnly = true;
            // 
            // del
            // 
            this.del.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.del.DataPropertyName = "BtCancel";
            this.del.FillWeight = 56.86351F;
            this.del.HeaderText = "Отказ";
            this.del.MinimumWidth = 90;
            this.del.Name = "del";
            this.del.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1300, 674);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Бизнес игра \"Аэропорт\"";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFlight)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSchedule)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btCrew;
        private System.Windows.Forms.Button btFlights;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbDateName;
        private System.Windows.Forms.Label lbDate;
        private System.Windows.Forms.Label lbTimeName;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label lbMoney;
        private System.Windows.Forms.Label lbMoneyName;
        private System.Windows.Forms.Button btPlayX2;
        private System.Windows.Forms.Button btPlayX1;
        private System.Windows.Forms.Button btnAngar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnChoose;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem играToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новаяИграToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem загрузитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btAdvert;
        private DataGridViewEx dgvSchedule;
        private DataGridViewEx dgvFlight;
        private System.Windows.Forms.Label lbImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDistance;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTypeOfFlight;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCapacity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMoney;
        private System.Windows.Forms.DataGridViewTextBoxColumn expens;
        private System.Windows.Forms.DataGridViewButtonColumn colPlane;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEnd;
        private System.Windows.Forms.DataGridViewTextBoxColumn regular;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn deadline;
        private System.Windows.Forms.DataGridViewButtonColumn del;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.Label lbEnemy;
        private System.Windows.Forms.Label lbEBalance;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}

