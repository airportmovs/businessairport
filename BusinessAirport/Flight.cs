﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAirport
{
    [Serializable]
    public class Flight : INotifyPropertyChanged
    {
        public static int[,] massDist = new int[,] {  { 0, 2649, 577, 937, 8254, 3286, 1783, 358, 8950, 1483, 9226, 2150, 5869, 1969, 5637, 430, 3241, 712, 1298, 1772, 2741, 9299, 5988, 2091, 6615 },
                                        { 2649, 0, 2202, 1717, 8094, 764, 1509, 2916, 11509, 2998, 11852, 2163, 8489, 1067, 8281, 2647, 3001, 1961, 1659, 2565, 1069, 9129, 8633, 1946, 9264 },
                                        { 577, 2202, 0, 524, 7870, 2895, 1206, 933, 9320, 1871, 9738, 1611, 6392, 1416, 6135, 878, 2737, 281, 1184, 1327, 2179, 8926, 6483, 2097, 7091 },
                                        { 937, 1717, 524, 0, 8075, 2385, 1055, 1237, 9830, 1812, 10163, 1672, 6804, 1098, 6574, 1035, 2829, 251, 765, 1585, 1873, 9138, 6925, 1693, 7549 },
                                        { 8254, 8094, 7870, 8075, 0, 8568, 7145, 8533, 8883, 9724, 11339, 6422, 10350, 7405, 9810, 8676, 5286, 8020, 8800, 6543, 7031, 1064, 9888, 9656, 9783 },
                                        { 3286, 764, 2895, 2385, 8568, 0, 2272, 3515, 12214, 3354, 12382, 2906, 9032, 1830, 8867, 3214, 3658, 2636, 2135, 3328, 1686, 9575, 9219, 2089, 9872 },
                                        { 1783, 1509, 1206, 1055, 7145, 2272, 0, 2136, 10142, 2866, 10829, 757, 7520, 442, 7223, 2026, 1860, 1143, 1677, 1056, 1033, 8208, 7564, 2512, 8135 },
                                        { 358, 2916, 933, 1237, 8533, 3515, 2136, 0, 8765, 1265, 8940, 2503, 5576, 2303, 5367, 344, 3582, 1036, 1435, 2103, 3079, 9569, 5719, 2111, 6360 },
                                        { 8950, 11509, 9320, 9830, 8883, 12214, 10142, 8765, 0, 9373, 2493, 9780, 3940, 10547, 3805, 9096, 9774, 9579, 10200, 9185, 11108, 8825, 3498, 10716, 2807 },
                                        { 1483, 2998, 1871, 1812, 9724, 3354, 2866, 1265, 9373, 0, 9073, 3441, 5774, 2839, 5696, 1054, 4593, 1775, 1366, 3194, 3569, 10774, 6043, 1379, 6732 },
                                        { 9226, 11852, 9738, 10163, 11339, 12382, 10829, 8940, 2493, 9073, 0, 10731, 3363, 11140, 3607, 9207, 11120, 9935, 10252, 10098, 11861, 11307, 3263, 10327, 2718 },
                                        { 2150, 2163, 1611, 1672, 6422, 2906, 757, 2503, 9780, 3441, 10731, 0, 7510, 1136, 7159, 2487, 1158, 1668, 2376, 633, 1360, 7479, 7482, 2943, 8003 },
                                        { 5869, 8489, 6392, 6804, 10350, 9032, 7520, 5576, 3940, 5774, 3363, 7510, 0, 7804, 540, 5837, 8109, 6571, 6889, 6886, 8536, 10861, 551, 7152, 1146 },
                                        { 1969, 1067, 1416, 1098, 7405, 1830, 442, 2303, 10547, 2839, 11140, 1136, 7804, 0, 7533, 2129, 2140, 1269, 1537, 1492, 775, 8455, 7869, 2009, 8464 },
                                        { 5637, 8281, 6135, 6574, 9810, 8867, 7223, 5367, 3805, 5696, 3607, 7159, 540, 7533, 0, 5649, 7693, 6333, 6729, 6529, 8247, 10322, 351, 6910, 1039 },
                                        { 430, 2647, 878, 1035, 8676, 3214, 2026, 344, 9096, 1054, 9207, 2487, 5837, 2129, 5649, 0, 3608, 882, 1105, 2160, 2903, 9713, 6000, 1480, 6654 },
                                        { 3241, 3001, 2737, 2829, 5286, 3658, 1860, 3582, 9774, 4593, 11120, 1158, 8109, 2140, 7693, 3608, 0, 2819, 3523, 1497, 1970, 6341, 7986, 4076, 8418 },
                                        { 712, 1961, 281, 251, 8020, 2636, 1143, 1036, 9579, 1775, 9935, 1668, 6571, 1269, 6333, 882, 2819, 0, 923, 1479, 2043, 9070, 6680, 1514, 7303 },
                                        { 1298, 1659, 1184, 765, 8800, 2135, 1677, 1435, 10200, 1366, 10252, 2376, 6889, 1537, 6729, 1105, 3523, 923, 0, 2341, 2223, 9855, 7080, 598, 7743 },
                                        { 1772, 2565, 1327, 1585, 6543, 3328, 1056, 2103, 9185, 3194, 10098, 633, 6886, 1492, 6529, 2160, 1497, 1479, 2341, 0, 1921, 7598, 6850, 2938, 7370 },
                                        { 2741, 1069, 2179, 1873, 7031, 1686, 1033, 3079, 11108, 3569, 11861, 1360, 8536, 775, 8247, 2903, 1970, 2043, 2223, 1921, 0, 8063, 8585, 2605, 9160 },
                                        { 9299, 9129, 8926, 9138, 1064, 9575, 8208, 9569, 8825, 10774, 11307, 7479, 10861, 8455, 10322, 9713, 6341, 9070, 9855, 7598, 8063, 0, 10343, 10417, 10140 },
                                        { 5988, 8633, 6483, 6925, 9888, 9219, 7564, 5719, 3498, 6043, 3263, 7482, 551, 7869, 351, 6000, 7986, 6680, 7080, 6850, 8585, 10343, 0, 7258, 702 },
                                        { 2091, 1946, 2097, 1693, 9656, 2089, 2512, 2111, 10716, 1379, 10327, 2943, 7152, 2009, 6910, 1480, 4076, 1514, 598, 2938, 2605, 10417, 7258, 0, 7941 },
                                        { 6615, 9264, 7091, 7549, 9783, 9872, 8135, 6360, 2807, 6732, 2718, 8003, 1146, 8464, 1039, 6654, 8418, 7303, 7743, 7370, 9160, 10140, 702, 7941, 0 }
                                    };      // матрица расстояний
        Flight originalFlight;
        int number;
        Cities from;            // откуда
        Cities to;              // куда
        int distance;           // расстояние
        int cost;               // стоимость
        PlaneType pType;        // требуемый тип самолета
        int weight;             // требуемая грузоподъемность
        HowOften howOften;      // частота для регулярного
        DateTime completeBefore;     // время исчезновения рейса
        int completeCount;
        int appointCount = 0;
        int randomHour;
        bool isEmptyFlight;
        bool backFlight;
        Status status;
        public event PropertyChangedEventHandler PropertyChanged;
        string btPlane = "Назначить";
        string btCancel = "Отказаться";
        List<FlightScheduleElem> fseList = new List<FlightScheduleElem>();

        public int Number { get { return number; } set { number = value; NotifyPropertyChanged("Number"); } }
        public Cities From { get { return from; } set { from = value; NotifyPropertyChanged("Flight"); } }
        public Cities To { get { return to; } set { to = value; NotifyPropertyChanged("To"); } }
        public int Distance { get { return distance; } set { distance = value; NotifyPropertyChanged("Distance"); } }
        public PlaneType Type { get { return pType; } set { pType = value; NotifyPropertyChanged("Type"); } }
        public int Weight { get { return weight; } set { weight = value; NotifyPropertyChanged("Weight"); } }
        public int Cost { get { return cost; } set { cost = value; NotifyPropertyChanged("Cost"); } }
        public HowOften HowOften { get { return howOften; } set { howOften = value; NotifyPropertyChanged("SHowOften"); } }
        public string SHowOften
        {
            get
            {
                string s = "";
                switch (howOften)
                {
                    case HowOften.Нет: s = "Нет"; break;
                    case HowOften.РазВДень: s = "Раз в день"; break;
                    case HowOften.РазВНеделю: s = "Раз в неделю"; break;
                    case HowOften.РазВМесяц: s = "Раз в месяц"; break;
                }
                return s;
            }

        }
        public DateTime CompleteBefore { get { return completeBefore; } set { completeBefore = value; NotifyPropertyChanged("SCompleteBefore"); } }
        public string SCompleteBefore { get { return completeBefore.ToString("dd.MM.yy # HH:mm"); } }
        public Status Status { get { return status; } set { status = value; NotifyPropertyChanged("SStatus"); } }
        public string SStatus
        {
            get
            {
                string s = "";
                switch (Status)
                {
                    case Status.Выполнен: s = "Выполнен"; break;
                    case Status.Выполняется: s = "Выполняется"; break;
                    case Status.НеВыполнен: s = "Не выполнен"; break;
                }
                return s;
            }
        }
        public bool IsEmptyFlight { get { return isEmptyFlight; } set { isEmptyFlight = value; } }
        public bool BackFlight { get { return backFlight; } set { backFlight = value; } }
        public int RandomHour { get { return randomHour; } set { randomHour = value; } }
        public int CompleteCount { get { return completeCount; } set { completeCount = value; NotifyPropertyChanged("CompleteCount"); } }
        public int AppointCount { get { return appointCount; } set { appointCount = value; } }
        public string BtPlane { get { return btPlane; } set { btPlane = value; NotifyPropertyChanged("BtPlane"); } }
        public string BtCancel { get { return btCancel; } set { btCancel = value; NotifyPropertyChanged("BtCancel"); } }
        public List<FlightScheduleElem> FseList { get { return fseList; } set { fseList = value; } }
        public Flight OriginalFlight { get { return originalFlight; } set { originalFlight = value; } }

        public void RefreshTime(DateTime time)
        {
            completeBefore = time;
        }

        /// <summary>
        /// Конструктор для генерации рейсов
        /// </summary>
        /// <param name="time">Время жизни рейса</param>
        public Flight(int number, DateTime time, DateTime curTime)
        {
            this.number = number;
            randomHour = Game.Random.Next(24);
            completeCount = 0;
            from = (Cities)Game.Random.Next(0, Enum.GetNames(typeof(Cities)).Length);
            do
                to = (Cities)Game.Random.Next(0, Enum.GetNames(typeof(Cities)).Length);
            while (to == from);     // рандомим обратный снова, если города совпадают

            distance = massDist[(int)from, (int)to];
            pType = (PlaneType)Game.Random.Next(0, Enum.GetNames(typeof(PlaneType)).Length);

            if (pType == PlaneType.Пассажирский)
            {
                weight = Game.Random.Next(85, 853);                 // минимальная вместимость самолета из магазина = 85, макс = 853
                if (Game.Random.Next(0, 7) == 0)                    // генерим регулярность для пассажирского
                    howOften = (HowOften)Game.Random.Next(1, Enum.GetNames(typeof(HowOften)).Length);
                else
                    howOften = HowOften.Нет;
            }
            else
            {
                weight = Game.Random.Next(27, 250);                 // минимальная вместимость самолета из магазина = 27, макс = 250
                howOften = HowOften.Нет;
            }

            // Доход: расстояние рейса * кол-во пассажиров(тонн) * 0,005 * коэф.времени(1,07 например)

            cost = Convert.ToInt32(distance * weight * 0.025);      // TO DO: зависимость от времени

            if (howOften == HowOften.Нет)
                completeBefore = time;
            if (howOften == HowOften.РазВДень)
                completeBefore = curTime.Date.AddDays(1);
            if (howOften == HowOften.РазВНеделю)
                completeBefore = curTime.Date.AddDays(7);
            if (howOften == HowOften.РазВМесяц)
                completeBefore = curTime.Date.AddMonths(1);
        }

        /// <summary>
        /// Конструктор для обратных рейсов
        /// </summary>
        /// <param name="f">Рейс</param>
        public Flight(int number, Flight flightFrom)
        {
            originalFlight = flightFrom;
            this.number = number;
            BackFlight = true;
            from = flightFrom.To;
            to = flightFrom.From;
            distance = massDist[(int)from, (int)to];
            pType = flightFrom.pType;
            howOften = flightFrom.howOften;
            weight = flightFrom.weight;
            cost = flightFrom.Cost;
            completeBefore = flightFrom.CompleteBefore;
            randomHour = flightFrom.randomHour;
        }

        public Flight(int number, Cities from, Cities to, PlaneType pType)
        {
            this.number = number;
            this.from = from;
            this.to = to;
            distance = massDist[(int)from, (int)to];
            this.pType = pType;
            howOften = HowOften.Нет;
            weight = 0;
            cost = Convert.ToInt32(distance * weight * 0.025);
        }

        public double GetCoeff(double hour)
        {
            // -(| hour |^ (1 / 4)) * 10 + 25;
            return (-Math.Pow(Math.Abs(hour - randomHour), 0.25) * 10 + 25) / 5;
        }

        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}