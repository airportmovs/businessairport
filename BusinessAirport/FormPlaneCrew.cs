﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class FormPlaneCrew : Form
    {
        Plane plane; //экземпляр класса самолет
        Game game; //экземпляр класса гэйм
        FormChangeSalary formChangeSalary; //форма для изменения зарплаты
        MainForm mainForm; //экземпляр класса главной формы
        bool privateDrag; //флаг для того, чтобы с рабочего стола нельзя было ничего перетащить
        bool fromCrew; //флаг, чтобы из одного листа в тот же самый лист нельзя было вставить
        FormRecruitPersons frmRecruitPersons; // экземпляр формы FormRecruitPersons

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="plane">Самолет, экипаж которого надо изменить</param>
        /// <param name="game"> Экземпляр класса гэйм</param>
        /// <param name="mainForm">Экземпляр класса MainForm</param>
        public FormPlaneCrew(Plane plane, Game game, MainForm mainForm)
        {
            InitializeComponent();
            lvPlaneCrew.Columns.Add("Зарплата");
            lvPlaneCrew.Columns.Add("Настроение");
            lvFreePersons.Columns.Add("Зарплата");
            lvFreePersons.Columns.Add("Настроение");
            lvPlaneCrew.Columns[0].Width = 100;
            lvPlaneCrew.Columns[1].Width = 100;
            lvFreePersons.Columns[0].Width = 100;
            lvFreePersons.Columns[1].Width = 100;
            this.plane = plane;
            this.game = game;
            this.mainForm = mainForm;
            frmRecruitPersons = new FormRecruitPersons(game, plane);
            Text = "Экипаж самолета "+ plane.name;
        }

        private void btnFromMarket_Click(object sender, EventArgs e)
        {

            frmRecruitPersons.ShowDialog();
            FillPlaneCrew();
        }

        private void FormPlaneCrew_Shown(object sender, EventArgs e)
        {
            FillPlaneCrew();
            FillFreePersons();
        }
        /// <summary>
        /// Заполнение listview списком экипажа самолета
        /// </summary>
        private void FillPlaneCrew()
        {
            lvPlaneCrew.Items.Clear();
            if (plane.persons != null)
            {
                foreach (Person person in plane.persons)
                {
                    var lvi = new ListViewItem(new[]
                    {
                        person.TempSalary.ToString(),
                        (person.Mood) ? "Нормальное " : "Бастует"
                    })
                    {
                        Tag = person
                    };
                    lvPlaneCrew.Items.Add(lvi);
                }
            }
        }
        /// <summary>
        /// Заполнение listview списком резервных людей
        /// </summary>
        private void FillFreePersons()
        {
            lvFreePersons.Items.Clear();
            if (game.ListOfFreePersons != null)
            {
                foreach (Person person in game.ListOfFreePersons)
                {
                    var lvi = new ListViewItem(new[]
                    {
                        person.TempSalary.ToString(),
                        (person.Mood) ? "Нормальное " : "Бастует"
                    })
                    {
                        Tag = person
                    };
                    lvFreePersons.Items.Add(lvi);
                }
            }
        }

        private void lvPlaneCrew_ItemDrag(object sender, ItemDragEventArgs e)
        {
            privateDrag = fromCrew =true;
            DoDragDrop(e.Item, DragDropEffects.Copy);
            privateDrag = false;
        }

        private void lvFreePersons_DragEnter(object sender, DragEventArgs e)
        {
            if (privateDrag) e.Effect = e.AllowedEffect;
        }

        private void lvFreePersons_DragDrop(object sender, DragEventArgs e)
        {
            if (privateDrag && fromCrew)
            {
                Person person = (Person)((ListViewItem)e.Data.GetData(typeof(ListViewItem))).Tag;
                var lvi = new ListViewItem(new[]
                        {
                        person.TempSalary.ToString(),
                        (person.Mood) ? "Нормальное " : "Бастует"
                    })
                {
                    Tag = person
                };
                lvFreePersons.Items.Add(lvi);
                game.ListOfFreePersons.Add(person);
                lvPlaneCrew.Items.Remove((ListViewItem)e.Data.GetData(typeof(ListViewItem)));
                plane.persons.Remove(person);
            }
        }

        private void lvFreePersons_ItemDrag(object sender, ItemDragEventArgs e)
        {
            fromCrew = false;
            privateDrag = true;
            DoDragDrop(e.Item, DragDropEffects.Copy);
            privateDrag = false;
        }

        private void lvPlaneCrew_DragEnter(object sender, DragEventArgs e)
        {
            if (privateDrag) e.Effect = e.AllowedEffect;
        }

        private void lvPlaneCrew_DragDrop(object sender, DragEventArgs e)
        {
            if (privateDrag && !fromCrew && (lvPlaneCrew.Items.Count < plane.maxPersons))
            {
                Person person = (Person)((ListViewItem)e.Data.GetData(typeof(ListViewItem))).Tag;
                var lvi = new ListViewItem(new[]
                        {
                        person.TempSalary.ToString(),
                        (person.Mood) ? "Нормальное " : "Бастует"
                    })
                {
                    Tag = person
                };
                lvPlaneCrew.Items.Add(lvi);
                plane.persons.Add(person);
                lvFreePersons.Items.Remove((ListViewItem)e.Data.GetData(typeof(ListViewItem)));
                game.ListOfFreePersons.Remove(person);
            }
        }

        private void btnDismiss_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem selectedItem in lvFreePersons.SelectedItems)
            {
                if (game.Balance + ((Person)(selectedItem.Tag)).DismissalPayment(game.tMachine.MainTime) < 0)
                {
                    MessageBox.Show("Извините, но у Вас не хватает денег, чтобы выплатить увольнительные.");
                }
                else
                {
                    // выплата процента от зп при увольнении
                    mainForm.RefreshBalance("Увольнение экипажа",((Person)(selectedItem.Tag)).DismissalPayment(game.tMachine.MainTime));
                    //удаление из списков
                    game.ListOfFreePersons.Remove((Person)(selectedItem.Tag));
                    lvFreePersons.Items.Remove(selectedItem);
                }
            }
        }

        private void btnChangeSalary_Click(object sender, EventArgs e)
        {
            Person p;
            foreach (ListViewItem selectedItem in lvPlaneCrew.SelectedItems)
            {
                p = (Person)selectedItem.Tag;
                int pred = p.TempSalary;
                formChangeSalary = new FormChangeSalary(p);
                if (formChangeSalary.ShowDialog() == DialogResult.OK)
                {
                    p.SumSalary += p.TempSalary *
                        (game.tMachine.MainTime - p.DateChangeSalary).Days /
                        DateTime.DaysInMonth(game.tMachine.MainTime.Year, game.tMachine.MainTime.Month);
                    p.TempSalary = formChangeSalary.Salary;
                    p.DateChangeSalary = game.tMachine.MainTime;
                    int r = (game.MyAverageSalary + game.EnemyAverSalary) / 2;
                    if (!p.Mood && p.TempSalary >= r)
                    {
                        p.Mood = true;
                    }
                }
            }
            FillPlaneCrew();
        }

    }
}
