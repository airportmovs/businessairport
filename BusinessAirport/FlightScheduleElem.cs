﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAirport
{
    [Serializable]
    /// <summary>
    /// Класс элемент расписания, хранит в себе сведения о рейсе, самолете,
    /// времения отправления и прибытия, а также множетеля за рейс.
    /// </summary>
    public class FlightScheduleElem : INotifyPropertyChanged
    {
        int number;

        Flight flight;                  // рейс
        Plane plane;                    // самолет
        DateTime departureTime;             // время отправления
        DateTime arrivalTime;               // время прибытия
        uint hourDistance; // количество  км налёта
        int profit;                     // прибыль   
        int expense;                    // расходы
        int multiplier;                 // множитель
        FlightScheduleElem brother;     // рейс-близнец в другую сторону
        Status status;                  // выполнен/выполняется/не выполнен
        int weight;

        string btPlane = "Изменить";
        string btCancel = "Отменить";

        public event PropertyChangedEventHandler PropertyChanged;

        public Flight Flight { get { return flight; } set { flight = value; NotifyPropertyChanged("Flight"); } }
        public Status Status { get { return status; } set { status = value; NotifyPropertyChanged("SStatus"); } }
        public string SStatus
        {
            get
            {
                string s = "";
                switch (Status)
                {
                    case Status.Выполнен: s = "Выполнен"; break;
                    case Status.Выполняется: s = "Выполняется"; break;
                    case Status.НеВыполнен: s = "Не выполнен"; break;
                }
                return s;
            }
        }
        public FlightScheduleElem Brother { get { return brother; } set { brother = value; } }
        public DateTime DepartureTime { get { return departureTime; } set { departureTime = value; NotifyPropertyChanged("DepartureTime"); } }
        public DateTime ArrivalTime { get { return arrivalTime; } set { arrivalTime = value; NotifyPropertyChanged("ArrivalTime"); } }
        public int Profit { get { return profit; } set { profit = value; NotifyPropertyChanged("Profit"); } }
        public int Expense { get { return expense; } set { expense = value; NotifyPropertyChanged("Expense"); } }
        public Plane Plane { get { return plane; } set { plane = value; NotifyPropertyChanged("Plane"); } }
        public string DTime { get { return departureTime.ToString("dd.MM.yy # HH:mm"); } }
        public string ATime { get { return arrivalTime.ToString("dd.MM.yy # HH:mm"); } }
        public int Number { get { return number; } set { number = value; NotifyPropertyChanged("Number"); } }
        public string From { get { return flight.From.ToString(); } }
        public string To { get { return flight.To.ToString(); } }
        public string Distance { get { return flight.Distance.ToString(); } }
        public string PType { get { return flight.Type.ToString(); } }
        public string CBefore { get { return flight.CompleteBefore.ToString("dd.MM.yy # HH:mm"); } }
        public string Regular { get { return flight.SHowOften; } }
        public string BtPlane { get { return btPlane; } set { btPlane = value; NotifyPropertyChanged("BtPlane"); } }
        public string BtCancel { get { return btCancel; } set { btCancel = value; NotifyPropertyChanged("BtCancel"); } }
        public string Fullness { get { return (plane != null ? (double)weight / plane.carryWeight : 0).ToString("P", MainForm.nfi); } }
        public int Multiplier { get { return multiplier; } set { multiplier = value; } }
        public int Weight { get { return weight; } set { weight = value; } }

        /// <summary>
        /// Конструктор для элемента расписания
        /// </summary>
        /// <param name="f">Рейс</param>
        /// <param name="p">Самолет</param>
        /// <param name="dTime">Время отправления</param>
        /// <param name="aTime">Время прибытия</param>
        /// <param name="mult">Множитель</param>
        public FlightScheduleElem(int number, Flight f, Plane p, DateTime dTime, DateTime aTime, int mult)
        {
            this.number = number;
            flight = f;
            plane = p;
            departureTime = dTime;
            arrivalTime = aTime;
            multiplier = mult;
            CalcOfExpenses();
            weight = flight.Weight;
        }

        /// <summary>
        /// Конструктор для случая постановки в расписание 
        /// с будущей возможностью назначить самолет
        /// </summary>
        /// <param name="f">Рейс</param>
        /// <param name="mult">Множитель</param>
        public FlightScheduleElem(int number, Flight f, int mult)
        {
            this.number = number;
            flight = f;
            multiplier = mult;
            weight = flight.Weight;
        }

        /// <summary>
        /// Назначить самолет на этот рейс
        /// </summary>
        /// <param name="p">Самолет</param>
        /// <param name="dTime">Время отправления</param>
        /// <param name="aTime">Время прибытия</param>
        public void Appoint(Plane p, DateTime dTime, DateTime aTime)
        {
            Plane = p;
            DepartureTime = dTime;
            ArrivalTime = aTime;
            CalcOfExpenses();
        }

        public void AddBrother(FlightScheduleElem brother)
        {
            this.brother = brother;
        }

        /// <summary>
        /// Начать полет
        /// </summary>
        public void StartFlight()
        {
            CalcOfExpenses();
            Plane.IsFlight = true;
            flight.Status = Status = Status.Выполняется;
            BtCancel = BtPlane = "";
        }
        /// <summary>
        /// Завершить полет
        /// </summary>
        public void EndFlight()
        {
            if (brother != null)
                brother = brother.Brother = null;
            plane.IsFlight = false;
            plane.curCity = flight.To;
            flight.Status = Status = Status.Выполнен;
            CalcOfDist();
        }

        /// <summary>
        /// Подсчет расходов
        /// </summary>
        private void CalcOfExpenses()
        {
            CalcExpenses();
            CalcProfit();
        }

        public void CalcProfit()
        {
            //Прибыль рейса = доход - расход
            double profitCoeff = flight.Type == PlaneType.Пассажирский ? flight.GetCoeff(departureTime.Hour) : 20;
            int randomCoeff = Game.Random.Next(5);
            Profit = Convert.ToInt32(((flight.Cost * profitCoeff) / (0.7 + multiplier * 0.3)) * (1 + (Game.playerRating - Game.enemyRating + randomCoeff) / 100));
            Weight = multiplier == 0 ? 0 : Convert.ToInt32((flight.Weight * (1 + profitCoeff / 20 + (Game.playerRating - Game.enemyRating + randomCoeff) / 100)) / (0.7 + multiplier * 0.3));
            if (Plane != null)
            {
                if (Weight > plane.carryWeight)
                    Weight = plane.carryWeight;
                else
                    if (Weight < 0)
                    Weight = 0;
            }
        }

        public void CalcExpenses()
        {
            //(Расстояние рейса / скорость самолета) × расход топлива × 0,039 + цена самолета × (1 + налет часов × 0,00007) × 0,001
            if ( plane != null)
                Expense = Convert.ToInt32(((double)flight.Distance / plane.speed) * plane.consumption * Game.FuelCost + ((plane.shopPrice + plane.shopPrice * ((double)plane.allDistance / plane.speed) * 0.00007) * 0.001) * (1 + (double)plane.allDistance / plane.speed * 0.00007));
        }
        /// <summary>
        /// Подсчёт расстояний, которые пролетел самолёт
        /// </summary>
        private void CalcOfDist()
        {
            hourDistance = Convert.ToUInt32(flight.Distance);
            plane.allDistance += hourDistance;
        }

        public void CantStart(bool back = false)
        {
            if (back)
                BtPlane = "";
            BtCancel = "Отменить";
        }

        public void ChangeAccept()
        {
            BtCancel = "Отменить";
            BtPlane = "Изменить";
        }

        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}