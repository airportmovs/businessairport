﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessAirport
{
    [Serializable]
    class EventElemSer
    {
        string name;
        DateTime t;
        Object param;

        public string Name
        {
            get { return name; }
        }

        public DateTime Time
        {
            get { return t; }
        }

        public Object Param
        {
            get { return param; }
        }

        public EventElemSer(DateTime dt, EventElem ee)
        {
            name = ee.SomeMethod.Method.Name;
            t = dt;
            param = ee.param;
        }
    }
}
