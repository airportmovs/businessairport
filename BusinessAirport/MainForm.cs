﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class MainForm : Form
    {
        Game game;
        int day;
        public bool isGameOver = false;
        bool isGameStart = false;
        bool isGameLoad = false;
        public static NumberFormatInfo nfi = new CultureInfo("ru", false).NumberFormat;
        public FormHangar formHangar;
        public FormPlayerFlights pFlightsForm;
        public FormAllMyCrew formAllMyCrew;
        public FormAdvert formAdvert;
        public long Balance
        {
            get { return game.Balance; }
            set { game.Balance = value; }
        }

        public MainForm()
        {
            InitializeComponent();
            dgvSchedule.AutoGenerateColumns = dgvFlight.AutoGenerateColumns = false;
            nfi.NumberGroupSeparator = " ";
            nfi.NumberDecimalDigits = 0;
            nfi.PercentDecimalDigits = 1;
            dgvSchedule.DefaultCellStyle.Font = new Font("Tahoma", 10);
        }


        /// <summary>
        /// Метод для обновления рейтинга в т.ч. и на форме
        /// </summary>
        /// <param name="amount">Значение изменения рейтинга, по умолчаниюю 0</param>
        public void RefreshRaiting(int amount = 0)
        {
            game.PlayerRaiting += amount;
            if (game.PlayerRaiting > 100)
            {
                game.PlayerRaiting = 100;
            }
            if(game.PlayerRaiting<0) game.PlayerRaiting = 0;
            if (game.EnemyRaiting > 100)
            {
                game.EnemyRaiting = 100;
            }
            if (game.EnemyRaiting < 0) game.EnemyRaiting = 0;
            lbImage.BeginInvoke((MethodInvoker)delegate
            {
                lbImage.Text = game.PlayerRaiting.ToString();
            });
            lbEnemy.BeginInvoke((MethodInvoker)delegate
            {
                lbEnemy.Text = game.EnemyRaiting.ToString();
            });
        }

        /// <summary>
        /// Метод для обновления баланса в т.ч. и на форме
        /// </summary>
        /// <param name="amount">Значение изменения баланса, по умолчаниюю 0</param>
        public void RefreshBalance(string text, long amount = 0)
        {
            PopUpBalance(amount, text);
            game.Balance += amount;
            lbMoney.BeginInvoke((MethodInvoker)delegate
            {
                lbMoney.Text = game.Balance.ToString("N", nfi);
            });
            if (!isGameOver && game.Balance < 0)
            {
                if (game.tMachine.Status)
                    game.tMachine.Stop();
                isGameOver = true;
                MessageBox.Show("Ваш баланс ниже нуля, но у вас есть возможность продать несколько самолетов");
                formHangar.ShowDialog();
                if (game.Balance > 0) //-V3022
                {
                    isGameOver = false;
                    game.tMachine.Start();
                }
                else
                {
                    MessageBox.Show("               ВЫ БАНКРОТ! \n          ИГРА ЗАКОНЧЕНА\n                         🙁", "GameOver");
                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        Close();
                    });
                }

            }
        }

        /// <summary>
        /// Всплывающий лейбл об обновлении баланса
        /// </summary>
        /// <param name="amount">сумма</param>
        /// <param name="text">текст сообщения</param>
        public void PopUpBalance(long amount, string text)
        {
            this.BeginInvoke((MethodInvoker)delegate
            {
                Controls.OfType<Label>().Where(label => label.Name == "PopUpLabel").ToList().ForEach(l => l.Location = new Point(l.Location.X, l.Location.Y - 27));
                Label lb = new Label();            
                lb.Name = "PopUpLabel";
                lb.AutoSize = true;
                lb.Location = new Point(lbMoney.Location.X, groupBox1.Location.Y);
                lb.Text = (amount > 0? "+" : "") + amount.ToString("N", nfi) +" "+ text;
                Label lbBack = new Label();
                lbBack.Name = "PopUpLabel";
                lbBack.Location = new Point(lbMoney.Location.X - 2, groupBox1.Location.Y - 2);
                lbBack.BackColor = Color.Black;
                lb.ForeColor = amount >= 0 ? Color.Green : Color.Red;
                this.Controls.Add(lb);
                this.Controls.Add(lbBack);
                lbBack.Height = lb.Height + 4;
                lbBack.Width = lb.Width + 4;
                lb.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
                lbBack.Anchor = lb.Anchor;
                lbBack.BringToFront();
                lb.BringToFront();

                var t = new Timer();
                t.Interval = 5000; 
                t.Tick += (s, e) =>
                {
                    lbBack.Dispose();
                    lb.Dispose();
                    t.Stop();
                    t.Dispose();
                };
                t.Start();
            });
        }

        /// <summary>
        /// обновить значение баланса врага на форме
        /// </summary>
        public void RefreshEnemyBalance()
        {
            lbEBalance.BeginInvoke((MethodInvoker)delegate
            {
                lbEBalance.Text = game.EnemyBalance.ToString();
            });
        }

        /// <summary>
        /// Начало новой игры
        /// </summary>
        private void NewGame()
        {
            game?.tMachine.Stop();
            game?.Dispose();
            game = new Game(dgvFlight, dgvSchedule, this);
            InitializeGame();
            formHangar = new FormHangar(game, this);
            game.Hangar.PlaneList = new List<Plane>()
            {
                new Plane("Airbus А380", OwnType.Buy,    PlaneType.Пассажирский, 853,    1020,   12000, 15400, Cities.Москва, 16500000, 0,   55000, 0, 20, 0),
                new Plane("Ан-225 «Мрия»",  OwnType.Buy,   PlaneType.Грузовой, 150,    850,    15900, 10000, Cities.Москва, 17490000, 0,   58300, 0, 6, 0)
            };
            for (int i = 0; i < 20; i++)
                game.Hangar.PlaneList[0].persons.Add(new Person(30000, game.tMachine.MainTime));
            for (int i = 0; i < 6; i++)
                game.Hangar.PlaneList[1].persons.Add(new Person(30000, game.tMachine.MainTime));
            game.Hangar.ListOfPlaneEnemy = new List<Plane>()
            {
                new Plane("Airbus А380", OwnType.Buy,    PlaneType.Пассажирский, 853,    1020,   12000, 15400, Cities.Москва, 16500000, 0,   55000, 0, 20, 0),
                new Plane("Ан-225 «Мрия»",  OwnType.Buy,   PlaneType.Грузовой, 150,    850,    15900, 10000, Cities.Москва, 17490000, 0,   58300, 0, 6, 0)
            };
            InitializeLabels();
            game.tMachine.refreshTime += RefreshTime;
            game.tMachine.Startx1();
            formHangar.CheckOld(null);
            formHangar.CheckOldEnemy(null);
            game.tMachine.AddToList(game.tMachine.MainTime.AddDays(Game.Random.Next(10, 20)), game.AntiAdv, null);
            game.tMachine.AddToList(game.tMachine.MainTime.AddDays(Game.Random.Next(1, 2)), game.EnemyAdv, null);
            game.tMachine.AddToList(game.tMachine.MainTime.AddDays(4 + Game.Random.Next(15)), formHangar.EnemySalePlane, null);
        }

        /// <summary>
        /// Инициализация объкта game
        /// </summary>
        private void InitializeGame()
        {
            game?.Dispose();
            game = new Game(dgvFlight, dgvSchedule, this);
            if (!isGameLoad)
                game.GenerateFlights();
            isGameStart = true;
            formAllMyCrew = new FormAllMyCrew(game, this);
            formAdvert = new FormAdvert(this, game);
            pFlightsForm = new FormPlayerFlights(this, game);
        }

        /// <summary>
        /// Инициализация лейблов на форме в соответствии со значением в game
        /// </summary>
        private void InitializeLabels()
        {
            dgvSchedule.DataSource = game.ListOfFlightSheduleElem;
            dgvFlight.DataSource = game.ListOfFlights;
            lbTime.Text = game.tMachine.MainTime.ToShortTimeString();
            day = game.tMachine.MainTime.Day;
            lbDate.Text = game.tMachine.MainTime.ToLongDateString();
            lbMoney.Text = game.Balance.ToString("N", nfi);
            lbImage.Text = game.PlayerRaiting.ToString();
            lbEnemy.Text = game.EnemyRaiting.ToString();
            RefreshEnemyBalance();
        }

        /// <summary>
        /// Обновление времени на форме в соотвествии с объектом
        /// </summary>
        private void RefreshTime(object sender, EventArgs e)
        {
            lbTime.Invoke((MethodInvoker)delegate
            {
                // Запуск кода в основном потоке, так как нельзя изменять формы, 
                // созданные в основном потоке, второстепенным потоком
                lbTime.Text = game.tMachine.Time();
            });
            if (day != game.tMachine.MainTime.Day)
            {
                lbDate.Invoke((MethodInvoker)delegate
                {
                    game.NewDay();
                    lbDate.Text = game.tMachine.MainTime.ToLongDateString();
                });
                day = game.tMachine.MainTime.Day;
            }
        }

        private void btAngar_Click(object sender, EventArgs e)
        {
            if (isGameStart)
            {
                game.tMachine.Stop();
                formHangar.ShowDialog();
                game.tMachine.Start();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            game?.Save();
        }

        private void btPlayX1_Click(object sender, EventArgs e)
        {
            game?.tMachine.Startx1();
        }

        private void btPlayX2_Click(object sender, EventArgs e)
        {            
            game?.tMachine.IncSpeed();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            game?.tMachine.Stop();
            isGameLoad = true;
            InitializeGame();
            game.Load();
            InitializeLabels();
            game.tMachine.refreshTime += RefreshTime;
            game.tMachine.Startx1();
            formHangar = new FormHangar(game, this);
            pFlightsForm = new FormPlayerFlights(this, game);
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            game?.ChooseClick();
            //dgvFlight.Refresh();
        }

        private void dgvSchedule_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!isGameStart)
                return;
            // нажали кнопку в столбце Отказ
            if (e.RowIndex != -1 && dgvSchedule.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                    (string)dgvSchedule.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == "Отменить")
            {
                if (game.ListOfFlightSheduleElem[e.RowIndex].Status == Status.НеВыполнен)           // если рейс не выполнен
                {
                    if (game.ListOfFlightSheduleElem[e.RowIndex].Brother != null)                   // если рейс регулярный (есть обратный рейс)
                    {
                        // если выполняется прямой рейс - выходим
                        //if (game.ListOfFlightSheduleElem[e.RowIndex].Brother.Status == Status.Выполняется)
                        //    return;
                    }
                    bool isTicking = game.tMachine.Status;
                    if (isTicking)
                        game.tMachine.Stop();
                    string message;
                    message = "Вы уверены, что хотите ОТМЕНИТЬ рейс?";
                    if (MessageBox.Show(message, "Отмена рейса", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        game.DelFlight(e.RowIndex);                                                 // удаляем рейс из расписания
                    }
                    if (isTicking)
                        game.tMachine.Start();
                }
            }
            // нажали кнопку в столбце Самолет
            else
            {
                bool isTicking = game.tMachine.Status;
                if (isTicking)
                    game.tMachine.Stop();
                if (e.RowIndex != -1 && dgvSchedule.Columns[e.ColumnIndex] is DataGridViewButtonColumn
                    && (string)dgvSchedule.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == "Изменить")        // при нажатии на пустую кнопку не выводим форму
                {
                    FormAppoint formAppoint;
                    bool isAlreadyInSchedule = game.ListOfFlightSheduleElem[e.RowIndex].Plane != null;
                    formAppoint = new FormAppoint(this, game, game.ListOfFlightSheduleElem[e.RowIndex], true);
                    if (formAppoint.ShowDialog() == DialogResult.OK)
                        game.ChangeFlight(e.RowIndex, isAlreadyInSchedule);
                }
                if (isTicking)
                    game.tMachine.Start();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isGameOver && isGameStart)
            {
                game.tMachine.Stop();
                DialogResult dialogResult = MessageBox.Show("Желаете сохранить игру перед выходом?", "Сохранение игры", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                    game.Save();
            }
        }

        private void новаяИграToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isGameLoad = false;
            NewGame();
        }

        private void btFlights_Click(object sender, EventArgs e)
        {
            pFlightsForm?.ShowDialog();
        }

        private void btCrew_Click(object sender, EventArgs e)
        {
            if (isGameStart)
            {
                game.tMachine.Stop();
                formAllMyCrew.ShowDialog();
                game.tMachine.Start();
            }
        }

        private void dgvSchedule_MouseDown(object sender, MouseEventArgs e)
        {
            dgvSchedule.ClearSelection();
        }

        private void btAdvert_Click(object sender, EventArgs e)
        {
            if (isGameStart)
            {
                game.tMachine.Stop();
                formAdvert.ShowDialog();
                game.tMachine.Start();
            }
        }
    }
}
