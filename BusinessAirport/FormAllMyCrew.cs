﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    public partial class FormAllMyCrew : Form
    {
        Hangar hangar; //Экземпляр класса ангар
        List<Person> freePersons; //Список резервных людей
        Game game;
        MainForm mainForm;
        bool initialization; //инициализация формы
        FormRecruitPersons frmRecruitPersons;

        /// <summary>
        /// Конструктор для формы всех работников
        /// </summary>
        /// <param name="hangar">Экземпляр класса ангар</param>
        /// <param name="freePersons">Список резервных людей</param>
        public FormAllMyCrew(Game g, MainForm m)
        {
            InitializeComponent();
            hangar = g.Hangar;
            freePersons = g.ListOfFreePersons;
            game = g;
            mainForm = m;
            frmRecruitPersons = new FormRecruitPersons(game, this);
            
        }

        private void btnRecruit_Click(object sender, EventArgs e)
        {
            frmRecruitPersons.ShowDialog();
            FillDGV();
        }

        private void FormAllMyCrew_Shown(object sender, EventArgs e)
        {
            initialization = true;
            FillDGV();
            initialization = false;
        }
        /// <summary>
        /// Заполнение dgv на форме
        /// </summary>
        public void FillDGV()
        {
            if (dgvAllPersons.Rows.Count != 0) dgvAllPersons.Rows.Clear();
            if (hangar.PlaneList != null)
            {
                foreach (Plane plane in hangar.PlaneList)
                {
                    string inFlight;
                    if (!plane.IsFlight)
                    {
                        inFlight = "Нет";
                    }
                    else
                    {
                        inFlight = "В полете";
                    }
                    if (plane.persons != null)
                    {
                        foreach (Person person in plane.persons)
                        {

                            dgvAllPersons.Rows.Add(
                                person.TempSalary,
                                plane.name,
                                inFlight,
                                (person.Mood) ? "Нормальное " : "Бастует",
                                "Уволить");
                            dgvAllPersons.Rows[dgvAllPersons.Rows.Count - 1].Tag = Tuple.Create((Person)person, (Plane)plane);
                        }
                    }
                }
            }
            if (freePersons != null)
            {
                foreach (Person person in freePersons)
                {
                    dgvAllPersons.Rows.Add(
                        person.TempSalary,
                        "Не назначен",
                        "Нет",
                        (person.Mood) ? "Нормальное " : "Бастует",
                        "Уволить");
                    dgvAllPersons.Rows[dgvAllPersons.Rows.Count - 1].Tag = Tuple.Create((Person)person, (Plane)null);
                }
            }
        }

        private void dgvAllPersons_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (IsHeaderButtonCell(e))
            {
                //если человек "в полете" , то нельзя уволить
                if ((string)dgvAllPersons[e.ColumnIndex - 2, e.RowIndex].Value != "Нет")
                {
                    MessageBox.Show("Человек в полете. Его нельзя уволить!");
                }
                else
                {
                    Tuple<Person, Plane> selectedItemTagAsTuple = (Tuple<Person, Plane>)dgvAllPersons.Rows[e.RowIndex].Tag;
                    Person person = selectedItemTagAsTuple.Item1;
                    Plane plane = selectedItemTagAsTuple.Item2;
                    //если человек не прикреплен к самолету
                    if (plane == null)
                    {
                        //вычитаем из баланса "увольнительные"
                        mainForm.RefreshBalance("Увольнение члена экипажа", person.DismissalPayment(game.tMachine.MainTime));
                        freePersons.Remove(person);
                    }
                    //если человек прикреплен к самолету
                    else
                    {
                        foreach (Plane p in hangar.PlaneList)
                        {
                            if (p == plane)
                            {
                                //вычитаем из баланса "увольнительные"
                                if (game.Balance + person.DismissalPayment(game.tMachine.MainTime) < 0)
                                {
                                    MessageBox.Show("Извините, но у Вас не хватает денег, чтобы выплатить увольнительные.");                                    
                                }
                                else
                                {
                                    mainForm.RefreshBalance("Увольнение члена экипажа", person.DismissalPayment(game.tMachine.MainTime));
                                    p.persons.Remove(person);
                                }
                            }
                        }
                    }
                    dgvAllPersons.Rows.RemoveAt(e.RowIndex);
                }
            }
        }
        /// <summary>
        /// Проверка является ли выбранная ячейка DGV кнопкой
        /// </summary>
        /// <param name="cellEvent">Событие нажатия на DGV (DataGridViewCellEventArgs)</param>
        /// <returns>true - если в ячейке кнопка </returns>
        private bool IsHeaderButtonCell(DataGridViewCellEventArgs cellEvent)
        {
            if (cellEvent.RowIndex != -1 &&
              dgvAllPersons.Columns[cellEvent.ColumnIndex] is DataGridViewButtonColumn)
                return true;
            else return (false);
        }

        private void dgvAllPersons_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //обновляем зарплату, если есть люди и вызвали форму для показа
            if (e.RowIndex > -1 && !initialization)
            {
                UpdateSalary(e);
            }
        }
        /// <summary>
        /// Обновление зарплаты, когда ее обновляет пользователь
        /// </summary>
        /// <param name="e"></param>
        private void UpdateSalary(DataGridViewCellEventArgs e)
        {
            Tuple<Person, Plane> selectedItemTagAsTuple = (Tuple<Person, Plane>)dgvAllPersons.Rows[e.RowIndex].Tag;
            Person person = selectedItemTagAsTuple.Item1;
            int pred = person.TempSalary;
            int nov = person.TempSalary;
            if (dgvAllPersons[e.ColumnIndex, e.RowIndex].Value != null && 
                int.TryParse(dgvAllPersons[e.ColumnIndex, e.RowIndex].Value.ToString(), out nov) && 
                nov > 0)
            {
                dgvAllPersons[e.ColumnIndex, e.RowIndex].Value = nov;
                person.TempSalary = nov;
                int r = (game.MyAverageSalary + game.EnemyAverSalary) / 2;
                if (!person.Mood && nov >= r)
                {
                    initialization = person.Mood = true;
                    dgvAllPersons[e.ColumnIndex + 3, e.RowIndex].Value = "Нормальное";
                    initialization = false;
                }
            }
            else
            {
                dgvAllPersons[e.ColumnIndex, e.RowIndex].Value = pred;
            }
            dgvAllPersons.Refresh();
        }

        private void dgvAllPersons_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(CheckKey);
            e.Control.ContextMenu = new ContextMenu();
            TextBox tb = e.Control as TextBox;
            if (tb != null)
            {
                tb.ContextMenuStrip = new ContextMenuStrip();
                tb.KeyDown -= TextBox_KeyDown;
                tb.KeyDown += TextBox_KeyDown;
            }
        }
        /// <summary>
        /// Чтобы не вводилось ничего кроме цифр
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckKey(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void dgvAllPersons_SelectionChanged(object sender, EventArgs e)
        {
            //запрещает менять зп, если человек в полете
            if (dgvAllPersons.CurrentRow != null && 
                (string)dgvAllPersons[dgvAllPersons.ColumnCount - 3, dgvAllPersons.CurrentRow.Index].Value != "Нет") //&& dgvAllPersons.CurrentRow.Index < hangar.PlaneList.Count)
            {
                dgvAllPersons[0, dgvAllPersons.CurrentRow.Index].ReadOnly = true;
            }
        }

        //чтобы сохранялись введеные изменения при закрытии формы
        private void FormAllMyCrew_FormClosing(object sender, FormClosingEventArgs e)
        {
            dgvAllPersons.EndEdit();
        }

        void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Control && (e.KeyCode == Keys.C | e.KeyCode == Keys.V))
            //{
            //    e.SuppressKeyPress = true;
            //}
        }

        private void dgvAllPersons_Click(object sender, EventArgs e)
        {
            dgvAllPersons.CancelEdit();
        }
    }
}
