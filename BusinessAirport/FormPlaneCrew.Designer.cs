﻿namespace BusinessAirport
{
    partial class FormPlaneCrew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lvPlaneCrew = new System.Windows.Forms.ListView();
            this.btnChangeSalary = new System.Windows.Forms.Button();
            this.btnFromMarket = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lvFreePersons = new System.Windows.Forms.ListView();
            this.btnDismiss = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lvPlaneCrew);
            this.groupBox1.Controls.Add(this.btnChangeSalary);
            this.groupBox1.Controls.Add(this.btnFromMarket);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 314);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Список экипажа самолета";
            // 
            // lvPlaneCrew
            // 
            this.lvPlaneCrew.AllowDrop = true;
            this.lvPlaneCrew.FullRowSelect = true;
            this.lvPlaneCrew.Location = new System.Drawing.Point(6, 19);
            this.lvPlaneCrew.MultiSelect = false;
            this.lvPlaneCrew.Name = "lvPlaneCrew";
            this.lvPlaneCrew.Size = new System.Drawing.Size(233, 199);
            this.lvPlaneCrew.TabIndex = 9;
            this.lvPlaneCrew.UseCompatibleStateImageBehavior = false;
            this.lvPlaneCrew.View = System.Windows.Forms.View.Details;
            this.lvPlaneCrew.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvPlaneCrew_ItemDrag);
            this.lvPlaneCrew.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvPlaneCrew_DragDrop);
            this.lvPlaneCrew.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvPlaneCrew_DragEnter);
            // 
            // btnChangeSalary
            // 
            this.btnChangeSalary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnChangeSalary.Location = new System.Drawing.Point(6, 224);
            this.btnChangeSalary.Name = "btnChangeSalary";
            this.btnChangeSalary.Size = new System.Drawing.Size(233, 38);
            this.btnChangeSalary.TabIndex = 4;
            this.btnChangeSalary.Text = "Изменить зарплату";
            this.btnChangeSalary.UseVisualStyleBackColor = false;
            this.btnChangeSalary.Click += new System.EventHandler(this.btnChangeSalary_Click);
            // 
            // btnFromMarket
            // 
            this.btnFromMarket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnFromMarket.Location = new System.Drawing.Point(6, 268);
            this.btnFromMarket.Name = "btnFromMarket";
            this.btnFromMarket.Size = new System.Drawing.Size(233, 38);
            this.btnFromMarket.TabIndex = 3;
            this.btnFromMarket.Text = "Нанять";
            this.btnFromMarket.UseVisualStyleBackColor = false;
            this.btnFromMarket.Click += new System.EventHandler(this.btnFromMarket_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lvFreePersons);
            this.groupBox2.Controls.Add(this.btnDismiss);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(247, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(247, 314);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Резервные люди";
            // 
            // lvFreePersons
            // 
            this.lvFreePersons.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lvFreePersons.AllowDrop = true;
            this.lvFreePersons.FullRowSelect = true;
            this.lvFreePersons.Location = new System.Drawing.Point(6, 19);
            this.lvFreePersons.MultiSelect = false;
            this.lvFreePersons.Name = "lvFreePersons";
            this.lvFreePersons.Size = new System.Drawing.Size(233, 199);
            this.lvFreePersons.TabIndex = 9;
            this.lvFreePersons.UseCompatibleStateImageBehavior = false;
            this.lvFreePersons.View = System.Windows.Forms.View.Details;
            this.lvFreePersons.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvFreePersons_ItemDrag);
            this.lvFreePersons.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvFreePersons_DragDrop);
            this.lvFreePersons.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvFreePersons_DragEnter);
            // 
            // btnDismiss
            // 
            this.btnDismiss.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.btnDismiss.Location = new System.Drawing.Point(6, 225);
            this.btnDismiss.Name = "btnDismiss";
            this.btnDismiss.Size = new System.Drawing.Size(233, 38);
            this.btnDismiss.TabIndex = 4;
            this.btnDismiss.Text = "Уволить";
            this.btnDismiss.UseVisualStyleBackColor = false;
            this.btnDismiss.Click += new System.EventHandler(this.btnDismiss_Click);
            // 
            // FormPlaneCrew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(114)))), ((int)(((byte)(153)))));
            this.ClientSize = new System.Drawing.Size(494, 314);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPlaneCrew";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Экипаж самолета";
            this.Shown += new System.EventHandler(this.FormPlaneCrew_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnChangeSalary;
        private System.Windows.Forms.Button btnFromMarket;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDismiss;
        private System.Windows.Forms.ListView lvPlaneCrew;
        private System.Windows.Forms.ListView lvFreePersons;
    }
}