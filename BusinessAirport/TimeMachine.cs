﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace BusinessAirport
{
    [Serializable]
    /// <summary>
    /// Класс для обработки событий связанных с временной лентой
    /// </summary>
    public class TimeMachine : IDisposable
    {
        [NonSerialized]
        System.Timers.Timer timer;
        byte timeMultiplier;
        DateTime mainTime;
        public SortedList<DateTime, List<EventElem>> EventsList;
        public delegate void MethodTime(object sender, EventArgs e);
        public event MethodTime refreshTime;
        bool disposed = false;

        /// <summary>
        /// Инициализация элемента, с параметрами по умолчанию
        /// </summary>
        public TimeMachine()
        {
            mainTime = Properties.Settings.Default.StartDate;
            EventsList = new SortedList<DateTime, List<EventElem>>();
            timeMultiplier = 1;
            timer = new System.Timers.Timer(1000);
            timer.Elapsed += OnTick;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                timer.Dispose();
            }
            disposed = true;
        }

        /// <summary>
        /// Свойство, которое позволяет пользоваться текущим значением времени
        /// </summary>
        public DateTime MainTime
        {
            get { return mainTime; }
            set { mainTime = value; }
        }

        /// <summary>
        /// запуск таймера со значением 1 минута = 1 секунде реального времени
        /// </summary>
        public void Startx1()
        {
            timer.Interval = 1000;
            timeMultiplier = 1;
            timer.Start();
        }

        /// <summary>
        /// Возобновление игры
        /// </summary>
        public void Start()
        {
            timer.Start();
        }

        /// <summary>
        /// Остановка таймера
        /// </summary>
        public void Stop()
        {
            timer.Stop();
        }
        /// <summary>
        /// Увеличение скорость в 2 раза, вплоть до 8х
        /// </summary>
        public void IncSpeed()
        {
            if (timeMultiplier < 64)
            {
                timeMultiplier *= 2;
                timer.Interval = (double)1000 / timeMultiplier;
            }
        }

        public bool Status { get { return timer.Enabled; } }

        /// <summary>
        /// Текущее значение времени
        /// </summary>
        /// <returns>Возвращает строку вида "00:00" </returns>
        public string Time()
        {
            return mainTime.ToShortTimeString();
        }

        /// <summary>
        /// Добавление метода в очередь, в соответствии со временем его срабатываения
        /// </summary>
        /// <param name="t">Время срабатывания вызываемого метода</param>
        /// <param name="mc">Наименоваиние вызываемого метода без () в конце </param>
        /// <param name="param">Параметр, который вы хотите передать в свой метод</param>
        public void AddToList(DateTime t, EventElem.MethodContainer mc, object param)
        {
            if (mainTime > t)
                return;
            if (EventsList.ContainsKey(t))
                EventsList[t].Add(new EventElem(mc, param));
            else
            {
                List<EventElem> qee = new List<EventElem>
                {
                    new EventElem(mc, param)
                };
                EventsList.Add(t, qee);
            }
        }

        /// <summary>
        /// Удаление события из списка
        /// </summary>
        /// <param name="mc">Имя метода удаляемого события</param>
        /// <param name="param">Значение параметра удаляемого события</param>
        public void RemoveFromList(EventElem.MethodContainer mc, object param)
        {
            int mainIndex = 0, secondIndex;

            while (mainIndex < EventsList.Count)
            {
                secondIndex = 0;
                while (secondIndex < EventsList.Values[mainIndex].Count)
                    if (EventsList.Values[mainIndex][secondIndex].SomeMethod.Method == mc.Method && EventsList.Values[mainIndex][secondIndex].param == param)
                    {
                        EventsList.Values[mainIndex].RemoveAt(secondIndex);
                        if (EventsList.Values[mainIndex].Count == 0)
                            EventsList.RemoveAt(mainIndex);
                        return;
                    }
                    else
                        secondIndex++;
                mainIndex++;
            }
        }

        /// <summary>
        /// Срабатывание таймера
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnTick(Object source, ElapsedEventArgs e)
        {
            mainTime = mainTime.AddMinutes(1); // увеличение времени на 1 минуту
                                               // срабатывание событий, если они есть на назначеное время
            try
            {
                DateTime curTime = mainTime;
                while (EventsList.Count > 0 && EventsList.ContainsKey(curTime))
                {
                    foreach (EventElem ee in EventsList[curTime])
                    {
                        ee.SomeMethod(ee.param);
                    }
                    EventsList.Remove(curTime);
                }
            }
            catch
            {
                MessageBox.Show("Упс, похоже что-то пошло не так! \nчасики перстали тикать:( \nСообщите об этой ошибке разрабочикам", "Ошибка");
                System.Environment.Exit(0);
            }
            refreshTime?.Invoke(this, e); // обновление таймера на форме
        }
    }
}
