﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessAirport
{
    partial class FormAppoint : Form
    {
        bool isEnable = true;
        bool isChange;
        MainForm mainForm;
        TimeMachine tMachine;
        Game game;
        FlightScheduleElem CurFlight;
        List<Plane> PlaneForCurFlight = new List<Plane>();
        //Время вылета

        public DateTime TimeStart
        {
            get { return dtpDateDep.Value; }
            set { dtpDateDep.Value = value; }
        }
        //Время прибытия
        public DateTime TimeFinish
        {
            get { return CalcFinishTime(PlaneForCurFlight[dgvMyPlanes.CurrentRow.Index]); }
        }
        //Назначенный самолёт
        public Plane PlaneForFlight
        {
            get { return PlaneForCurFlight[dgvMyPlanes.CurrentRow.Index]; }
        }

        /// <summary>
        /// Конструктор для формы назначения самолёта
        /// </summary>
        /// <param name="mainForm">Экземпляр главной формы (если для пустого перелёта нужен баланс)</param>
        /// <param name="tMachine">Экземпляр машины времени</param>
        /// <param name="game">Экземпляр класса игры</param>
        /// <param name="CurFlight">Выбранный рейс</param>
        public FormAppoint(MainForm mainForm, Game game, FlightScheduleElem CurFlight, bool isChange = false)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            this.tMachine = game.tMachine;
            this.game = game;
            this.CurFlight = CurFlight;
            btnOK.Enabled = false;
            gbTimeOfFlight.Enabled = false;
            this.Text = "Рейс " + CurFlight.Flight.From + " - " + CurFlight.Flight.To;
            this.isChange = isChange;
            if (isChange)
                rbFlight.Enabled = rbEmptyFlight.Enabled = false;
            // Вывод лейбла с сообщением о выгодности
            InitializeProfit();
            // Задание граничных значений в ДТП
            InitializeDTP();
            // Вывод подходящих самолетов
            InitializePlanes();
        }

        public DateTime CalcFinishTime(Plane p)
        {
            DateTime t = dtpDateDep.Value;
            t = t.AddHours(Convert.ToByte(CurFlight.Flight.Distance / p.speed));
            t = t.AddMinutes(Convert.ToByte(20));
            return t;
        }

        public DateTime CalcFinishTime(Plane p, int distance)
        {
            DateTime t = dtpDateDep.Value;
            t = t.AddHours(Convert.ToByte(distance / p.speed));
            t = t.AddMinutes(Convert.ToByte(20));
            return t;
        }

        private void AddEmptyFlight()
        {
            Flight flight = new Flight(0, PlaneForFlight.curCity, CurFlight.Flight.From, CurFlight.Flight.Type);
            flight.IsEmptyFlight = true;
            DateTime startTime = dtpDateDep.Value;
            DateTime finishTime = CalcFinishTime(PlaneForFlight, flight.Distance);

            var fse = new FlightScheduleElem(game.ScheduleNumber, flight, PlaneForFlight, startTime, finishTime, 0);
            string message = "Вы уверены, что хотите выполнить пустой перелет? \n" + "Вам придется заплатить " + fse.Expense + "$";
            if (MessageBox.Show(message, "Пустой перелет", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                game.AddEmptyFlight(fse);
                Close();
            }
        }

        private void dgvMyPlanes_SelectionChanged(object sender, EventArgs e)
        {
            #region Блокировка кнопок
            if (dgvMyPlanes.RowCount == 0 /*|| PlaneForCurFlight[dgvMyPlanes.CurrentRow.Index].curCity != CurFlight.Flight.From*/)
            {
                btnOK.Enabled = gbTimeOfFlight.Enabled = false;
            }
            else
            {
                btnOK.Enabled = gbTimeOfFlight.Enabled = true;
                dtpDateDep_ValueChanged(null, null);
            }
            #endregion
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (rbFlight.Checked)
            {
                if (TimeStart > tMachine.MainTime)
                {
                    CurFlight.Appoint(PlaneForFlight, TimeStart, TimeFinish);
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    btnOK.Enabled = false;
                    DialogResult = DialogResult.None;
                    MessageBox.Show("Время, назначенное для старта, уже прошло!");
                }
            }
            else
            {
                DialogResult = DialogResult.None;
                AddEmptyFlight();
            }
        }

        private void FormAppoint_Load(object sender, EventArgs e)
        {
            if (!isEnable)
            {
                MessageBox.Show("К сожалению невозможно поставить рейс до конца дня");
                Close();
            }

        }

        private void dtpDateDep_ValueChanged(object sender, EventArgs e)
        {
            RefreshInfo();
        }

        private void RefreshInfo()
        {
            if (PlaneForCurFlight.Count != 0 && dgvMyPlanes.SelectedRows.Count != 0)
            {
                if (!isChange)
                    if (PlaneForFlight.curCity == CurFlight.Flight.From)
                    {
                        rbEmptyFlight.Checked = false;
                        rbEmptyFlight.Enabled = false;
                    }
                    else
                        rbEmptyFlight.Enabled = true;

                DateTime t = rbFlight.Checked ? CalcFinishTime(PlaneForCurFlight[dgvMyPlanes.CurrentRow.Index])
                    : CalcFinishTime(PlaneForFlight, Flight.massDist[(int)PlaneForFlight.curCity, (int)CurFlight.Flight.From]);
                lbTimeOfFinish.Text = t.ToString("dd.MM.yy # HH:mm");
                btnOK.Enabled = true;
            }
        }

        private void rbFlight_CheckedChanged(object sender, EventArgs e)
        {
            RefreshInfo();
        }

        private void InitializeDTP()
        {
            DateTime t = tMachine.MainTime.AddMinutes(1);
            if (!CurFlight.Flight.IsEmptyFlight && CurFlight.Flight.HowOften == HowOften.Нет)
            {
                dtpDateDep.MaxDate = CurFlight.Flight.CompleteBefore.AddMinutes(-1);
                lbCompleteBefore.Text = "до " + CurFlight.Flight.CompleteBefore.ToString("dd.MM.yy # HH:mm");
            }
            else
                lbCompleteBefore.Text = "";

            dtpDateDep.Value = CurFlight.DepartureTime == new DateTime() ? t : CurFlight.DepartureTime;
            dtpDateDep.MinDate = mainForm.pFlightsForm.isBackFlight ? mainForm.pFlightsForm.BackFlightTime : t;
            if (CurFlight.Brother != null)
                if (CurFlight.Brother.Plane != null)
                    if (CurFlight.Flight.BackFlight)
                        dtpDateDep.MinDate = CurFlight.Brother.ArrivalTime.AddMinutes(1);
                    else
                        dtpDateDep.MaxDate = CurFlight.Brother.DepartureTime.AddMinutes(-1);
                else
                    dtpDateDep.MaxDate = DateTime.MaxValue;
        }

        private void InitializeProfit()
        {
            if (CurFlight.Flight.Type == PlaneType.Грузовой)
                lbProfitTime.Visible = lbProfitText.Visible = false;
            else
                if (CurFlight.Flight.RandomHour <= 5)
                lbProfitTime.Text = "ночь";
            else
                    if (CurFlight.Flight.RandomHour <= 12)
                lbProfitTime.Text = "утро";
            else
                        if (CurFlight.Flight.RandomHour <= 18)
                lbProfitTime.Text = "день";
            else
                lbProfitTime.Text = "вечер";
        }

        private void InitializePlanes()
        {

            if (game.Hangar.PlaneList != null)
            {
                foreach (Plane p in game.Hangar.PlaneList)
                {
                    //Показываем только самолёты с подходящими характеристиками и уже находящиеся в ангаре
                    if (p.carryWeight >= CurFlight.Flight.Weight
                        && p.pType == CurFlight.Flight.Type
                        && p.maxDistance >= CurFlight.Flight.Distance
                        && !(p.typeOfOwnership == OwnType.Buy && p.lease > 0))
                    {
                        PlaneForCurFlight.Add(p);
                        dgvMyPlanes.Rows.Add(
                            p.name,
                            p.curCity,
                            Flight.massDist[(int)p.curCity, (int)CurFlight.Flight.From],
                            p.consumption,
                            p.allDistance / p.speed);
                        //Выделим самолёты, который находятся в другом городе
                        if (p.curCity != CurFlight.Flight.From)
                        {
                            dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.BackColor = Color.Silver;
                            dgvMyPlanes.Rows[dgvMyPlanes.RowCount - 1].DefaultCellStyle.ForeColor = Color.Gray;
                        }
                    }
                }
            }
        }

        private void FormAppoint_Shown(object sender, EventArgs e)
        {
            dgvMyPlanes.ClearSelection();
            dtpDateDep.Value = dtpDateDep.MinDate;
            btnOK.Enabled = false;
            lbTimeOfFinish.Text = "Выберите самолет";
        }
    }
}
